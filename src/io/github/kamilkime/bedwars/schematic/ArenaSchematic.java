package io.github.kamilkime.bedwars.schematic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.util.FileUtil;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaRegion;
import io.github.kamilkime.bedwars.arena.ArenaUtils;
import io.github.kamilkime.bedwars.constant.arena.ArenaMode;
import io.github.kamilkime.bedwars.constant.arena.ShopType;
import io.github.kamilkime.bedwars.constant.generator.GeneratorType;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.team.Team;

public final class ArenaSchematic {

  public static final File SCHEMATICS_FOLDER = new File(BedWars.getPlugin().getDataFolder(), "schematics");

  static {
    if (!SCHEMATICS_FOLDER.exists()) {
      SCHEMATICS_FOLDER.mkdirs();
    }
  }

  public static Arena loadSchematic(final File arenaSchemFile, final Location baseLocation, final String arenaName) {
    final YamlConfiguration arenaSchemYaml = YamlConfiguration.loadConfiguration(arenaSchemFile);
    final File weSchemFile = new File(SCHEMATICS_FOLDER, arenaSchemYaml.getString("weSchematic"));

    if (!SchematicHelper.pasteSchematic(weSchemFile, baseLocation, false)) {
      return null;
    }

    // CONSTRUTOR SETTINGS
    final ArenaMode arenaMode = ArenaMode.valueOf(arenaSchemYaml.getString("arenaMode"));
    final Location minBound = baseLocation.clone().subtract(DataUtils.stringToLocation(arenaSchemYaml.getString("region.minBound")));
    final Location maxBound = baseLocation.clone().subtract(DataUtils.stringToLocation(arenaSchemYaml.getString("region.maxBound")));

    final Arena arena = new Arena(arenaName, arenaMode, minBound, maxBound);

    // ARENA REGION
    final ArenaRegion arenaRegion = arena.getArenaRegion();
    arenaRegion.setMainLobbyLocation(baseLocation.clone().subtract(DataUtils.stringToLocation(arenaSchemYaml.getString("region.mainLobbyLocation"))));
    arenaRegion.setSpectatorLobbyLocation(baseLocation.clone().subtract(DataUtils.stringToLocation(arenaSchemYaml.getString("region.spectatorLobbyLocation"))));
    arenaRegion.setArenaCenter(baseLocation.clone().subtract(DataUtils.stringToLocation(arenaSchemYaml.getString("region.arenaCenter"))));

    // ARENA SHOPS
    final List<String> shopList = arenaSchemYaml.getStringList("region.shops");
    if (shopList != null && !shopList.isEmpty()) {
      for (final String shop : shopList) {
        final String[] split = shop.split("\\|");
        final ShopType shopType = ShopType.valueOf(split[1]);

        final Location relativeShop = DataUtils.stringToLocation(split[0]);
        final Location shopLocation = baseLocation.clone().subtract(relativeShop);

        shopLocation.setYaw(relativeShop.getYaw());
        shopLocation.setPitch(relativeShop.getPitch());

        arenaRegion.addShop(ArenaUtils.spawnShop(shopLocation, shopType), shopType);
      }
    }

    // ARENA GENERATORS
    final List<String> generatorList = arenaSchemYaml.getStringList("region.generators");
    if (generatorList != null && !generatorList.isEmpty()) {
      for (final String generator : generatorList) {
        final String[] split = generator.split("\\|");
        arenaRegion.addGenerator(new ItemGenerator(GeneratorType.valueOf(split[1]), baseLocation.clone().subtract(DataUtils.stringToLocation(split[0]))));
      }
    }

    // ARENA TEAMS
    for (final Team team : arena.getTeams().values()) {
      final ConfigurationSection teamSection = arenaSchemYaml.getConfigurationSection("teams." + team.getTeamColor().toString());
      if (teamSection == null) {
        continue;
      }

      final Location generationLocation = DataUtils.stringToLocation(teamSection.getString("teamGenerator"));
      if (generationLocation != null) {
        team.setTeamGenerator(new ItemGenerator(GeneratorType.TEAM, baseLocation.clone().subtract(generationLocation)));
      }

      team.setTeamSpawn(baseLocation.clone().subtract(DataUtils.stringToLocation(teamSection.getString("teamSpawn"))));
      team.setDeathmatchSpawn(baseLocation.clone().subtract(DataUtils.stringToLocation(teamSection.getString("deathmatchSpawn"))));

      team.setTeamChest(baseLocation.clone().subtract(DataUtils.stringToLocation(teamSection.getString("teamChest"))));
      if (team.getTeamChest() != null && team.getTeamChest().getBlock().getType() != Material.CHEST) {
        team.getTeamChest().getBlock().setType(Material.CHEST);
      }

      final String[] bed = teamSection.getString("teamBed").split("\\|");
      team.setTeamBed(Pair.of(baseLocation.clone().subtract(DataUtils.stringToLocation(bed[0])), Byte.parseByte(bed[1])));
    }

    return arena;
  }

  public static boolean saveSchematic(final Arena arena, final Location baseLocation, final File weSchemFile, final File arenaSchemFile) {
    try {
      arenaSchemFile.createNewFile();
    } catch (IOException exception) {
      Bukkit.getLogger().warning("[BedWars] Failed to create a schem file for arena " + arena.getArenaName() + "!");
      return false;
    }

    final YamlConfiguration arenaSchemYaml = YamlConfiguration.loadConfiguration(arenaSchemFile);

    // MAIN SETTINGS
    arenaSchemYaml.set("weSchematic", weSchemFile.getName());
    arenaSchemYaml.set("arenaMode", arena.getArenaMode().toString());

    // ARENA REGION
    final ArenaRegion arenaRegion = arena.getArenaRegion();
    arenaSchemYaml.set("region.mainLobbyLocation", DataUtils.locationToString(baseLocation.clone().subtract(arenaRegion.getMainLobbyLocation())));
    arenaSchemYaml.set("region.spectatorLobbyLocation", DataUtils.locationToString(baseLocation.clone().subtract(arenaRegion.getSpectatorLobbyLocation())));
    arenaSchemYaml.set("region.arenaCenter", DataUtils.locationToString(baseLocation.clone().subtract(arenaRegion.getArenaCenter())));
    arenaSchemYaml.set("region.minBound", DataUtils.locationToString(baseLocation.clone().subtract(arenaRegion.getArenaBounds().getLeft())));
    arenaSchemYaml.set("region.maxBound", DataUtils.locationToString(baseLocation.clone().subtract(arenaRegion.getArenaBounds().getRight())));

    // ARENA SHOPS
    final List<String> shops = new ArrayList<>();
    for (final Entry<Entity, ShopType> shopEntry : arenaRegion.getShops().entrySet()) {
      final Location relativeShop = baseLocation.clone().subtract(shopEntry.getKey().getLocation());

      relativeShop.setYaw(shopEntry.getKey().getLocation().getYaw());
      relativeShop.setPitch(shopEntry.getKey().getLocation().getPitch());

      shops.add(DataUtils.locationToString(relativeShop) + "|" + shopEntry.getValue().toString());
    }

    arenaSchemYaml.set("region.shops", shops);

    // ARENA GENERATORS
    final List<String> generators = new ArrayList<>();
    for (final ItemGenerator generator : arenaRegion.getGenerators()) {
      generators.add(DataUtils.locationToString(baseLocation.clone().subtract(generator.getLocation())) + "|" + generator.getType().toString());
    }

    arenaSchemYaml.set("region.generators", generators);

    // ARENA TEAMS
    for (final Team team : arena.getTeams().values()) {
      final String base = "teams." + team.getTeamColor().toString() + ".";

      arenaSchemYaml.set(base + "teamGenerator", DataUtils.locationToString(baseLocation.clone().subtract(team.getTeamGenerator().getLocation())));
      arenaSchemYaml.set(base + "teamSpawn", DataUtils.locationToString(baseLocation.clone().subtract(team.getTeamSpawn())));
      arenaSchemYaml.set(base + "teamChest", DataUtils.locationToString(baseLocation.clone().subtract(team.getTeamChest())));
      arenaSchemYaml.set(base + "deathmatchSpawn", DataUtils.locationToString(baseLocation.clone().subtract(team.getDeathmatchSpawn())));
      arenaSchemYaml.set(base + "teamBed", DataUtils.locationToString(baseLocation.clone().subtract(team.getTeamBed().getLeft())) + "|" + team.getTeamBedData());
    }

    try {
      arenaSchemYaml.save(arenaSchemFile);
    } catch (IOException exception) {
      Bukkit.getLogger().warning("[BedWars] Failed to save file for arena named " + arena.getArenaName() + "!");
      return false;
    }

    FileUtil.copy(weSchemFile, new File(ArenaSchematic.SCHEMATICS_FOLDER, weSchemFile.getName()));
    return true;
  }

}
