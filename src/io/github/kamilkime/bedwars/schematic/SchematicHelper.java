/*
 * Code by: insertt (https://github.com/insertt)
 * Taken from: FunnyGuilds (https://github.com/FunnyGuilds/FunnyGuilds/)
 */

package io.github.kamilkime.bedwars.schematic;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.session.ClipboardHolder;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.registry.WorldData;

import java.io.File;
import java.io.FileInputStream;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public final class SchematicHelper {

  private SchematicHelper() {
  }

  public static boolean pasteSchematic(final File schematicFile, final Location location, final boolean withAir) {
    try {
      final Vector pasteLocation = new Vector(location.getX(), location.getY(), location.getZ());
      final World pasteWorld = new BukkitWorld(location.getWorld());
      final WorldData pasteWorldData = pasteWorld.getWorldData();

      final Clipboard clipboard = ClipboardFormat.SCHEMATIC.getReader(new FileInputStream(schematicFile)).read(pasteWorldData);
      final ClipboardHolder clipboardHolder = new ClipboardHolder(clipboard, pasteWorldData);

      final EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(pasteWorld, -1);
      final Operation operation = clipboardHolder.createPaste(editSession, pasteWorldData).to(pasteLocation).ignoreAirBlocks(!withAir).build();

      Operations.completeLegacy(operation);
      return true;
    } catch (final Exception exception) {
      Bukkit.getLogger().warning("[BedWars] Failed to paste schematic " + schematicFile.getName());
      return false;
    }
  }

}
