package io.github.kamilkime.bedwars.listener.block;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockIgniteEvent;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;

public class BlockIgniteListener implements Listener {

  @EventHandler
  public void onIgnite(final BlockIgniteEvent event) {
    Arena targetArena = null;
    for (final Arena arena : ArenaData.getArenas()) {
      if (arena.getArenaRegion().isInRegion(event.getBlock().getLocation())) {
        targetArena = arena;
        break;
      }
    }

    if (targetArena != null) {
      event.setCancelled(true);
    }
  }

}
