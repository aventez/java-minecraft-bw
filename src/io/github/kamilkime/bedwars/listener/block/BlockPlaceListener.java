package io.github.kamilkime.bedwars.listener.block;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaRegion;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.buildmode.BuildModePlaceHandler;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.perk.PerkUtils;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.turret.Turret;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class BlockPlaceListener implements Listener {

  @EventHandler
  public void onPlace(final BlockPlaceEvent event) {
    final Player player = event.getPlayer();
    final User user = UserData.getUser(player.getUniqueId());

    final Arena arena = user.getCurrentArena();
    if (arena == null) {
      return;
    }

    if (BuildMode.isBuilding(user)) {
      BuildModePlaceHandler.handleBlockPlace(event);
      return;
    }

    final Block block = event.getBlock();
    final Location blockLocation = block.getLocation();

    if (blockLocation.getY() > BedWarsConstants.MAX_BUILD_HEIGHT) {
      player.sendMessage(Messages.ARENA_PLACE_CANCEL_HEIGHT);
      event.setCancelled(true);
      return;
    }

    if (block.getType() == Material.TNT) {
      Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> {
        block.setType(Material.AIR);
        PerkUtils.addTntPlacer(blockLocation.getWorld().spawn(blockLocation, TNTPrimed.class), user);
      });

      return;
    }

    final ArenaRegion arenaRegion = arena.getArenaRegion();

    for (final Team team : arena.getTeams().values()) {
      if (team.getTeamSpawn().distanceSquared(blockLocation) <= BedWarsConstants.ARENA_NO_BUILD_TEAM_SPAWN_RADIUS_SQ) {
        player.sendMessage(Messages.ARENA_PLACE_CANCEL_TEAM_SPAWN);
        event.setCancelled(true);
        return;
      }

      if (team.getDeathmatchSpawn().distanceSquared(blockLocation) <= BedWarsConstants.ARENA_NO_BUILD_TEAM_DEATHMATCH_SPAWN_RADIUS_SQ) {
        player.sendMessage(Messages.ARENA_PLACE_CANCEL_TEAM_DEATHMATCH_SPAWN);
        event.setCancelled(true);
        return;
      }
    }

    for (final ItemGenerator generator : arenaRegion.getGenerators()) {
      if (generator.getLocation().distanceSquared(blockLocation) <= BedWarsConstants.ARENA_NO_BUILD_GENERATOR_RADIUS_SQ) {
        player.sendMessage(Messages.ARENA_PLACE_CANCEL_GENERATOR);
        event.setCancelled(true);
        return;
      }
    }

    final ItemStack handItem = event.getItemInHand().clone();
    final Turret placedTurret = Turret.getTurret(handItem, blockLocation.add(0.5D, 0.0D, 0.5D), arena, arena.getUserTeam(user), user);

    if (placedTurret != null) {
      if (arena.getUserTeam(user).getTeamSpawn().distanceSquared(blockLocation) > BedWarsConstants.ARENA_TURRET_PLACE_RADIUS_SQ) {
        player.sendMessage(Messages.ARENA_TURRET_PLACE_CANCEL);
        event.setCancelled(true);
        return;
      }

      if (arenaRegion.getTurrets().get(user) == null) {
        Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> {
          placedTurret.place();
          placedTurret.start();

          arenaRegion.addTurret(user, placedTurret);

          handItem.setAmount(1);
          player.getInventory().removeItem(handItem);
        });
      } else {
        player.sendMessage(Messages.ARENA_ONE_TURRET_PER_USER);
      }

      event.setCancelled(true);
    } else {
      arena.getArenaRegion().addPlacedBlock(block);
    }
  }

}
