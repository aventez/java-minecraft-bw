package io.github.kamilkime.bedwars.listener.block;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.buildmode.BuildModeBreakHandler;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.TeamUtils;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class BlockBreakListener implements Listener {

  @EventHandler
  public void onBreak(final BlockBreakEvent event) {
    final Player player = event.getPlayer();
    final User user = UserData.getUser(player.getUniqueId());

    final Arena arena = user.getCurrentArena();
    if (arena == null) {
      return;
    }

    if (BuildMode.isBuilding(user)) {
      BuildModeBreakHandler.handleBlockBreak(event);
      return;
    }

    final Block block = event.getBlock();
    final Location blockLocation = block.getLocation();

    if (block.getType() == Material.BED_BLOCK) {
      final Team userTeam = arena.getUserTeam(user);
      final Pair<Location, Location> userBed = userTeam.getTeamBed();

      if (userBed.getLeft().equals(blockLocation) || userBed.getRight().equals(blockLocation)) {
        event.setCancelled(true);
        return;
      }

      for (final Team team : arena.getTeams().values()) {
        if (team.equals(userTeam)) {
          continue;
        }

        final Pair<Location, Location> bed = team.getTeamBed();
        if (bed.getLeft().equals(blockLocation) || bed.getRight().equals(blockLocation)) {
          TeamUtils.destroyBed(arena, team, user);
          event.setCancelled(true);
          return;
        }
      }
    } else {
      if (arena.getArenaRegion().getPlacedBlocks().contains(block)) {
        arena.getArenaRegion().removePlacedBlock(block);
      } else {
        event.setCancelled(true);
      }
    }
  }

}
