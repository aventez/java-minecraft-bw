package io.github.kamilkime.bedwars.listener.entity;

import org.bukkit.entity.Entity;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Player;
import org.bukkit.entity.Silverfish;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.turret.Turret;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class EntityDamageByEntityListener implements Listener {

  @EventHandler
  public void onDamage(final EntityDamageByEntityEvent event) {
    if (!(event.getEntity() instanceof Player)) {
      return;
    }

    final User user = UserData.getUser(event.getEntity().getUniqueId());
    if (!user.isInGame()) {
      return;
    }

    final Entity damager = event.getDamager();

    final Arena arena = user.getCurrentArena();
    final Team userTeam = arena.getUserTeam(user);

    final Team arenaEntityTeam = arena.getUserTeam(arena.getArenaRegion().getEntityUser(damager));

    final Turret turret = arena.getArenaRegion().getProjectileTurret(damager);
    final Team arenaProjectileTeam = turret == null ? null : turret.getOwnerTeam();

    final double currentDamage = event.getDamage();
    if (damager instanceof IronGolem) {
      event.setDamage(currentDamage * 0.2D);
    } else if (damager instanceof Silverfish) {
      event.setDamage(currentDamage * 1.5D);
    }

    if (userTeam.equals(arenaEntityTeam) || userTeam.equals(arenaProjectileTeam)) {
      event.setCancelled(true);
    }
  }

}
