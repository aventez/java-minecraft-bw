package io.github.kamilkime.bedwars.listener.entity;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.arena.ArenaRegion;

public class EntityDeathListener implements Listener {

  @EventHandler
  public void onDeath(final EntityDeathEvent event) {
    final Entity dead = event.getEntity();

    event.getDrops().clear();

    ArenaData.getArenas().forEach(arena -> {
      final ArenaRegion arenaRegion = arena.getArenaRegion();

      if (dead instanceof ArmorStand) {
        arenaRegion.getTurrets().values().forEach(turret -> {
          if (turret.getEntity().getUniqueId().equals(dead.getUniqueId())) {
            turret.stop();
            arenaRegion.removeTurret(turret);
            return;
          }
        });
      } else {
        arenaRegion.removeArenaEntity(dead);
        arenaRegion.removeTurretProjectile(dead);
      }
    });
  }

}
