package io.github.kamilkime.bedwars.listener.entity;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.scheduler.BukkitRunnable;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class ProjectileLaunchListener implements Listener {

  @EventHandler
  public void onLaunch(final ProjectileLaunchEvent event) {
    final Projectile projectile = event.getEntity();
    if (!(projectile instanceof Snowball)) {
      return;
    }

    if (!(projectile.getShooter() instanceof Player)) {
      return;
    }

    final Player player = (Player) projectile.getShooter();
    final User user = UserData.getUser(player.getUniqueId());

    if (!user.isInGame()) {
      return;
    }

    final int entityNum = user.getCurrentArena().getArenaRegion().getUserEntityNumber(user, EntityType.SILVERFISH);
    if (entityNum >= BedWarsConstants.ENTITY_LIMITS.get(EntityType.SILVERFISH)) {
      event.setCancelled(true);
      player.sendMessage(Messages.ARENA_SILVERFISH_LIMIT);
    }
  }

  @EventHandler
  public void onHypixelBridge(ProjectileLaunchEvent e) {
    if (!(e.getEntity() instanceof Egg)) {
    	//particles
        if (!(e.getEntity().getShooter() instanceof Player)) {
            return;
        }
        
        final Player player = (Player) e.getEntity().getShooter();
        player.sendMessage("test");
        
        if(!(e.getEntity() instanceof Arrow)) {
        	return;
        }
        
		Player p = (Player) e.getEntity().getShooter();
		if (BedWars.getArrowTrails().hasTrail(p)) {
			BedWars.getArrowTrails().getTrail(p).addArrow((Arrow) e.getEntity());
			player.sendMessage("test3");
		}
        
        player.sendMessage("test2");
    	
    	return;
    }
     
    if (!(e.getEntity().getShooter() instanceof Player)) {
      return;
    }

    final Player player = (Player) e.getEntity().getShooter();
    final User user = UserData.getUser(player.getUniqueId());

    if (!user.isInGame()) {
      return;
    }

    new BukkitRunnable() {

      int delayIncrement = 2;
      int iteration = 0;

      @Override
      public void run() {
        //skip higher coordinates
        if (e.getEntity().getLocation().getY() > 40 || e.getEntity().getLocation().getY() < 10) {
          return;
        }
        Location location = e.getEntity().getLocation();
        World world = e.getEntity().getWorld();

        Bukkit.getScheduler().runTaskLater(BedWars.getPlugin(), () -> {
          validateAndSet(world.getBlockAt(location.clone().add(0, -1, 0)));
          validateAndSet(world.getBlockAt(location.clone().add(-1, -1, -1)));
          validateAndSet(world.getBlockAt(location.clone().add(0, -1, 1)));
          validateAndSet(world.getBlockAt(location.clone().add(1, -1, 1)));
          validateAndSet(world.getBlockAt(location.clone().add(-1, -1, 0)));
          validateAndSet(world.getBlockAt(location.clone().add(1, -1, 0)));
          validateAndSet(world.getBlockAt(location.clone().add(-1, -1, -1)));
          validateAndSet(world.getBlockAt(location.clone().add(0, -1, -1)));
          validateAndSet(world.getBlockAt(location.clone().add(1, -1, -1)));
          world.playSound(e.getEntity().getLocation(), Sound.BLOCK_LAVA_POP, 2, 1);
        }, delayIncrement);
        if (delayIncrement < 15) {
          delayIncrement++;
        }
        iteration++;
        if (e.getEntity().isOnGround() || e.getEntity().isDead() || iteration >= 40) {
          this.cancel();
        }
      }
    }.runTaskTimer(BedWars.getPlugin(), 0, 0);
  }

  private void validateAndSet(Block block) {
    if (block.getType() == Material.AIR) {
      block.setType(Material.SANDSTONE);
    }
  }

}
