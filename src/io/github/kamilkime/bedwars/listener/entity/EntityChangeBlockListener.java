package io.github.kamilkime.bedwars.listener.entity;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;

public class EntityChangeBlockListener implements Listener {

  @EventHandler
  public void onBlockChange(final EntityChangeBlockEvent event) {
    if (event.getEntityType() == EntityType.SILVERFISH) {
      event.setCancelled(true);
    }
  }

}
