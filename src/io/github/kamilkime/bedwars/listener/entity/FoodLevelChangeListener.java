package io.github.kamilkime.bedwars.listener.entity;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import io.github.kamilkime.bedwars.user.UserData;

public class FoodLevelChangeListener implements Listener {

  @EventHandler
  public void onFoodChange(final FoodLevelChangeEvent event) {
    if (UserData.getUser(event.getEntity().getUniqueId()).isInGame()) {
      event.setCancelled(true);
    }
  }

}
