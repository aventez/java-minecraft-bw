package io.github.kamilkime.bedwars.listener.entity;

import java.util.Map.Entry;

import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.turret.Turret;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class EntityDamageListener implements Listener {

  @EventHandler
  public void onDamage(final EntityDamageEvent event) {
    if (event.getEntity() instanceof Player) {
      final User user = UserData.getUser(event.getEntity().getUniqueId());
      if (!user.isInGame()) {
        return;
      }

      if (!user.getCurrentArena().getGamePhase().isGame()) {
        event.setCancelled(true);
        return;
      }
    } else if (!(event.getEntity() instanceof ArmorStand)) {
      return;
    }

    Turret turret = null;
    User turretOwner = null;

    for (final Arena arena : ArenaData.getArenas()) {
      for (final Entry<User, Turret> entry : arena.getArenaRegion().getTurrets().entrySet()) {
        if (entry.getValue().getEntity().getUniqueId().equals(event.getEntity().getUniqueId())) {
          turret = entry.getValue();
          turretOwner = entry.getKey();

          break;
        }
      }
    }

    if (turret == null || turretOwner == null) {
      return;
    }

    final Arena arena = turretOwner.getCurrentArena();
    if (event instanceof EntityDamageByEntityEvent) {
      final EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent) event;
      Player damager = null;

      if (damageEvent.getDamager() instanceof Player) {
        damager = (Player) damageEvent.getDamager();
      } else if (damageEvent.getDamager() instanceof Projectile) {
        final Projectile proj = (Projectile) damageEvent.getDamager();
        if (proj.getShooter() instanceof Player) {
          damager = (Player) proj.getShooter();
        }
      }

      if (damager != null) {
        final User damagerUser = UserData.getUser(damager.getUniqueId());

        if (arena.getUserTeam(turretOwner).equals(arena.getUserTeam(damagerUser))) {
          event.setCancelled(true);
          return;
        }
      }
    }

    final double newHealth = turret.getEntity().getHealth() - event.getFinalDamage();
    if (newHealth <= 0.0D) {
      turret.stop();
      arena.getArenaRegion().removeTurret(turret);

      event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_ZOMBIE_BREAK_DOOR_WOOD, 0.4F, 1.0F);
      return;
    }

    turret.getEntity().setHealth(newHealth);
    turret.updateName();

    event.setDamage(0.0D);
    event.getEntity().getWorld().playSound(event.getEntity().getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 0.4F, 1.0F);
  }

}
