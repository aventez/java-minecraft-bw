package io.github.kamilkime.bedwars.listener.entity;

import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.constant.task.ArenaEntityTask;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class EntityTargetLivingEntityListener implements Listener {

  @EventHandler
  public void onTarget(final EntityTargetLivingEntityEvent event) {
    if (event.getTarget() == null) {
      return;
    }

    final Entity entity = event.getEntity();
    if (!(entity instanceof Creature)) {
      return;
    }

    Arena entityArena = null;
    Team ownerTeam = null;

    for (final Arena arena : ArenaData.getArenas()) {
      final User owner = arena.getArenaRegion().getEntityUser(entity);
      if (owner != null) {
        entityArena = arena;
        ownerTeam = arena.getUserTeam(owner);
        break;
      }
    }

    if (ownerTeam == null) {
      return;
    }

    Team targetTeam = null;
    for (final Arena arena : ArenaData.getArenas()) {
      final User owner = arena.getArenaRegion().getEntityUser(entity);
      if (owner != null) {
        targetTeam = arena.getUserTeam(owner);
        break;
      }
    }

    if (targetTeam == null) {
      final User user = UserData.getUser(event.getTarget().getUniqueId());
      if (user == null || !user.isInGame()) {
        return;
      }

      targetTeam = entityArena.getUserTeam(user);
    }

    if (ownerTeam.equals(targetTeam)) {
      event.setTarget(ArenaEntityTask.targetNearestEnemy((Creature) entity, entityArena, ownerTeam));
    }
  }

}
