package io.github.kamilkime.bedwars.listener.entity;

import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Silverfish;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.task.ArenaEntityTask;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class ProjectileHitListener implements Listener {

  @EventHandler
  public void onHit(final ProjectileHitEvent event) {
    final Projectile projectile = event.getEntity();
    if (!(projectile instanceof Snowball)) {
      return;
    }

    if (!(projectile.getShooter() instanceof Player)) {
      return;
    }

    final User shooter = UserData.getUser(((Player) projectile.getShooter()).getUniqueId());
    if (!shooter.isInGame()) {
      return;
    }

    final Arena arena = shooter.getCurrentArena();
    final Team shooterTeam = arena.getUserTeam(shooter);

    new ArenaEntityTask(projectile.getWorld().spawn(projectile.getLocation(), Silverfish.class), arena, shooterTeam, shooter, 30.0D).start();
    projectile.remove();
  }

}
