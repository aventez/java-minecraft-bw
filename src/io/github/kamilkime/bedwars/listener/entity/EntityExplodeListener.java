package io.github.kamilkime.bedwars.listener.entity;

import java.util.Iterator;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.util.Vector;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.arena.ArenaRegion;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.perk.Perk;
import io.github.kamilkime.bedwars.perk.PerkUtils;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.TeamUtils;
import io.github.kamilkime.bedwars.user.User;

public class EntityExplodeListener implements Listener {

  @EventHandler
  public void onExplode(final EntityExplodeEvent event) {
    Arena targetArena = null;
    for (final Arena arena : ArenaData.getArenas()) {
      if (arena.getArenaRegion().isInRegion(event.getLocation())) {
        targetArena = arena;
        break;
      }
    }

    if (targetArena == null) {
      return;
    }

    final ArenaRegion region = targetArena.getArenaRegion();
    final Iterator<Block> blockIterator = event.blockList().iterator();

    boolean bomberFired = false;

    while (blockIterator.hasNext()) {
      final Block block = blockIterator.next();

      if (block.getType() == Material.BED_BLOCK && !bomberFired) {
        final User placer = PerkUtils.getTntPlacer(event.getEntity());
        if (placer != null && Math.random() < placer.getPerkBonus(Perk.BOMBER)) {
          final Team userTeam = targetArena.getUserTeam(placer);
          Team bedTeam = null;

          for (final Team team : targetArena.getTeams().values()) {
            if (team.equals(userTeam)) {
              continue;
            }

            final Pair<Location, Location> teamBed = team.getTeamBed();
            if (teamBed.getLeft().equals(block.getLocation()) || teamBed.getRight().equals(block.getLocation())) {
              bedTeam = team;
              break;
            }
          }

          if (bedTeam != null) {
            placer.getPlayer().sendMessage(Messages.USER_BOMBER_FIRED);
            TeamUtils.destroyBed(targetArena, bedTeam, placer);

            bomberFired = true;
            blockIterator.remove();

            continue;
          }
        }
      }

      if (region.getPlacedBlocks().contains(block)) {
        final int steps = 10;
        final Vector route = block.getLocation().toVector().subtract(event.getLocation().toVector());
        final Vector step = route.clone().multiply(1.0F / steps);

        for (int i = 0; i < steps; i++) {
          final Block between = event.getLocation().clone().add(route.subtract(step)).getBlock();
          if (between.getType() == Material.STAINED_GLASS) {
            blockIterator.remove();
            return;
          }
        }

        region.removePlacedBlock(block);
      } else {
        blockIterator.remove();
      }
    }

    PerkUtils.removeTntPlacer(event.getEntity());
  }

}
