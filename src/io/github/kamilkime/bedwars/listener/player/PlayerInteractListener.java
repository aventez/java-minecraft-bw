package io.github.kamilkime.bedwars.listener.player;

import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.buildmode.BuildModeInteractHandler;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.arena.GamePhase;
import io.github.kamilkime.bedwars.constant.arena.ShopType;
import io.github.kamilkime.bedwars.constant.gui.GuiConstants;
import io.github.kamilkime.bedwars.constant.gui.GuiType;
import io.github.kamilkime.bedwars.constant.task.ArenaEntityTask;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class PlayerInteractListener implements Listener {

  @EventHandler
  public void onInteract(final PlayerInteractEvent event) {
    final Player player = event.getPlayer();
    final User user = UserData.getUser(player.getUniqueId());

    final Arena arena = user.getCurrentArena();
    if (arena == null) {
      return;
    }

    if (BuildMode.isBuilding(user)) {
      BuildModeInteractHandler.handleInteract(event);
      return;
    }

    final Action action = event.getAction();
    if (action == Action.PHYSICAL) {
      return;
    }

    if (action == Action.RIGHT_CLICK_BLOCK) {
      final Block clickedBlock = event.getClickedBlock();
      if (clickedBlock.getType() == Material.BED_BLOCK && !player.isSneaking()) {
        event.setCancelled(true);
        return;
      }

      // BLOCK FROM OPENING OTHER TEAMS CHEST WHILE IT IS STILL ALIVE
      if (clickedBlock.getType() == Material.CHEST) {
        final Team userTeam = arena.getUserTeam(user);
        for (final Team team : arena.getTeams().values()) {
          if (!team.equals(userTeam) && clickedBlock.getLocation().equals(team.getTeamChest())) {
            if (team.getAliveTeamMemebers().size() == 0) {
              continue;
            }

            event.setCancelled(true);
            return;
          }
        }
      }

      final ItemStack handItem = event.getItem();
      if (handItem == null) {
        return;
      }

      if (handItem.getType() == Material.MONSTER_EGG) {
        event.setCancelled(true);

        final int entityNum = user.getCurrentArena().getArenaRegion().getUserEntityNumber(user, EntityType.IRON_GOLEM);
        if (entityNum >= BedWarsConstants.ENTITY_LIMITS.get(EntityType.IRON_GOLEM)) {
          event.setCancelled(true);
          player.sendMessage(Messages.ARENA_GOLEM_LIMIT);
          return;
        }

        final IronGolem ironGolem = player.getWorld().spawn(clickedBlock.getLocation().add(0.0D, 1.0D, 0.0D), IronGolem.class);

        ironGolem.setHealth(50.0D);
        ironGolem.setMaxHealth(50.0D);

        new ArenaEntityTask(ironGolem, arena, arena.getUserTeam(user), user, 90.0D).start();

        final ItemStack toTake = event.getItem().clone();
        toTake.setAmount(1);
        player.getInventory().removeItem(toTake);
      }
    }

    final ItemStack handItem = event.getItem();
    if (handItem == null) {
      return;
    }

    if (handItem.getType() == Material.FIREBALL) {
      final Vector direction = player.getLocation().getDirection();
      final Location spawnLocation = player.getEyeLocation().add(direction.clone());

      final Fireball fireball = spawnLocation.getWorld().spawn(spawnLocation, Fireball.class);
      fireball.setVelocity(direction.clone().multiply(1.5D));

      final ItemStack toTake = event.getItem().clone();
      toTake.setAmount(1);
      player.getInventory().removeItem(toTake);

      Bukkit.getScheduler().runTaskLater(BedWars.getPlugin(), fireball::remove, 30L * 20L);
    }
  }

  @EventHandler
  public void onEntityInteract(final PlayerInteractAtEntityEvent event) {
    final Player player = event.getPlayer();
    final User user = UserData.getUser(player.getUniqueId());

    final Arena arena = user.getCurrentArena();
    if (arena == null) {
      return;
    }

    if (BuildMode.isBuilding(user)) {
      BuildModeInteractHandler.handleEntityInteract(event);
      return;
    }

    // Disable armor taking from turret entities
    if (event.getRightClicked().getType() == EntityType.ARMOR_STAND) {
      event.setCancelled(true);
      return;
    }

    if (!arena.getUserTeam(user).isAlive(user)) {
      return;
    }
    
    if(event.getRightClicked().getName().equalsIgnoreCase(ShopType.TEAM_LOBBY.getShopName())) {
    	if(arena.getGamePhase() != GamePhase.LOBBY && arena.getGamePhase() != GamePhase.EMPTY) {
    		player.sendMessage(Messages.SHOP_LOBBY_DISABLED);
    		return;
    	} else {
    		GuiConstants.openGui(GuiType.TEAM_LOBBY, user);
    	}
    	return;
    }

    for (final Entry<Entity, ShopType> shop : arena.getArenaRegion().getShops().entrySet()) {
      if (event.getRightClicked().getUniqueId().equals(shop.getKey().getUniqueId())) {
        event.setCancelled(true);
        GuiConstants.openGui(shop.getValue().getGuiType(), user);
        return;
      }
    }
  }

}
