package io.github.kamilkime.bedwars.listener.player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaUtils;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.XpConstants;
import io.github.kamilkime.bedwars.constant.arena.GamePhase;
import io.github.kamilkime.bedwars.constant.user.ToolTier;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.TeamUtils;
import io.github.kamilkime.bedwars.turret.Turret;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class PlayerDeathListener implements Listener {

  private final Set<Material> transferable = new HashSet<>();
  private final Set<Material> toKeep = new HashSet<>();

  public PlayerDeathListener() {
    transferable.add(Material.DIAMOND);
    transferable.add(Material.EMERALD);
    transferable.add(Material.IRON_INGOT);
    transferable.add(Material.GOLD_INGOT);

    // KILL PLAYERS BELOW Y=0
    Bukkit.getScheduler().runTaskTimer(BedWars.getPlugin(), () -> {
      for (final Player player : Bukkit.getOnlinePlayers()) {
        if (player.getLocation().getY() >= 0.0D) {
          continue;
        }

        final User user = UserData.getUser(player.getUniqueId());
        if (!user.isInGame()) {
          continue;
        }

        player.setHealth(0.0D);
      }
    }, 0L, 10L);
  }

  @EventHandler
  public void onDeath(final PlayerDeathEvent event) {
    final Player victim = event.getEntity();
    final User victimUser = UserData.getUser(victim.getUniqueId());

    if (!victimUser.isInGame()) {
      return;
    }

    event.setDeathMessage(null);

    final Arena arena = victimUser.getCurrentArena();

    Entity killer = victim.getKiller();
    User killerUser = null;

    if (killer == null) {
      final EntityDamageEvent lastDamage = victim.getLastDamageCause();
      if (lastDamage instanceof EntityDamageByEntityEvent) {
        final EntityDamageByEntityEvent lastEntityDamage = (EntityDamageByEntityEvent) lastDamage;
        final Entity lastDamager = lastEntityDamage.getDamager();

        final User entityOwner = arena.getArenaRegion().getEntityUser(lastDamager);
        if (entityOwner != null) {
          killer = lastDamager;
          killerUser = entityOwner;
        }

        final Turret entityTurret = arena.getArenaRegion().getProjectileTurret(lastDamager);
        if (entityTurret != null) {
          killer = lastDamager;
          killerUser = entityTurret.getOwner();
        }
      }
    }

    if (killerUser == null && killer != null) {
      killerUser = UserData.getUser(killer.getUniqueId());
    }

    event.setKeepInventory(true);
    victimUser.addDeath();

    final Team victimTeam = arena.getUserTeam(victimUser);
    if (!victimTeam.hasBed()) {
      victimTeam.setAlive(victimUser, false);

      if (killerUser != null) {
        killerUser.addFinalKill();
        killerUser.addMoney(BedWarsConstants.MONEY_FOR_FINAL_KILL);
        killerUser.addXP(XpConstants.XP_FOR_FINAL_KILL);
      }

      if (killer == null) {
        arena.broadcast(Replacer.of(Messages.ARENA_FINAL_DEATH).with("{PLAYER}", victim.getName()).toString());
      } else {
        arena.broadcast(Replacer.of(Messages.ARENA_FINAL_KILL).with("{PLAYER}", victim.getName()).with("{KILLER}", killer.getName()).toString());
      }

      TeamUtils.tryEliminateTeam(arena, victimTeam);

      // DROP PLAYERS ENDERCHEST CONTENTS INTO THE TEAM GENERATOR
      final Location dropLocation = victimTeam.getTeamGenerator().getLocation();
      final World dropWorld = dropLocation.getWorld();

      for (final ItemStack item : victim.getEnderChest().getContents()) {
        if (item == null) {
          continue;
        }

        dropWorld.dropItemNaturally(dropLocation, item);
      }

      // HIDE PLAYER FROM ALIVE PLAYERS AND SHOW HIM THE SPECTATORS
      for (final Team team : arena.getTeams().values()) {
        for (final Entry<User, Boolean> member : team.getTeamMembers().entrySet()) {
          final Player memberPlayer = member.getKey().getPlayer();

          if (member.getValue()) {
            if (!memberPlayer.hasPermission("bedwars.hideplayer.bypass")) {
              memberPlayer.hidePlayer(victim);
            }
          } else {
            victim.showPlayer(memberPlayer);
          }
        }
      }
    } else {
      if (killerUser != null) {
        killerUser.addKill();
        killerUser.addMoney(BedWarsConstants.MONEY_FOR_KILL);
        killerUser.addXP(XpConstants.XP_FOR_KILL);
      }

      if (killer == null) {
        arena.broadcast(Replacer.of(Messages.ARENA_DEATH).with("{PLAYER}", victim.getName()).toString());
      } else {
        arena.broadcast(Replacer.of(Messages.ARENA_KILL).with("{PLAYER}", victim.getName()).with("{KILLER}", killer.getName()).toString());
      }
      new BukkitRunnable() {
        long iterations = BedWarsConstants.RESPAWN_DELAY;

        @Override
        public void run() {
          victim.sendTitle(Messages.ARENA_DEATH_TITLE, Messages.ARENA_DEATH_SUBTITLE
              .replace("%time%", String.valueOf(iterations / 20)), 0, 30, 5);
          iterations -= 20;
          if (iterations <= 0) {
            this.cancel();
          }
        }
      }.runTaskTimer(BedWars.getPlugin(), 0, 20);
    }

    if (arena.getGamePhase() != GamePhase.DEATHMATCH && ArenaUtils.tryFinish(arena)) {
      return;
    }

    final Inventory victimInventory = victim.getInventory();
    final Inventory killerInventory = (killer instanceof Player) ? ((Player) killer).getInventory() : null;
    final Map<Material, Integer> transferedItems = new HashMap<>();

    for (int slot = 0; slot < 36; slot++) {
      final ItemStack item = victimInventory.getItem(slot);
      if (item != null) {
        if (victimUser.getPickaxeTier() != ToolTier.NONE && item.equals(victimUser.getPickaxeTier().getPickaxeItem())) {
          victimUser.setPickaxeTier(ToolTier.getPrev(victimUser.getPickaxeTier()));
          victimInventory.setItem(slot, victimUser.getPickaxeTier().getPickaxeItem());
        } else if (victimUser.getAxeTier() != ToolTier.NONE && item.equals(victimUser.getAxeTier().getAxeItem())) {
          victimUser.setAxeTier(ToolTier.getPrev(victimUser.getAxeTier()));
          victimInventory.setItem(slot, victimUser.getAxeTier().getAxeItem());
        } else if (!this.toKeep.contains(item.getType())) {
          victimInventory.setItem(slot, new ItemStack(Material.AIR));
        }

        if (this.transferable.contains(item.getType()) && killerInventory != null) {
          killerInventory.addItem(item);
          transferedItems.put(item.getType(), transferedItems.getOrDefault(item.getType(), 0) + item.getAmount());
        }
      }
    }

    if (!transferedItems.isEmpty()) {
      killer.sendMessage(Messages.ARENA_ITEMS_TRANSFERED);
      for (final Entry<Material, Integer> entry : transferedItems.entrySet()) {
        killer.sendMessage(Replacer.of(Messages.ARENA_ITEM_TRANSFER_ENTRY).with("{ITEM}", entry.getKey().toString())
            .with("{AMOUNT}", entry.getValue()).toString());
      }
    }

    victim.removeMetadata("BedWarsTrapImmune", BedWars.getPlugin());
    victim.spigot().respawn();
  }

}
