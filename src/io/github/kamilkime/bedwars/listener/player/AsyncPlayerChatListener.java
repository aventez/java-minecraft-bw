package io.github.kamilkime.bedwars.listener.player;

import java.util.Iterator;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.XpConstants;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public final class AsyncPlayerChatListener implements Listener {

  @EventHandler
  public void onAsyncChat(final AsyncPlayerChatEvent event) {
    final User sender = UserData.getUser(event.getPlayer().getUniqueId());
    final Arena senderArena = sender.getCurrentArena();
    final Team senderTeam = senderArena != null ? senderArena.getUserTeam(sender) : null;

    final boolean senderInGame = sender.isInGame();
    final boolean senderAlive = senderInGame && senderTeam.getAliveTeamMemebers().contains(sender);
    final boolean isGlobalShout = event.getMessage().startsWith("!");
    if (isGlobalShout) {
      event.setMessage(event.getMessage().replaceFirst("!", ""));
    }

    String bwPrefix;
    if (senderInGame) {
      if (senderAlive && isGlobalShout) {
        bwPrefix = senderTeam.getTeamColor().getChatPrefix();
      } else if (senderAlive) {
        bwPrefix = "&e[TEAM]";
      } else {
        bwPrefix = "&8[SPECTATOR]";
      }
    } else {
      bwPrefix = Replacer.of(XpConstants.getChatPrefix(sender.getLevel())).with("{LVL}", sender.getLevel()).toString();
    }

    // event.setFormat(Replacer.of(event.getFormat()).with("{BW-PREFIX}", bwPrefix).toString());
    event.setFormat(DataUtils.color(bwPrefix + "&r ") + event.getFormat());

    final Iterator<Player> recipientIterator = event.getRecipients().iterator();
    while (recipientIterator.hasNext()) {
      final User recipient = UserData.getUser(recipientIterator.next().getUniqueId());
      final Arena recipientArena = recipient.getCurrentArena();
      final Team recipientTeam = recipientArena != null ? recipientArena.getUserTeam(recipient) : null;

      final boolean recipientInGame = recipient.isInGame();
      final boolean recipientAlive = recipientInGame && recipientTeam.getAliveTeamMemebers().contains(recipient);

      // Both not in game
      if (!recipientInGame && !senderInGame) {
        continue;
      }

      // One of them not in game
      if ((!recipientInGame && senderInGame) || (recipientInGame && !senderInGame)) {
        recipientIterator.remove();
        continue;
      }

      // Not playing on the same arena
      if (!recipientArena.equals(senderArena)) {
        recipientIterator.remove();
        continue;
      }

      // One of them alive and the other not
      if ((!recipientAlive && senderAlive) || (recipientAlive && !senderAlive)) {
        recipientIterator.remove();
        continue;
      }

      // Both alive and in different teams
      if (recipientAlive && senderAlive && !recipientTeam.equals(senderTeam)) {
        if (!isGlobalShout) {
          recipientIterator.remove();
        }
      }
    }
  }

}
