package io.github.kamilkime.bedwars.listener.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public final class PlayerJoinListener implements Listener {

  @EventHandler
  public void onJoin(final PlayerJoinEvent event) {
    final Player player = event.getPlayer();

    User user = UserData.getUser(player.getUniqueId());
    if (user == null) {
      user = new User(player.getUniqueId());
    }

    // GIVE A NEW SCOREBOARD TO THE PLAYER
    player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());

    // ADD UUID TO UUID-NAME CACHE
    UserData.createMatch(player.getName(), player.getUniqueId());

    // TELEPORT TO SPAWN
    player.teleport(BedWars.getMainSpawn());

    // HIDE CURRENTLY PLAYING PLAYERS
    for (final Player onlinePlayer : Bukkit.getOnlinePlayers()) {
      if (onlinePlayer.equals(player)) {
        continue;
      }

      final User onlineUser = UserData.getUser(onlinePlayer.getUniqueId());
      if (!onlineUser.isInGame()) {
        continue;
      }

      if (!onlinePlayer.hasPermission("bedwars.hideplayer.bypass")) {
        onlinePlayer.hidePlayer(player);
      }

      if (!player.hasPermission("bedwars.hideplayer.bypass")) {
        player.hidePlayer(onlinePlayer);
      }
    }
  }

}
