package io.github.kamilkime.bedwars.listener.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import io.github.kamilkime.bedwars.arena.ArenaUtils;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public final class PlayerQuitListener implements Listener {

  @EventHandler
  public void onQuit(final PlayerQuitEvent event) {
    this.handleEvent(event);
  }

  @EventHandler
  public void onKick(final PlayerKickEvent event) {
    this.handleEvent(event);
  }

  private void handleEvent(PlayerEvent event) {
    final User user = UserData.getUser(event.getPlayer().getUniqueId());
    if (user.getCurrentArena() != null) {
      if (BuildMode.isBuilding(user)) {
        BuildMode.removeBuilder(user);
        return;
      }

      ArenaUtils.kickPlayer(user, true, true);
    }
  }

}
