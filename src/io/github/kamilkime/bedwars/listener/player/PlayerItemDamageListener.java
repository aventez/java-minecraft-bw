package io.github.kamilkime.bedwars.listener.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemDamageEvent;

import io.github.kamilkime.bedwars.user.UserData;

public class PlayerItemDamageListener implements Listener {

  @EventHandler
  public void onItemDamage(final PlayerItemDamageEvent event) {
    if (UserData.getUser(event.getPlayer().getUniqueId()).isInGame()) {
      event.setCancelled(true);
    }
  }

}
