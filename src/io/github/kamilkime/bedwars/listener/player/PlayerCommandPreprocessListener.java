package io.github.kamilkime.bedwars.listener.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class PlayerCommandPreprocessListener implements Listener {

  @EventHandler
  public void onCommandPreprocess(final PlayerCommandPreprocessEvent event) {
    final User user = UserData.getUser(event.getPlayer().getUniqueId());
    if (!user.isInGame()) {
      return;
    }

    final String[] cmdParts = event.getMessage().toLowerCase().split(" ");
    boolean allow = false;

    cmdLoop:
    for (final String[] allowedCmd : BedWarsConstants.ALLOWED_COMMANDS) {
      if (cmdParts.length < allowedCmd.length) {
        continue;
      }

      for (int i = 0; i < allowedCmd.length; i++) {
        if (!allowedCmd[i].equals(cmdParts[i])) {
          continue cmdLoop;
        }
      }

      allow = true;
      break;
    }

    if (!allow) {
      event.getPlayer().sendMessage(Messages.USER_CMD_NOT_ALLOWED);
      event.setCancelled(true);
    }
  }

}
