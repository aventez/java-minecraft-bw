package io.github.kamilkime.bedwars.listener.player;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.upgrade.TeamUpgradeUtils;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class PlayerRespawnListener implements Listener {

  private final Map<UUID, Map<Integer, ItemStack>> itemCache = new HashMap<>();

  @EventHandler
  public void onRespawn(final PlayerRespawnEvent event) {
    final Player player = event.getPlayer();
    final User user = UserData.getUser(player.getUniqueId());

    if (!user.isInGame()) {
      return;
    }

    final Arena arena = user.getCurrentArena();
    final Team team = arena.getUserTeam(user);

    if (team.isAlive(user)) {
      player.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
      TeamUpgradeUtils.addSharpness(user);

      final PlayerInventory playerInventory = player.getInventory();
      final Map<Integer, ItemStack> playerItems = new HashMap<>();

      for (int slot = 0; slot <= 40; slot++) {
        final ItemStack item = playerInventory.getItem(slot);
        if (item == null || item.getType() == Material.AIR) {
          continue;
        }

        playerItems.put(slot, item);
      }

      this.itemCache.put(player.getUniqueId(), playerItems);

      Bukkit.getScheduler().runTaskLater(BedWars.getPlugin(), () -> {
        if (!user.isInGame()) {
          return;
        }

        player.setGameMode(GameMode.SURVIVAL);
        TeamUpgradeUtils.addHaste(user);

        for (final Entry<Integer, ItemStack> item : this.itemCache.get(player.getUniqueId()).entrySet()) {
          playerInventory.setItem(item.getKey(), item.getValue());
        }

        player.teleport(team.getTeamSpawn());
      }, BedWarsConstants.RESPAWN_DELAY);
    }

    player.getInventory().clear();

    for (final PotionEffect effect : player.getActivePotionEffects()) {
      player.removePotionEffect(effect.getType());
    }

    player.setGameMode(GameMode.SPECTATOR);
    event.setRespawnLocation(arena.getArenaRegion().getSpectatorLobbyLocation());
  }

}
