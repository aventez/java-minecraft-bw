package io.github.kamilkime.bedwars.listener.player;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import io.github.kamilkime.bedwars.BedWars;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;

public class Trail implements Listener {
	
	//Listeners
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if (BedWars.getArrowTrails().hasTrail(p)) {
			BedWars.getArrowTrails().removeTrail(p);
		}
	}
	
	private EnumParticle e;

	private ArrayList<Arrow> arrows = new ArrayList<>();
	
	public Trail(EnumParticle e) {
		this.e = e;
	}

	public void addArrow(Arrow a) {
		arrows.add(a);
	}

	public void tick() {
		for (Arrow a : arrows) {
			if (a.isOnGround() || a.isDead() || a == null) {
				arrows.remove(a);
				return;
			} else {
				particle(a.getLocation());
			}
		}
	}

	private void particle(Location loc) {
		PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(e, true, (float) loc.getX(),
				(float) loc.getY(), (float) loc.getZ(), 0, 0, 0, 0, 15, null);
		for (Player p : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}
	}
}
