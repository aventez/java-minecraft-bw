package io.github.kamilkime.bedwars.listener.player;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.metadata.FixedMetadataValue;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.user.UserData;

public class ItemConsumeListener implements Listener {

  @EventHandler
  public void onConsume(final PlayerItemConsumeEvent e) {
    if (!UserData.getUser(e.getPlayer().getUniqueId()).isInGame()) {
      return;
    }
    if (e.getItem().getType() == Material.POTION) {
      Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> e.getPlayer().getInventory().remove(Material.GLASS_BOTTLE));
    } else if (e.getItem().getType() == Material.MILK_BUCKET) {
      Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> e.getPlayer().getInventory().remove(Material.BUCKET));
      e.getPlayer().sendMessage(Messages.ARENA_TRAP_IMMUNITY_ON);
      e.getPlayer().setMetadata("BedWarsTrapImmune", new FixedMetadataValue(BedWars.getPlugin(), true));
      Bukkit.getScheduler().runTaskLater(BedWars.getPlugin(), () -> e.getPlayer().removeMetadata("BedWarsTrapImmune", BedWars.getPlugin()), 20 * 60);
    }
  }

}
