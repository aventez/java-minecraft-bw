package io.github.kamilkime.bedwars.listener.inventory;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.user.UserData;

// PREVENT CRAFTING, ENCHANTING AND ANVIL REPAIRS
public class ItemCraftListener implements Listener {

  private final ItemStack airItem = new ItemStack(Material.AIR);

  @EventHandler
  public void onPrepareCraft(final PrepareItemCraftEvent event) {
    if (this.isViewerInGame(event)) {
      event.getInventory().setResult(this.airItem);
    }
  }

  @EventHandler
  public void onCraft(final CraftItemEvent event) {
    if (this.isViewerInGame(event)) {
      event.setCancelled(true);
    }
  }

  @EventHandler
  public void onPrepareAnvil(final PrepareAnvilEvent event) {
    if (this.isViewerInGame(event)) {
      event.setResult(this.airItem);
    }
  }

  @EventHandler
  public void onPrepareEnchant(final PrepareItemEnchantEvent event) {
    if (this.isViewerInGame(event)) {
      event.setCancelled(true);
    }
  }

  private boolean isViewerInGame(final InventoryEvent event) {
    final List<HumanEntity> viewers = event.getViewers();
    if (viewers == null || viewers.isEmpty()) {
      return false;
    }

    for (final HumanEntity human : viewers) {
      if (UserData.getUser(human.getUniqueId()).isInGame()) {
        return true;
      }
    }

    return false;
  }

}
