package io.github.kamilkime.bedwars.listener.inventory;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import io.github.kamilkime.bedwars.constant.gui.GuiConstants;
import io.github.kamilkime.bedwars.constant.gui.GuiType;
import io.github.kamilkime.bedwars.data.QuickShopItem;
import io.github.kamilkime.bedwars.gui.GuiItem;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class GuiClickListener implements Listener {

  @EventHandler
  public void onClickQuickShop(InventoryClickEvent e) {
    final Inventory inventory = e.getClickedInventory();
    if (inventory == null) {
      return;
    }
    final GuiType guiType = GuiType.getGuiType(inventory.getTitle());
    if (guiType != null && inventory.getType() == InventoryType.CHEST) {
      switch (guiType) {
        case PERSONAL_QUICK_SHOP:
          e.setCancelled(true);
          for (Map<Integer, GuiItem> entries : GuiConstants.GUI_TEMPLATES.values()) {
            for (GuiItem item : entries.values()) {
              User user = UserData.getUser(e.getWhoClicked().getUniqueId());
              if (!item.getItem(user).equals(e.getCurrentItem())) {
                continue;
              }
              item.handleClick(user, e);
              return;
            }
          }
          break;
        case QUICK_SHOP_PERSONALIZER:
          e.setCancelled(true);
          if (e.getSlot() == 4 || e.getCurrentItem().getType() == Material.AIR) {
            return;
          }
          User user = UserData.getUser(e.getWhoClicked().getUniqueId());
          QuickShopItem item = user.getQuickShopData().getEventQuickShopItem();
          if (item == null) {
            e.getWhoClicked().closeInventory();
            return;
          }
          user.getQuickShopData().getQuickShopItems().put(e.getSlot(), item);
          user.getQuickShopData().setEventQuickShopItem(null);
          GuiConstants.openGui(GuiType.PERSONAL_BLOCK, user);
          e.getWhoClicked().closeInventory();
          return;
      }
    }
  }

  @EventHandler
  public void onClick(final InventoryClickEvent event) {
    final Inventory inventory = event.getClickedInventory();
    if (inventory == null) {
      return;
    }

    final GuiType guiType = GuiType.getGuiType(inventory.getTitle());
    if (guiType != null && inventory.getType() == InventoryType.CHEST) {
      if (GuiConstants.GUI_TEMPLATES.get(guiType) == null) {
        return;
      }
      final GuiItem item = GuiConstants.GUI_TEMPLATES.get(guiType).get(event.getSlot());
      if (item != null) {
        item.handleClick(UserData.getUser(event.getWhoClicked().getUniqueId()), event);
      }

      event.setResult(Result.DENY);
      event.setCancelled(true);
    }

    // STOP PLAYERS FROM TAKING OFF THEIR ARMOR
    if (inventory.getType() == InventoryType.PLAYER) {
      if (event.getSlot() >= 36 && event.getSlot() <= 39) {
        event.setResult(Result.DENY);
        event.setCancelled(true);
      }
    }
  }

  @EventHandler
  public void onInteract(final InventoryInteractEvent event) {
    final Inventory inventory = event.getInventory();
    if (inventory == null) {
      return;
    }

    final GuiType guiType = GuiType.getGuiType(inventory.getTitle());
    if (guiType != null && inventory.getType() == InventoryType.CHEST) {
      event.setResult(Result.DENY);
      event.setCancelled(true);
    }
  }

}
