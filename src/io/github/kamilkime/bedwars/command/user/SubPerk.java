package io.github.kamilkime.bedwars.command.user;

import java.util.UUID;

import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.perk.Perk;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class SubPerk implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (args.length < 4) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/user perk <perkName> <userName> <level>").toString());
      return;
    }

    final UUID uuid = UserData.getUUID(args[2], true);
    if (uuid == null) {
      sender.sendMessage(Messages.CMD_NO_USER_FOUND);
      return;
    }

    final Perk perk = Perk.fromName(args[1]);
    if (perk == null) {
      sender.sendMessage(Messages.CMD_NO_PERK_FOUND);
      return;
    }

    int value;
    try {
      value = Integer.parseInt(args[3]);
    } catch (final NumberFormatException exception) {
      sender.sendMessage(Replacer.of(Messages.CMD_NOT_A_NUMBER).with("{ARG}", args[3]).toString());
      return;
    }

    final User user = UserData.getUser(uuid);
    user.setPerkLevel(perk, value);

    sender.sendMessage(Replacer.of(Messages.CMD_USER_PERK_CHANGED).with("{USER}", UserData.getName(uuid))
        .with("{VALUE}", value).with("{PERK}", perk.getPerkName()).toString());
  }

  @Override
  public String getPermission() {
    return "bedwars.user.perk";
  }

}
