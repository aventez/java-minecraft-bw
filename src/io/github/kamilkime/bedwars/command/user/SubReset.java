package io.github.kamilkime.bedwars.command.user;

import java.util.UUID;

import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class SubReset implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (args.length < 2) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/user reset <userName>").toString());
      return;
    }

    final UUID uuid = UserData.getUUID(args[1], true);
    if (uuid == null) {
      sender.sendMessage(Messages.CMD_NO_USER_FOUND);
      return;
    }

    final User user = UserData.getUser(uuid);
    user.setDeaths(0L);
    user.setDestroyedBeds(0L);
    user.setFinalKills(0L);
    user.setKills(0L);
    user.setLevel(0L);
    user.setMoney(0L);
    user.setXP(0L);
    user.resetPerks();

    sender.sendMessage(Replacer.of(Messages.CMD_USER_RESET).with("{USER}", UserData.getName(uuid)).toString());

  }

  @Override
  public String getPermission() {
    return "bedwars.user.reset";
  }

}
