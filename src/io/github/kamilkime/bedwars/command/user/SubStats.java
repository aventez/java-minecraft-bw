package io.github.kamilkime.bedwars.command.user;

import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.perk.Perk;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class SubStats implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (args.length < 2) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/user stats <userName>").toString());
      return;
    }

    final UUID uuid = UserData.getUUID(args[1], true);
    if (uuid == null) {
      sender.sendMessage(Messages.CMD_NO_USER_FOUND);
      return;
    }

    final User user = UserData.getUser(uuid);

    sender.sendMessage(DataUtils.color("&8---------------------------------------------------"));
    sender.sendMessage(DataUtils.color("&aUUID&8: &7") + uuid.toString());
    sender.sendMessage(DataUtils.color("&aName&8: &7") + UserData.getName(uuid));
    sender.sendMessage("");
    sender.sendMessage(DataUtils.color("&aKills&8: &7") + user.getKills());
    sender.sendMessage(DataUtils.color("&aFinal kills&8: &7") + user.getFinalKills());
    sender.sendMessage(DataUtils.color("&aBeds destroyed&8: &7") + user.getDestroyedBeds());
    sender.sendMessage(DataUtils.color("&aDeaths&8: &7") + user.getDeaths());

    if (!user.getPerkLevels().isEmpty()) {
      sender.sendMessage("");
      sender.sendMessage(DataUtils.color("&aPerks&8:"));
      for (final Entry<Perk, Integer> perkEntry : user.getPerkLevels().entrySet()) {
        sender.sendMessage(DataUtils.color(" &8� &a" + perkEntry.getKey().getPerkName() + "&8: &7level " + DataUtils.toRoman(perkEntry.getValue())));
      }
    }

    sender.sendMessage("");
    sender.sendMessage(DataUtils.color("&aMoney&8: &7") + user.getMoney());
    sender.sendMessage(DataUtils.color("&aTotal XP&8: &7") + user.getXP());
    sender.sendMessage(DataUtils.color("&aLevel&8: &7") + user.getLevel());
    sender.sendMessage(DataUtils.color("&8---------------------------------------------------"));
  }

  @Override
  public String getPermission() {
    return "bedwars.user.stats";
  }

}
