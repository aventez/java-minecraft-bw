package io.github.kamilkime.bedwars.command.user;

import java.util.UUID;

import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class SubXp implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (args.length < 4) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/user xp <mode> <userName> <value>").toString());
      return;
    }

    final UUID uuid = UserData.getUUID(args[2], true);
    if (uuid == null) {
      sender.sendMessage(Messages.CMD_NO_USER_FOUND);
      return;
    }

    long value;
    try {
      value = Long.parseLong(args[3]);
    } catch (final NumberFormatException exception) {
      sender.sendMessage(Replacer.of(Messages.CMD_NOT_A_NUMBER).with("{ARG}", args[3]).toString());
      return;
    }

    final User user = UserData.getUser(uuid);
    long newValue;

    switch (args[1].toLowerCase()) {
      case "add":
        newValue = user.getXP() + value;
        break;
      case "remove":
        newValue = user.getXP() - value;
        break;
      case "set":
        newValue = value;
        break;
      default:
        sender.sendMessage(Messages.CMD_UNKNOWN_MODE);
        return;
    }

    sender.sendMessage(Replacer.of(Messages.CMD_USER_XP_CHANGED).with("{USER}", UserData.getName(uuid))
        .with("{VALUE}", user.setXP(newValue)).toString());
  }

  @Override
  public String getPermission() {
    return "bedwars.user.xp";
  }


}
