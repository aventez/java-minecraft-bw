package io.github.kamilkime.bedwars.command;

import org.bukkit.command.CommandSender;

public interface SubCommand {

  void execute(final CommandSender sender, final String[] args);

  String getPermission();

}
