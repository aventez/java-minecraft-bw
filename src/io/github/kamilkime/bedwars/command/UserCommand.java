package io.github.kamilkime.bedwars.command;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.command.user.SubBeds;
import io.github.kamilkime.bedwars.command.user.SubDeaths;
import io.github.kamilkime.bedwars.command.user.SubFinalKills;
import io.github.kamilkime.bedwars.command.user.SubHelp;
import io.github.kamilkime.bedwars.command.user.SubKills;
import io.github.kamilkime.bedwars.command.user.SubMoney;
import io.github.kamilkime.bedwars.command.user.SubPerk;
import io.github.kamilkime.bedwars.command.user.SubReset;
import io.github.kamilkime.bedwars.command.user.SubStats;
import io.github.kamilkime.bedwars.command.user.SubXp;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.Replacer;

public class UserCommand implements CommandExecutor {

  private final Map<String, SubCommand> userSubCommands = new HashMap<>();

  public UserCommand() {
    this.register(new SubBeds(), "beds");
    this.register(new SubDeaths(), "deaths");
    this.register(new SubFinalKills(), "finalkills");
    this.register(new SubHelp(), "help", "pomoc");
    this.register(new SubKills(), "kills");
    this.register(new SubMoney(), "money");
    this.register(new SubPerk(), "perk");
    this.register(new SubReset(), "reset");
    this.register(new SubStats(), "stats");
    this.register(new SubXp(), "xp");
  }

  @Override
  public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
    SubCommand sub = null;
    if (args.length != 0) {
      sub = this.userSubCommands.get(args[0].toLowerCase());
    }

    if (sub == null) {
      sender.sendMessage(Replacer.of(Messages.CMD_UNKNOWN_ARGUMENT).with("{HELP}", "/user help").toString());
      return true;
    }

    if (!sender.hasPermission(sub.getPermission())) {
      sender.sendMessage(Messages.CMD_NO_PERMISSION);
      return true;
    }

    sub.execute(sender, args);
    return true;
  }

  private void register(final SubCommand sub, final String... args) {
    for (final String arg : args) {
      this.userSubCommands.put(arg, sub);
    }
  }

}
