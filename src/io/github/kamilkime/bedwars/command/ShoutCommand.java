package io.github.kamilkime.bedwars.command;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class ShoutCommand implements CommandExecutor {

  @Override
  public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
    if (!sender.hasPermission("bedwars.shout")) {
      sender.sendMessage(Messages.CMD_NO_PERMISSION);
      return true;
    }

    if (!(sender instanceof Player)) {
      sender.sendMessage(Messages.CMD_NOT_ONLINE);
      return true;
    }

    if (args.length == 0) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/shout <message>").toString());
      return true;
    }

    final User user = UserData.getUser(((Player) sender).getUniqueId());
    if (!user.isInGame()) {
      sender.sendMessage(Messages.CMD_NOT_IN_GAME_TO_SHOUT);
      return true;
    }

    final Arena arena = user.getCurrentArena();
    final Team team = arena.getUserTeam(user);

    if (!team.isAlive(user)) {
      sender.sendMessage(Messages.CMD_SHOUT_NOT_ALIVE);
      return true;
    }

    final String message = DataUtils.color("&7[SHOUT] &" + team.getTeamColor().getColorCode() + ": &r")
        + StringUtils.join(args, " ", 0, args.length);

    for (final Team targetTeam : arena.getTeams().values()) {
      for (final User target : team.getAliveTeamMemebers()) {
        if (targetTeam.isAlive(target)) {
          target.getPlayer().sendMessage(message);
        }
      }
    }

    return true;
  }

}
