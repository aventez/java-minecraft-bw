package io.github.kamilkime.bedwars.command;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.command.arena.SubCheck;
import io.github.kamilkime.bedwars.command.arena.SubCopy;
import io.github.kamilkime.bedwars.command.arena.SubCreate;
import io.github.kamilkime.bedwars.command.arena.SubDelete;
import io.github.kamilkime.bedwars.command.arena.SubDisable;
import io.github.kamilkime.bedwars.command.arena.SubEdit;
import io.github.kamilkime.bedwars.command.arena.SubEnable;
import io.github.kamilkime.bedwars.command.arena.SubHelp;
import io.github.kamilkime.bedwars.command.arena.SubJoin;
import io.github.kamilkime.bedwars.command.arena.SubLeave;
import io.github.kamilkime.bedwars.command.arena.SubList;
import io.github.kamilkime.bedwars.command.arena.SubMainLobby;
import io.github.kamilkime.bedwars.command.arena.SubPaste;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.Replacer;

public class ArenaCommand implements CommandExecutor {

  private final Map<String, SubCommand> arenaSubCommands = new HashMap<>();

  public ArenaCommand() {
    this.register(new SubCheck(), "check");
    this.register(new SubCopy(), "copy");
    this.register(new SubCreate(), "create");
    this.register(new SubDelete(), "delete", "remove", "del", "rem");
    this.register(new SubDisable(), "disable");
    this.register(new SubEdit(), "edit");
    this.register(new SubEnable(), "enable");
    this.register(new SubHelp(), "help", "pomoc");
    this.register(new SubJoin(), "join");
    this.register(new SubLeave(), "leave");
    this.register(new SubList(), "list");
    this.register(new SubPaste(), "paste");
    this.register(new SubMainLobby(), "setmainlobby");
  }

  @Override
  public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
    SubCommand sub = null;
    if (args.length != 0) {
      sub = this.arenaSubCommands.get(args[0].toLowerCase());
    }

    if (sub == null) {
      sender.sendMessage(Replacer.of(Messages.CMD_UNKNOWN_ARGUMENT).with("{HELP}", "/arena help").toString());
      return true;
    }

    if (!sender.hasPermission(sub.getPermission())) {
      sender.sendMessage(Messages.CMD_NO_PERMISSION);
      return true;
    }

    sub.execute(sender, args);
    return true;
  }

  private void register(final SubCommand sub, final String... args) {
    for (final String arg : args) {
      this.arenaSubCommands.put(arg, sub);
    }
  }

}
