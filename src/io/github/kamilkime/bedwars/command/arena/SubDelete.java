package io.github.kamilkime.bedwars.command.arena;

import java.io.File;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.BedWarsData;
import io.github.kamilkime.bedwars.data.Replacer;

public class SubDelete implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (args.length < 2) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/arena delete <arenaName>").toString());
      return;
    }

    final Arena arena = ArenaData.getArena(args[1], true);
    if (arena == null) {
      sender.sendMessage(Messages.CMD_NO_ARENA_FOUND);
      return;
    }

    if (arena.isEnabled()) {
      sender.sendMessage(Messages.CMD_ARENA_NOT_DISABLED);
      return;
    }

    for (final Entity shop : arena.getArenaRegion().getShops().keySet()) {
      if (!shop.getLocation().getChunk().isLoaded()) {
        shop.getLocation().getChunk().load(true);
      }

      shop.remove();
    }

    Bukkit.getScheduler().runTaskAsynchronously(BedWars.getPlugin(), () -> {
      final Pair<Location, Location> bounds = arena.getArenaRegion().getArenaBounds();
      final Location minBound = bounds.getLeft();
      final Location maxBound = bounds.getRight();
      final World world = bounds.getLeft().getWorld();

      final int minX = Math.min(minBound.getBlockX(), maxBound.getBlockX());
      final int maxX = Math.max(minBound.getBlockX(), maxBound.getBlockX());
      final int minY = Math.min(minBound.getBlockY(), maxBound.getBlockY());
      final int maxY = Math.max(minBound.getBlockY(), maxBound.getBlockY());
      final int minZ = Math.min(minBound.getBlockZ(), maxBound.getBlockZ());
      final int maxZ = Math.max(minBound.getBlockZ(), maxBound.getBlockZ());

      for (int y = maxY; y >= minY; y--) {
        for (int x = minX; x <= maxX; x++) {
          for (int z = minZ; z <= maxZ; z++) {
            final Block block = new Location(world, x, y, z).getBlock();
            Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> block.setType(Material.AIR));
          }
        }
      }
    });

    final File arenaFile = new File(BedWarsData.ARENAS_FOLDER, arena.getArenaName() + ".arena");
    if (arenaFile.exists()) {
      arenaFile.delete();
    }

    ArenaData.removeArena(arena.getArenaName());
    sender.sendMessage(Replacer.of(Messages.CMD_ARENA_DELETED).with("{ARENA}", arena.getArenaName()).toString());
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.delete";
  }

}
