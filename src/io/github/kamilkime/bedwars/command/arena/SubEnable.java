package io.github.kamilkime.bedwars.command.arena;

import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.arena.ArenaUtils;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.Replacer;

public class SubEnable implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (args.length < 2) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/arena enable <arenaName>").toString());
      return;
    }

    final Arena arena = ArenaData.getArena(args[1], true);
    if (arena == null) {
      sender.sendMessage(Messages.CMD_NO_ARENA_FOUND);
      return;
    }

    if (arena.isEnabled()) {
      sender.sendMessage(Messages.CMD_ARENA_NOT_DISABLED);
      return;
    }

    final String canBeEnabled = arena.canBeEnabled();
    if (canBeEnabled != null) {
      sender.sendMessage(Replacer.of(Messages.CMD_ARENA_INCORRECT).with("{REASON}", canBeEnabled).toString());
      return;
    }

    if (BuildMode.isWorkedOn(arena)) {
      sender.sendMessage(Messages.CMD_CANT_ENABLE_WHILE_BUILD);
      return;
    }

    arena.setEnabled(true);
    ArenaUtils.resetArena(arena, true);

    sender.sendMessage(Replacer.of(Messages.CMD_ARENA_ENABLED).with("{ARENA}", arena.getArenaName()).toString());
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.enable";
  }

}
