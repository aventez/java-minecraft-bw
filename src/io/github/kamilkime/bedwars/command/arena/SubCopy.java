package io.github.kamilkime.bedwars.command.arena;

import java.io.File;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.schematic.ArenaSchematic;

public class SubCopy implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(Messages.CMD_NOT_ONLINE);
      return;
    }

    if (args.length < 3) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/arena copy <arenaName> <schemName>").toString());
      return;
    }

    final Arena arena = ArenaData.getArena(args[1], true);
    if (arena == null) {
      sender.sendMessage(Messages.CMD_NO_ARENA_FOUND);
      return;
    }

    if (arena.isEnabled()) {
      sender.sendMessage(Messages.CMD_ARENA_NOT_DISABLED);
      return;
    }

    final String canBeEnabled = arena.canBeEnabled();
    if (canBeEnabled != null) {
      sender.sendMessage(Replacer.of(Messages.CMD_CANT_COPY_ARENA).with("{REASON}", canBeEnabled).toString());
      return;
    }

    final File weSchemFile = new File(BedWars.getPlugin().getDataFolder(), "../WorldEdit/schematics/" + args[2] + ".schematic");
    if (!weSchemFile.exists()) {
      sender.sendMessage(Replacer.of(Messages.CMD_NO_WE_SCHEMATIC_FOUND).with("{PATH}", weSchemFile.getAbsolutePath()).toString());
      return;
    }

    final File arenaSchemFile = new File(ArenaSchematic.SCHEMATICS_FOLDER, arena.getArenaName() + ".arenaschem");
    if (arenaSchemFile.exists()) {
      sender.sendMessage(Replacer.of(Messages.CMD_SCHEMATIC_ALREADY_EXISTS).with("{ARENA}", arena.getArenaName()).toString());
      return;
    }

    final Location baseLocation = ((Player) sender).getLocation().getBlock().getLocation();
    if (ArenaSchematic.saveSchematic(arena, baseLocation, weSchemFile, arenaSchemFile)) {
      sender.sendMessage(Replacer.of(Messages.CMD_ARENA_COPIED).with("{ARENA}", arena.getArenaName()).with("{SCHEM}", weSchemFile.getName()).toString());
    } else {
      sender.sendMessage(Messages.CMD_ARENA_COPY_ERROR);
    }
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.copy";
  }

}
