package io.github.kamilkime.bedwars.command.arena;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pl.plajerlair.commonsbox.minecraft.serialization.LocationSerializer;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.data.DataUtils;

/**
 * @author Plajer
 * <p>
 * Created at 04.03.2019
 */
public class SubMainLobby implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    sender.sendMessage(DataUtils.color("&8---------------------------------------------------"));

    sender.sendMessage(DataUtils.color("&cMain lobby set here!"));

    BedWars.getPlugin().getConfig().set("Main-Spawn-Location", LocationSerializer.locationToString(((Player) sender).getLocation()));
    BedWars.getPlugin().saveConfig();

    sender.sendMessage(DataUtils.color("&8---------------------------------------------------"));
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.setmainlobby";
  }

}
