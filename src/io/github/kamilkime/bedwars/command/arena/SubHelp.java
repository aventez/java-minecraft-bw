package io.github.kamilkime.bedwars.command.arena;

import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;

public class SubHelp implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    for (final String message : Messages.CMD_ARENA_HELP) {
      sender.sendMessage(message);
    }
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.help";
  }

}
