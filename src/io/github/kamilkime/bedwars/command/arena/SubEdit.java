package io.github.kamilkime.bedwars.command.arena;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class SubEdit implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(Messages.CMD_NOT_ONLINE);
      return;
    }

    if (args.length < 2) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/arena edit <arenaName>").toString());
      return;
    }

    final Arena arena = ArenaData.getArena(args[1], true);
    if (arena == null) {
      sender.sendMessage(Messages.CMD_NO_ARENA_FOUND);
      return;
    }

    if (arena.isEnabled()) {
      sender.sendMessage(Messages.CMD_ARENA_NOT_DISABLED);
      return;
    }

    final Player player = (Player) sender;
    final User user = UserData.getUser(player.getUniqueId());

    if (BuildMode.isBuilding(user)) {
      BuildMode.removeBuilder(user);
      sender.sendMessage(Replacer.of(Messages.CMD_STOP_BUILDING).with("{ARENA}", arena.getArenaName()).toString());
    } else {
      BuildMode.addBuilder(user, ArenaData.getArena(args[1], true));
      sender.sendMessage(Replacer.of(Messages.CMD_START_BUILDING).with("{ARENA}", arena.getArenaName()).toString());
    }
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.edit";
  }

}
