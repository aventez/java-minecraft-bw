package io.github.kamilkime.bedwars.command.arena;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.arena.JoinDenyReason;
import io.github.kamilkime.bedwars.constant.team.TeamColor;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class SubJoin implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(Messages.CMD_NOT_ONLINE);
      return;
    }

    if (args.length < 2) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/arena join <arenaName> [teamColor]").toString());
      return;
    }

    final Arena arena = ArenaData.getArena(args[1], true);
    if (arena == null) {
      sender.sendMessage(Messages.CMD_NO_ARENA_FOUND);
      return;
    }

    TeamColor teamColor = null;
    if (args.length >= 3) {
      try {
        teamColor = TeamColor.valueOf(args[2].toUpperCase());
      } catch (final Exception exception) {
        sender.sendMessage(Replacer.of(Messages.CMD_NO_TEAM_FOUND).with("{NAME}", args[2].toUpperCase()).toString());
        return;
      }
    }

    final User user = UserData.getUser(((Player) sender).getUniqueId());
    if (user.isInGame()) {
      sender.sendMessage(Messages.CMD_ALREADY_IN_GAME);
      return;
    }

    if (BuildMode.isBuilding(user)) {
      sender.sendMessage(Messages.CMD_BUILDING_ARENA);
      return;
    }

    if (teamColor != null) {
      final Team team = arena.getTeams().get(teamColor);
      if (team == null) {
        sender.sendMessage(Messages.CMD_NO_TEAM_ON_ARENA);
        return;
      }

      final JoinDenyReason reason = arena.join(user, team);
      if (reason != null) {
        sender.sendMessage(Replacer.of(Messages.CMD_CANT_JOIN).with("{REASON}", reason.getReasonMessage()).toString());
      }
    } else {
      final JoinDenyReason reason = arena.join(user);
      if (reason != null) {
        sender.sendMessage(Replacer.of(Messages.CMD_CANT_JOIN).with("{REASON}", reason.getReasonMessage()).toString());
      }
    }
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.join";
  }

}
