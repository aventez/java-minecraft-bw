package io.github.kamilkime.bedwars.command.arena;

import com.sk89q.worldedit.bukkit.selections.Selection;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.arena.ArenaMode;
import io.github.kamilkime.bedwars.data.BedWarsData;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.user.UserData;

public class SubCreate implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(Messages.CMD_NOT_ONLINE);
      return;
    }

    if (args.length < 3) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/arena create <arenaName> <arenaMode>").toString());
      return;
    }

    final String arenaName = args[1];
    if (ArenaData.getArena(arenaName, true) != null) {
      sender.sendMessage(Messages.CMD_ARENA_ALREADY_EXISTS);
      return;
    }

    ArenaMode arenaMode;
    try {
      arenaMode = ArenaMode.valueOf(args[2].toUpperCase());
    } catch (final Exception exception) {
      sender.sendMessage(Replacer.of(Messages.CMD_NO_ARENA_MODE_FOUND).with("{NAME}", args[2].toUpperCase()).toString());
      return;
    }

    final Player player = (Player) sender;
    final Selection selection = BedWars.getWE().getSelection(player);

    if (selection == null) {
      sender.sendMessage(Messages.CMD_NO_WE_SELECTION);
      return;
    }

    final Location minBound = selection.getMinimumPoint();
    final Location maxBound = selection.getMaximumPoint();

    if (minBound == null || maxBound == null) {
      sender.sendMessage(Messages.CMD_NO_WE_SELECTION);
      return;
    }

    final Arena arena = new Arena(arenaName, arenaMode, minBound, maxBound);
    sender.sendMessage(Replacer.of(Messages.CMD_ARENA_CREATED).with("{NAME}", arenaName).toString());

    BedWarsData.saveArena(arena);
    BuildMode.addBuilder(UserData.getUser(player.getUniqueId()), arena);
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.create";
  }

}
