package io.github.kamilkime.bedwars.command.arena;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.arena.ArenaUtils;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public class SubLeave implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(Messages.CMD_NOT_ONLINE);
      return;
    }

    final Player player = (Player) sender;
    final User user = UserData.getUser(player.getUniqueId());

    if (!user.isInGame()) {
      sender.sendMessage(Messages.CMD_NOT_IN_GAME_TO_LEAVE);
      return;
    }

    ArenaUtils.kickPlayer(user, true, true);
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.leave";
  }

}
