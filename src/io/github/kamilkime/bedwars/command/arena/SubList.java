package io.github.kamilkime.bedwars.command.arena;

import org.bukkit.command.CommandSender;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.data.DataUtils;

public class SubList implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    sender.sendMessage(DataUtils.color("&8---------------------------------------------------"));

    for (final Arena arena : ArenaData.getSortedArenas()) {
      sender.sendMessage(DataUtils.color("&7� &a" + arena.getArenaName() + "&8, &a" + arena.getArenaMode().toString() + " mode&8, &a"
          + arena.getPlayerNumber() + "/" + arena.getArenaMode().getMaxPlayers() + " playing&8, &aphase: "
          + (arena.isEnabled() ? arena.getGamePhase().toString() : "&cDISABLED")));
    }

    sender.sendMessage(DataUtils.color("&8---------------------------------------------------"));
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.list";
  }

}
