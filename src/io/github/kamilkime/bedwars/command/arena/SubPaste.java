package io.github.kamilkime.bedwars.command.arena;

import java.io.File;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.command.SubCommand;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.arena.GamePhase;
import io.github.kamilkime.bedwars.data.BedWarsData;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.schematic.ArenaSchematic;

public class SubPaste implements SubCommand {

  @Override
  public void execute(final CommandSender sender, final String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(Messages.CMD_NOT_ONLINE);
      return;
    }

    if (args.length < 3) {
      sender.sendMessage(Replacer.of(Messages.CMD_CORRECT_USAGE).with("{USAGE}", "/arena paste <newArenaName> <schemName>").toString());
      return;
    }

    if (ArenaData.getArena(args[1], true) != null) {
      sender.sendMessage(Messages.CMD_ARENA_ALREADY_EXISTS);
      return;
    }

    final File arenaSchemFile = new File(ArenaSchematic.SCHEMATICS_FOLDER, args[2] + ".arenaschem");
    if (!arenaSchemFile.exists()) {
      sender.sendMessage(Replacer.of(Messages.CMD_NO_ARENA_SCHEMATIC_FOUND).with("{SCHEM}", args[2]).toString());
      return;
    }

    final Location baseLocation = ((Player) sender).getLocation().getBlock().getLocation();
    final Arena arena = ArenaSchematic.loadSchematic(arenaSchemFile, baseLocation, args[1]);

    if (arena != null) {
      arena.setEnabled(true);

      arena.setGamePhase(GamePhase.EMPTY);
      arena.startArena();

      BedWarsData.saveArena(arena);

      sender.sendMessage(Replacer.of(Messages.CMD_ARENA_PASTED).with("{ARENA}", arena.getArenaName()).with("{SCHEM}", args[2]).toString());
    } else {
      sender.sendMessage(Messages.CMD_ARENA_PASTE_ERROR);
    }
  }

  @Override
  public String getPermission() {
    return "bedwars.arena.paste";
  }

}
