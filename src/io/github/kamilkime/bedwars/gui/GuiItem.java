package io.github.kamilkime.bedwars.gui;

import java.util.function.BiConsumer;
import java.util.function.Function;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.user.User;

public final class GuiItem {

  private final Function<User, ItemStack> itemFunction;
  private final BiConsumer<User, InventoryClickEvent> itemAction;

  public GuiItem(final Function<User, ItemStack> itemFunction) {
    this.itemFunction = itemFunction;
    this.itemAction = (u, i) -> {
    };
  }

  public GuiItem(final Function<User, ItemStack> itemFunction, final BiConsumer<User, InventoryClickEvent> itemAction) {
    this.itemFunction = itemFunction;
    this.itemAction = itemAction != null ? itemAction : (u, i) -> {
    };
  }

  public ItemStack getItem(final User user) {
    return this.itemFunction.apply(user);
  }

  public void handleClick(final User user, final InventoryClickEvent event) {
    this.itemAction.accept(user, event);
  }

}
