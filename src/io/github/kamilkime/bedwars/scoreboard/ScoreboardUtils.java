package io.github.kamilkime.bedwars.scoreboard;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.arena.ArenaMode;
import io.github.kamilkime.bedwars.constant.arena.GamePhase;
import io.github.kamilkime.bedwars.constant.team.TeamColor;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.data.TimeUtils;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;

public final class ScoreboardUtils {

  private static final String OBJECTIVE_NAME = DataUtils.color("  &b&lBedWars v1.0  ");
  private static final String SERVER_NAME = DataUtils.color("&ekamilki.me");
  private static final String SEPARATOR_ONE = DataUtils.color("&r");
  private static final String SEPARATOR_TWO = DataUtils.color("&r&r");
  private static final String SEPARATOR_THREE = DataUtils.color("&r&r&r");
  private static final String WAITING = DataUtils.color("Waiting...");
  private static final String STARTING_IN = DataUtils.color("Starting in &a");
  private static final String PLAYERS = DataUtils.color("Players: &a");
  private static final String MODE = DataUtils.color("Mode: &a");
  private static final String TEAM = DataUtils.color("Team: &a");
  private static final String ALIVE_TEAMS = DataUtils.color("&4&lAlive teams:");
  private static final String DEATHMATCH = DataUtils.color("&c&lDEATHMATCH");
  private static final String GAME_FINISHED = DataUtils.color("&a&lGAME FINISHED");
  private static final String NEXT_PHASE_IN = DataUtils.color(" in &a");
  private static final String CLEAR = DataUtils.color("&r&f");
  private static final String HAS_BED = DataUtils.color("&a&l\u2713");
  private static final String DOESNT_HAVE_BED = DataUtils.color("&c&lx");
  private static final String IS_USER_TEAM = DataUtils.color("   &7&lYOU");

  private ScoreboardUtils() {
  }

  public static void startScoreboard(final User user, final Team userTeam, final Arena arena) {
    if (!user.isInGame()) {
      return;
    }

    final Player player = user.getPlayer();
    Scoreboard scoreboard = player.getScoreboard();

    if (scoreboard == null) {
      scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
      player.setScoreboard(scoreboard);
    }

    for (final Team team : arena.getTeams().values()) {
      org.bukkit.scoreboard.Team scoreTeam = scoreboard.getTeam("bwt-" + team.getTeamColor().toString());

      if (scoreTeam == null) {
        scoreTeam = scoreboard.registerNewTeam("bwt-" + team.getTeamColor().toString());

        scoreTeam.setPrefix(team.getTeamColor().getScoreboardPrefix());

        scoreTeam.setAllowFriendlyFire(false);
        scoreTeam.setCanSeeFriendlyInvisibles(true);
      } else {
        for (final String entry : scoreTeam.getEntries()) {
          scoreTeam.removeEntry(entry);
        }
      }

      for (final User member : team.getAliveTeamMemebers()) {
        scoreTeam.addEntry(member.getPlayer().getName());
      }
    }

    updateScoreboard(user, userTeam, arena);
  }

  public static void stopScoreboard(final User user) {
    final Player player = user.getPlayer();
    final Scoreboard scoreboard = player.getScoreboard();

    if (scoreboard == null) {
      return;
    }

    final Objective gameObjective = scoreboard.getObjective("bw-game");
    if (gameObjective != null) {
      gameObjective.unregister();
    }

    scoreboard.getTeams().stream().filter(team -> team.getName().startsWith("bwt-")).forEach(org.bukkit.scoreboard.Team::unregister);
  }

  public static void updateScoreboard(final User user, final Team userTeam, final Arena arena) {
    if (!user.isInGame()) {
      return;
    }
    
    final ArenaMode arenaMode = arena.getArenaMode();
    final GamePhase arenaPhase = arena.getGamePhase();

    final Player player = user.getPlayer();
    final Scoreboard scoreboard = player.getScoreboard();

    Objective gameObjective = scoreboard.getObjective("bw-game");
    if (gameObjective == null) {
      gameObjective = scoreboard.registerNewObjective("bw-game", "dummy");
      gameObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
      gameObjective.setDisplayName(OBJECTIVE_NAME);
    }

    final Map<String, Integer> newEntries = new HashMap<>();
    int score = 1;

    newEntries.put(SERVER_NAME, score++);
    newEntries.put(SEPARATOR_ONE, score++);

    switch (arenaPhase) {
      case EMPTY: {
        final String players = arena.getPlayerNumber() + "/" + arenaMode.getMaxPlayers();

        newEntries.put(WAITING, score++);
        newEntries.put(SEPARATOR_TWO, score++);
        newEntries.put(PLAYERS + players, score++);
        newEntries.put(MODE + arenaMode.toString(), score++);
        newEntries.put(TEAM + userTeam.getTeamColor().toString(), score++);
        
        break;
      }

      case LOBBY: {
        final String players = arena.getPlayerNumber() + "/" + arenaMode.getMaxPlayers();
        final String timeLeft = TimeUtils.getAbbreviatedTime(arenaPhase.getPhaseDuration() - arena.getPhaseDuration());

        newEntries.put(STARTING_IN + timeLeft, score++);
        newEntries.put(SEPARATOR_TWO, score++);
        newEntries.put(PLAYERS + players, score++);
        newEntries.put(MODE + arenaMode.toString(), score++);
        newEntries.put(TEAM + userTeam.getTeamColor().toString(), score++);

        break;
      }

      case DEATHMATCH: {
        for (final Team team : arena.getTeams().values()) {
          if (team.getAliveTeamMemebers().size() > 0) {
            final TeamColor teamColor = team.getTeamColor();
            newEntries.put(teamColor.getScoreboardPrefix() + CLEAR + teamColor.getSimpleName(), score++);
          }
        }

        newEntries.put(ALIVE_TEAMS, score++);
        newEntries.put(SEPARATOR_TWO, score++);
        newEntries.put(DEATHMATCH, score++);

        break;
      }

      case FINISH: {
        newEntries.put(GAME_FINISHED, score++);
        break;
      }

      default: {
        final String nextPhase = GamePhase.next(arenaPhase).getPhaseName();
        final String timeLeft = TimeUtils.getColonTime(arenaPhase.getPhaseDuration() - arena.getPhaseDuration());

        for (final Team team : arena.getTeams().values()) {
          String hasBed = DOESNT_HAVE_BED;
          if (team.hasBed() && team.getAliveTeamMemebers().size() > 0) {
            hasBed = HAS_BED;
          }

          String isUserTeam = "";
          if (team.equals(userTeam)) {
            isUserTeam = IS_USER_TEAM;
          }

          final TeamColor teamColor = team.getTeamColor();
          newEntries.put(teamColor.getScoreboardPrefix() + CLEAR + teamColor.getSimpleName() + ": " + hasBed + isUserTeam, score++);
        }

        newEntries.put(SEPARATOR_TWO, score++);
        newEntries.put(nextPhase + NEXT_PHASE_IN + timeLeft, score++);

        break;
      }
    }

    newEntries.put(SEPARATOR_THREE, score++);
    newEntries.put(ChatColor.GRAY + TimeUtils.getDate(), score++);

    final Set<String> currentEntries = scoreboard.getEntries();
    for (final String currentEntry : currentEntries) {
      if (!newEntries.containsKey(currentEntry)) {
        scoreboard.resetScores(currentEntry);
      }
    }

    for (final Entry<String, Integer> newEntry : newEntries.entrySet()) {
      final Score entryScore = gameObjective.getScore(newEntry.getKey());

      if (!currentEntries.contains(newEntry.getKey()) || entryScore.getScore() != newEntry.getValue()) {
        entryScore.setScore(newEntry.getValue());
      }
    }
  }

  public static void updateScoreboard(final Arena arena) {
    arena.getTeams().values().forEach(team -> team.getTeamMembers().keySet().forEach(user -> updateScoreboard(user, team, arena)));
  }

}
