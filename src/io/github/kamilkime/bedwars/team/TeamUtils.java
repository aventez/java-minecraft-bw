package io.github.kamilkime.bedwars.team;

import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.XpConstants;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.user.User;

public final class TeamUtils {

  private static final BlockFace[] BED_FACES = new BlockFace[] {BlockFace.SOUTH, BlockFace.WEST, BlockFace.NORTH, BlockFace.EAST};

  private TeamUtils() {
  }

  public static void tryEliminateTeam(final Arena arena, final Team team) {
    final Set<User> aliveMembers = team.getAliveTeamMemebers();
    if (aliveMembers.size() > 0) {
      return;
    }

    // DROP TEAM CHEST CONTENTS INTO THE TEAM GENERATOR
    final Location dropLocation = team.getTeamGenerator().getLocation();
    final World dropWorld = dropLocation.getWorld();
    final Chest teamChest = (Chest) team.getTeamChest().getBlock().getState();

    for (final ItemStack item : teamChest.getInventory().getContents()) {
      if (item == null) {
        continue;
      }

      dropWorld.dropItemNaturally(dropLocation, item);
    }

    teamChest.getInventory().clear();
    arena.broadcast(Replacer.of(Messages.ARENA_TEAM_ELIMINATED).with("{TEAM}", team.getTeamColor().getTeamName()).toString());
  }

  public static void destroyBed(final Arena arena, final Team team, final User breaker) {
    if (!team.hasBed()) {
      return;
    }

    breakBedBlocks(team.getTeamBed());
    team.setBedStatus(false);

    if (breaker != null && team.getAliveTeamMemebers().size() > 0) {
      breaker.addDestroyedBed();
      breaker.addXP(XpConstants.XP_FOR_BED_DESTROY);
      breaker.addMoney(BedWarsConstants.MONEY_FOR_BED_DESTROY);

      final char breakerColorCode = arena.getUserTeam(breaker).getTeamColor().getColorCode();
      final String breakerName = DataUtils.color("&" + breakerColorCode + breaker.getPlayer().getName() + "&r");

      breaker.getCurrentArena().broadcast(Replacer.of(Messages.ARENA_BED_DESTROYED).with("{TEAM}", team.getTeamColor().getTeamName())
          .with("{BREAKER}", breakerName).toString());

      for (User user : team.getAliveTeamMemebers()) {
        user.getPlayer().sendTitle(Messages.ARENA_BED_DESTROYED_TITLE, Messages.ARENA_BED_DESTROYED_SUBTITLE, 5, 30, 5);
      }
    }
  }

  public static void breakBedBlocks(final Pair<Location, Location> teamBed) {
    teamBed.getLeft().getBlock().setType(Material.AIR);
    teamBed.getRight().getBlock().setType(Material.AIR);

    Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> {
      for (final Entity entity : teamBed.getLeft().getWorld().getNearbyEntities(teamBed.getLeft(), 2.5D, 2.0D, 2.5D)) {
        if (entity instanceof Item && ((Item) entity).getItemStack().getType() == Material.BED) {
          entity.remove();
        }
      }
    });
  }

  public static BlockFace getBedFaceFromData(final byte data) {
    return BED_FACES[data];
  }

}
