package io.github.kamilkime.bedwars.team;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Location;
import org.bukkit.block.Block;

import io.github.kamilkime.bedwars.constant.gui.TeamLobbyUpgrade;
import io.github.kamilkime.bedwars.constant.team.TeamColor;
import io.github.kamilkime.bedwars.constant.team.upgrade.TeamUpgrade;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.user.User;

public final class Team {

  private final TeamColor teamColor;

  private Map<TeamUpgrade, Integer> upgradesPurchased = new HashMap<>();

  private final Map<User, Boolean> teamMembers = new HashMap<>();
  private final Set<TeamUpgrade> teamUpgrades = new LinkedHashSet<>();
  private final Set<TeamLobbyUpgrade> teamLobbyUpgrades = new LinkedHashSet<>();

  private boolean hasBed = true;

  private ItemGenerator teamGenerator;

  private Location teamSpawn;
  private Location teamChest;
  private Location deathmatchSpawn;

  private Pair<Location, Byte> teamBed;

  public Team(final TeamColor teamColor) {
    this.teamColor = teamColor;
  }

  public int getUpgradesPurchased(TeamUpgrade upgrade) {
    return upgradesPurchased.getOrDefault(upgrade, 0);
  }

  public void setUpgradesPurchased(TeamUpgrade upgrade, int amount) {
    upgradesPurchased.put(upgrade, amount);
  }

  public int getPriceForNextTrapUpgrade(TeamUpgrade upgrade) {
    return (getUpgradesPurchased(upgrade) * 2) + 1;
  }

  public boolean hasBed() {
    return this.hasBed;
  }

  public void setBedStatus(final boolean hasBed) {
    this.hasBed = hasBed;
  }

  public ItemGenerator getTeamGenerator() {
    return this.teamGenerator;
  }

  public void setTeamGenerator(final ItemGenerator teamGenerator) {
    this.teamGenerator = teamGenerator;
  }

  public Location getTeamSpawn() {
    return this.teamSpawn == null ? null : this.teamSpawn.clone();
  }

  public void setTeamSpawn(final Location teamSpawn) {
    this.teamSpawn = teamSpawn;
  }

  public Location getTeamChest() {
    return this.teamChest == null ? null : this.teamChest.clone();
  }

  public void setTeamChest(final Location teamChest) {
    this.teamChest = teamChest;
  }

  public Location getDeathmatchSpawn() {
    return this.deathmatchSpawn == null ? null : this.deathmatchSpawn.clone();
  }

  public void setDeathmatchSpawn(final Location deathmatchSpawn) {
    this.deathmatchSpawn = deathmatchSpawn;
  }

  public byte getTeamBedData() {
    if (this.teamBed == null) {
      return -1;
    }

    return this.teamBed.getRight();
  }

  public Pair<Location, Location> getTeamBed() {
    if (this.teamBed == null) {
      return null;
    }

    final Block adjacentBlock = this.teamBed.getLeft().getBlock().getRelative(TeamUtils.getBedFaceFromData(this.teamBed.getRight()));

    return Pair.of(this.teamBed.getLeft().clone(), adjacentBlock.getLocation());
  }

  public void setTeamBed(final Pair<Location, Byte> teamBed) {
    this.teamBed = teamBed;
  }

  public TeamColor getTeamColor() {
    return this.teamColor;
  }

  public Map<User, Boolean> getTeamMembers() {
    return new HashMap<>(this.teamMembers);
  }

  public Set<User> getAliveTeamMemebers() {
    return this.teamMembers.keySet().stream().filter(this::isAlive).collect(Collectors.toSet());
  }

  public void addTeamMember(final User user) {
    this.teamMembers.put(user, true);
  }

  public void removeTeamMember(final User user) {
    this.teamMembers.remove(user);
  }

  public boolean isAlive(final User user) {
    return this.teamMembers.getOrDefault(user, false);
  }

  public void setAlive(final User user, final boolean alive) {
    this.teamMembers.put(user, alive);
  }

  public Set<TeamUpgrade> getTeamUpgrades() {
    return new LinkedHashSet<>(this.teamUpgrades);
  }

  public void addTeamUpgrade(final TeamUpgrade upgrade) {
    this.teamUpgrades.add(upgrade);
  }

  public void removeTeamUpgrade(final TeamUpgrade upgrade) {
    this.teamUpgrades.remove(upgrade);
  }

  public boolean hasTeamUpgrade(final TeamUpgrade upgrade) {
    return this.teamUpgrades.contains(upgrade);
  }
  
  //lobby
  public Set<TeamLobbyUpgrade> getTeamLobbyUpgrades() {
    return new LinkedHashSet<>(this.teamLobbyUpgrades);
  }
  public void addTeamLobbyUpgrade(final TeamLobbyUpgrade upgrade) {
    this.teamLobbyUpgrades.add(upgrade);
  }
  public void removeTeamUpgrade(final TeamLobbyUpgrade upgrade) {
    this.teamLobbyUpgrades.remove(upgrade);
  }
  public boolean hasTeamLobbyUpgrade(final TeamLobbyUpgrade upgrade) {
	  return this.teamLobbyUpgrades.contains(upgrade);
  }

  public void clearUpgrades() {
    this.teamUpgrades.clear();
    this.teamLobbyUpgrades.clear();
    this.upgradesPurchased.clear();
  }

}
