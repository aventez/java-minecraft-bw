package io.github.kamilkime.bedwars.team.upgrade;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import io.github.kamilkime.bedwars.constant.gui.ItemBuilder;
import io.github.kamilkime.bedwars.constant.team.upgrade.TeamUpgrade;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;

public final class TeamUpgradeUtils {

  private static final Set<Material> SWORDS = new HashSet<>();

  static {
    SWORDS.add(Material.WOOD_SWORD);
    SWORDS.add(Material.STONE_SWORD);
    SWORDS.add(Material.IRON_SWORD);
    SWORDS.add(Material.GOLD_SWORD);
    SWORDS.add(Material.DIAMOND_SWORD);
  }

  private TeamUpgradeUtils() {
  }

  public static void addProtection(final User user) {
    final Player player = user.getPlayer();
    final Team userTeam = user.getCurrentArena().getUserTeam(user);

    int enchantLvl;

    if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_IV)) {
      enchantLvl = 4;
    } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_III)) {
      enchantLvl = 3;
    } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_II)) {
      enchantLvl = 2;
    } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_I)) {
      enchantLvl = 1;
    } else {
      return;
    }

    final EntityEquipment eq = player.getEquipment();

    eq.setHelmet(ItemBuilder.of(eq.getHelmet()).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, enchantLvl).build());
    eq.setChestplate(ItemBuilder.of(eq.getChestplate()).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, enchantLvl).build());
    eq.setLeggings(ItemBuilder.of(eq.getLeggings()).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, enchantLvl).build());
    eq.setBoots(ItemBuilder.of(eq.getBoots()).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, enchantLvl).build());
  }

  public static void addSharpness(final User user) {
    final Player player = user.getPlayer();
    final Team userTeam = user.getCurrentArena().getUserTeam(user);

    int enchantLvl;

    if (userTeam.hasTeamUpgrade(TeamUpgrade.SHARPNESS_I)) {
      enchantLvl = 1;
    } else {
      return;
    }

    final PlayerInventory playerInventory = player.getInventory();
    for (int slot = 0; slot < 36; slot++) {
      final ItemStack item = playerInventory.getItem(slot);
      if (item != null && SWORDS.contains(item.getType())) {
        playerInventory.setItem(slot, ItemBuilder.of(item).enchant(Enchantment.DAMAGE_ALL, enchantLvl).build());
      }
    }
  }

  public static void addHaste(final User user) {
    final Player player = user.getPlayer();
    final Team userTeam = user.getCurrentArena().getUserTeam(user);

    int hasteLvl;

    if (userTeam.hasTeamUpgrade(TeamUpgrade.HASTE_II)) {
      hasteLvl = 1;
    } else if (userTeam.hasTeamUpgrade(TeamUpgrade.HASTE_I)) {
      hasteLvl = 0;
    } else {
      return;
    }

    player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, hasteLvl, false, false), true);
  }

}
