package io.github.kamilkime.bedwars.turret.impl;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.gui.ItemBuilder;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.turret.Turret;
import io.github.kamilkime.bedwars.user.User;

public class ArrowTurret extends Turret {

  private static final ItemStack ITEM;
  private static final ItemStack GUI_ITEM;

  static {
    ITEM = ItemBuilder.of(Material.DISPENSER).name("&6Arrow turret").lore("&9Range: &710 blocks", "&9Speed: &760 SPM").build();
    GUI_ITEM = ItemBuilder.of(ITEM).addLoreLines("", "&9Cost: &a1 emerald", "&cYou can only have one active turret!").build();
  }

  public ArrowTurret(final Location turretLocation, final Arena arena, final Team ownerTeam, final User owner) {
    super(turretLocation, arena, ownerTeam, owner, 20L, 40.0D, 10.D, 1.0D, 1.0D, 0.75D, Arrow.class);
  }

  public static ItemStack getItem() {
    return ITEM.clone();
  }

  public static ItemStack getGuiItem() {
    return GUI_ITEM.clone();
  }

  @Override
  public void customizeTurret(final ArmorStand turret) {
    turret.setHelmet(new ItemStack(Material.DISPENSER));
    turret.setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
    turret.setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
    turret.setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
  }

  @Override
  public void customizeProjectile(final Entity projectile) {
    ((Arrow) projectile).setKnockbackStrength(0);
  }

}
