package io.github.kamilkime.bedwars.turret.impl;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.gui.ItemBuilder;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.turret.Turret;
import io.github.kamilkime.bedwars.user.User;

public class FireballTurret extends Turret {

  private static final ItemStack ITEM;
  private static final ItemStack GUI_ITEM;

  static {
    ITEM = ItemBuilder.of(Material.FURNACE).name("&6Fireball turret").lore("&9Range: &715 blocks", "&9Speed: &76 SPM").build();
    GUI_ITEM = ItemBuilder.of(ITEM).addLoreLines("", "&9Cost: &a1 emerald", "&cYou can only have one active turret!").build();
  }

  public FireballTurret(final Location turretLocation, final Arena arena, final Team ownerTeam, final User owner) {
    super(turretLocation, arena, ownerTeam, owner, 10L * 20L, 50.0D, 15.D, 1.3D, 1.3D, 0.85D, Fireball.class);
  }

  public static ItemStack getItem() {
    return ITEM.clone();
  }

  public static ItemStack getGuiItem() {
    return GUI_ITEM.clone();
  }

  @Override
  public void customizeTurret(final ArmorStand turret) {
    turret.setHelmet(new ItemStack(Material.FURNACE));
    turret.setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
    turret.setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
    turret.setBoots(new ItemStack(Material.LEATHER_BOOTS));
  }

  @Override
  public void customizeProjectile(final Entity projectile) {
    ((Fireball) projectile).setYield(0.0F);
  }

}
