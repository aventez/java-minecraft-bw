package io.github.kamilkime.bedwars.turret;

import java.util.Locale;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.turret.impl.ArrowTurret;
import io.github.kamilkime.bedwars.turret.impl.FireballTurret;
import io.github.kamilkime.bedwars.user.User;

public abstract class Turret {

  protected final Location turretLocation;
  protected final Arena arena;
  protected final Team ownerTeam;
  protected final User owner;

  protected final long timerInterval;

  protected final double turretMaxHealth;
  protected final double rangeSquared;

  // DETERMINES THE PROJECTILE SPEED
  protected final double projectileVectorMultiplier;

  // DETERMINES HOW FAR FROM THE TURRET THE PROJECTILE WILL SPAWN
  protected final double projectilePositionVectorMultiplier;

  // DETERMINES THE Y LEVEL OF THE SPAWNED PROJECTILE
  protected final double projectilePositionVectorY;

  protected final Class<? extends Entity> projectileClass;

  protected ArmorStand turretEntity;
  protected BukkitTask turretTask;

  public Turret(final Location turretLocation, final Arena arena, final Team ownerTeam, final User owner, final long timerInterval,
                final double turretMaxHealth, final double range, final double projectileVectorMultiplier,
                final double projectilePositionVectorMultiplier, final double projectilePositionVectorY,
                final Class<? extends Entity> projectileClass) {
    this.turretLocation = turretLocation;
    this.arena = arena;
    this.ownerTeam = ownerTeam;
    this.owner = owner;

    this.timerInterval = timerInterval;

    this.turretMaxHealth = turretMaxHealth;
    this.rangeSquared = Math.pow(range + 1.0D, 2.0D);

    this.projectileVectorMultiplier = projectileVectorMultiplier;
    this.projectilePositionVectorMultiplier = projectilePositionVectorMultiplier;
    this.projectilePositionVectorY = projectilePositionVectorY;

    this.projectileClass = projectileClass;
  }

  public static ItemStack getItem() {
    return null;
  }

  public static ItemStack getGuiItem() {
    return null;
  }

  public static Turret getTurret(final ItemStack item, final Location turretLocation, final Arena arena, final Team ownerTeam, final User owner) {
    if (item == null) {
      return null;
    }

    if (ArrowTurret.getItem().isSimilar(item)) {
      return new ArrowTurret(turretLocation, arena, ownerTeam, owner);
    } else if (FireballTurret.getItem().isSimilar(item)) {
      return new FireballTurret(turretLocation, arena, ownerTeam, owner);
    }

    return null;
  }

  public ArmorStand getEntity() {
    return this.turretEntity;
  }

  public User getOwner() {
    return this.owner;
  }

  public Team getOwnerTeam() {
    return this.ownerTeam;
  }

  public void place() {
    final ArmorStand turret = this.turretLocation.getWorld().spawn(this.turretLocation, ArmorStand.class);

    turret.setAI(false);
    turret.setArms(false);
    turret.setBasePlate(true);
    turret.setMarker(false);
    turret.setRemoveWhenFarAway(false);
    turret.setSmall(true);

    turret.setMaxHealth(this.turretMaxHealth);
    turret.setHealth(this.turretMaxHealth);

    turret.setCustomNameVisible(true);

    this.customizeTurret(turret);
    this.turretEntity = turret;

    this.updateName();
  }

  public void start() {
    this.turretTask = Bukkit.getScheduler().runTaskTimer(BedWars.getPlugin(), () -> {
      double distanceToShoot = this.rangeSquared + 1.0D;
      Location toShoot = null;

      for (final User target : this.arena.getArenaPlayers()) {
        final Team targetTeam = this.arena.getUserTeam(target);
        if (this.ownerTeam.equals(targetTeam) || !targetTeam.isAlive(target)) {
          continue;
        }

        final Location targetLocation = target.getPlayer().getEyeLocation();
        final double targetDistance = targetLocation.distanceSquared(this.turretLocation);

        if (targetDistance <= this.rangeSquared && distanceToShoot > targetDistance) {
          distanceToShoot = targetDistance;
          toShoot = targetLocation;
        }
      }

      if (toShoot != null) {
        final Vector shootVector = toShoot.subtract(this.turretLocation).toVector();
        final Location spawnLocation = this.turretLocation.clone().add(shootVector.clone().normalize()
            .multiply(this.projectilePositionVectorMultiplier).setY(this.projectilePositionVectorY));

        final EulerAngle pose = new EulerAngle(0.0D, shootVector.angle(new Vector(0.0D, 0.0D, 1.0D)), 0.0D);

        turretEntity.setBodyPose(pose);
        turretEntity.setHeadPose(pose);
        turretEntity.setLeftArmPose(pose);
        turretEntity.setLeftLegPose(pose);
        turretEntity.setRightArmPose(pose);
        turretEntity.setRightLegPose(pose);

        final Entity projectile = toShoot.getWorld().spawn(spawnLocation, this.projectileClass);

        customizeProjectile(projectile);
        projectile.setVelocity(shootVector.clone().multiply(this.projectileVectorMultiplier));

        this.arena.getArenaRegion().addTurretProjectile(projectile, this);

        Bukkit.getScheduler().runTaskLater(BedWars.getPlugin(), () -> {
          this.arena.getArenaRegion().removeTurretProjectile(projectile);
          projectile.remove();
        }, 30L * 20L);
      }
    }, this.timerInterval, this.timerInterval);
  }

  public void stop() {
    this.turretTask.cancel();
    this.turretEntity.remove();
  }

  public void updateName() {
    this.turretEntity.setCustomName(DataUtils.color(String.format(Locale.US,
        "&" + this.ownerTeam.getTeamColor().getColorCode() + "%.1f &7HP", this.turretEntity.getHealth())));
  }

  public abstract void customizeTurret(final ArmorStand turret);

  public abstract void customizeProjectile(final Entity projectile);

}
