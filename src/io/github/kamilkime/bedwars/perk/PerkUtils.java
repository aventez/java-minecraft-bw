package io.github.kamilkime.bedwars.perk;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Entity;

import io.github.kamilkime.bedwars.user.User;

public final class PerkUtils {

  private static final Map<Entity, User> BOMBER_PLACED_TNT = new HashMap<>();

  private PerkUtils() {
  }

  public static User getTntPlacer(final Entity exploding) {
    return BOMBER_PLACED_TNT.get(exploding);
  }

  public static void addTntPlacer(final Entity tnt, final User placer) {
    BOMBER_PLACED_TNT.put(tnt, placer);
  }

  public static void removeTntPlacer(final Entity tnt) {
    BOMBER_PLACED_TNT.remove(tnt);
  }

}
