package io.github.kamilkime.bedwars.arena;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import io.github.kamilkime.bedwars.constant.arena.ShopType;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.turret.Turret;
import io.github.kamilkime.bedwars.user.User;

public final class ArenaRegion {

  private final Pair<Location, Location> arenaBounds;
  private final Set<ItemGenerator> generators = new HashSet<>();
  private final Set<Block> placedBlocks = new HashSet<>();
  private final Map<Entity, ShopType> shops = new HashMap<>();
  private final Map<User, Turret> turrets = new HashMap<>();
  private final Map<Entity, User> arenaEntities = new HashMap<>();
  private final Map<Entity, Turret> turretProjectiles = new HashMap<>();
  private Location mainLobbyLocation;
  private Location spectatorLobbyLocation;
  private Location shopLobbyLocation;
  private Location arenaCenter;

  public ArenaRegion(final Location minBound, final Location maxBound) {
    this.arenaBounds = Pair.of(minBound, maxBound);
  }

  public Location getShopLobbyLocation() {
	  return this.shopLobbyLocation == null ? null : this.shopLobbyLocation.clone();
  }
  
  public void setShopLobbyLocation(final Location shopLocation) {
	  this.shopLobbyLocation = shopLocation;
  }
  
  public Location getMainLobbyLocation() {
    return this.mainLobbyLocation == null ? null : this.mainLobbyLocation.clone();
  }

  public void setMainLobbyLocation(final Location lobbyLocation) {
    this.mainLobbyLocation = lobbyLocation;
  }

  public Location getSpectatorLobbyLocation() {
    return this.spectatorLobbyLocation == null ? null : this.spectatorLobbyLocation.clone();
  }

  public void setSpectatorLobbyLocation(final Location lobbyLocation) {
    this.spectatorLobbyLocation = lobbyLocation;
  }

  public Location getArenaCenter() {
    return this.arenaCenter == null ? null : this.arenaCenter.clone();
  }

  public void setArenaCenter(final Location centerLocation) {
    this.arenaCenter = centerLocation;
  }

  public Pair<Location, Location> getArenaBounds() {
    if (this.arenaBounds == null) {
      return null;
    }

    if (this.arenaBounds.getLeft() == null || this.arenaBounds.getRight() == null) {
      return Pair.of(null, null);
    }

    return Pair.of(this.arenaBounds.getLeft().clone(), this.arenaBounds.getRight().clone());
  }

  public Map<Entity, ShopType> getShops() {
    return new HashMap<>(this.shops);
  }

  public void addShop(final Entity shopEntity, final ShopType shopType) {
    this.shops.put(shopEntity, shopType);
  }

  public ShopType removeShop(final Entity shopEntity) {
    return this.shops.remove(shopEntity);
  }

  public Set<ItemGenerator> getGenerators() {
    return new HashSet<>(this.generators);
  }

  public void addGenerator(final ItemGenerator generator) {
    this.generators.add(generator);
  }

  public void removeGenerator(final ItemGenerator generator) {
    this.generators.remove(generator);
  }

  public Set<Block> getPlacedBlocks() {
    return new HashSet<>(this.placedBlocks);
  }

  public void addPlacedBlock(final Block block) {
    this.placedBlocks.add(block);
  }

  public void removePlacedBlock(final Block block) {
    this.placedBlocks.remove(block);
  }

  public void clearPlacedBlocks() {
    for (final Block block : this.placedBlocks) {
      block.setType(Material.AIR);
    }

    this.placedBlocks.clear();
  }

  public Map<User, Turret> getTurrets() {
    return new HashMap<>(this.turrets);
  }

  public void addTurret(final User user, final Turret turret) {
    this.turrets.put(user, turret);
  }

  public void removeTurret(final User user) {
    this.turrets.remove(user);
  }

  public void clearTurrets() {
    for (final Turret turret : this.turrets.values()) {
      turret.stop();
    }

    this.turrets.clear();
  }

  public void removeTurret(final Turret turret) {
    User user = null;
    for (final Entry<User, Turret> entry : this.turrets.entrySet()) {
      if (entry.getValue().equals(turret)) {
        user = entry.getKey();
        break;
      }
    }

    this.turrets.remove(user);
  }

  public Map<Entity, User> getArenaEntities() {
    return new HashMap<>(this.arenaEntities);
  }

  public void addArenaEntity(final Entity entity, final User user) {
    this.arenaEntities.put(entity, user);
  }

  public void removeArenaEntity(final Entity entity) {
    this.arenaEntities.remove(entity);
  }

  public void clearArenaEntities() {
    this.arenaEntities.keySet().forEach(Entity::remove);
    this.arenaEntities.clear();
  }

  public User getEntityUser(final Entity entity) {
    return this.arenaEntities.get(entity);
  }

  public int getUserEntityNumber(final User user, final EntityType entityType) {
    int number = 0;
    for (final Entry<Entity, User> entry : this.arenaEntities.entrySet()) {
      if (entry.getKey().getType() == entityType && entry.getValue().equals(user)) {
        number++;
      }
    }

    return number;
  }

  public Map<Entity, Turret> getTurretProjectiles() {
    return new HashMap<>(this.turretProjectiles);
  }

  public void addTurretProjectile(final Entity entity, final Turret turret) {
    this.turretProjectiles.put(entity, turret);
  }

  public void removeTurretProjectile(final Entity entity) {
    this.turretProjectiles.remove(entity);
  }

  public void clearTurretProjectiles() {
    this.turretProjectiles.keySet().forEach(Entity::remove);
    this.turretProjectiles.clear();
  }

  public Turret getProjectileTurret(final Entity entity) {
    return this.turretProjectiles.get(entity);
  }

  public boolean isInRegion(final Location location) {
    final int minX = Math.min(this.arenaBounds.getLeft().getBlockX(), this.arenaBounds.getRight().getBlockX());
    final int maxX = Math.max(this.arenaBounds.getLeft().getBlockX(), this.arenaBounds.getRight().getBlockX());

    if (location.getBlockX() < minX || location.getBlockX() > maxX) {
      return false;
    }

    final int minY = Math.min(this.arenaBounds.getLeft().getBlockY(), this.arenaBounds.getRight().getBlockY());
    final int maxY = Math.max(this.arenaBounds.getLeft().getBlockY(), this.arenaBounds.getRight().getBlockY());

    if (location.getBlockY() < minY || location.getBlockY() > maxY) {
      return false;
    }

    final int minZ = Math.min(this.arenaBounds.getLeft().getBlockZ(), this.arenaBounds.getRight().getBlockZ());
    final int maxZ = Math.max(this.arenaBounds.getLeft().getBlockZ(), this.arenaBounds.getRight().getBlockZ());

    return location.getBlockZ() >= minZ && location.getBlockZ() <= maxZ;
  }

}
