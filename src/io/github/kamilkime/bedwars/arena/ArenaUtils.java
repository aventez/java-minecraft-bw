package io.github.kamilkime.bedwars.arena;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Colorable;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import pl.plajerlair.commonsbox.minecraft.misc.MiscUtils;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.XpConstants;
import io.github.kamilkime.bedwars.constant.arena.GamePhase;
import io.github.kamilkime.bedwars.constant.arena.ShopType;
import io.github.kamilkime.bedwars.constant.team.TeamColor;
import io.github.kamilkime.bedwars.constant.user.ToolTier;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.scoreboard.ScoreboardUtils;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.TeamUtils;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public final class ArenaUtils {

  private ArenaUtils() {
  }

  public static void resetArena(final Arena arena, final boolean startAfter) {
    if (!arena.isEnabled()) {
      return;
    }

    arena.stopArena();

    // KICK ALL ARENA PLAYERS
    for (final User toKick : arena.getArenaPlayers()) {
      kickPlayer(toKick, false, false);
    }

    // RESET DATA FOR EACH TEAM
    for (final Team team : arena.getTeams().values()) {
      final Pair<Location, Location> bed = team.getTeamBed();
      final Block leftBlock = bed.getLeft().getBlock();
      final Block rightBlock = bed.getRight().getBlock();

      // REPLACE THE BED
      leftBlock.setType(Material.BED_BLOCK);
      leftBlock.setData(team.getTeamBedData());

      rightBlock.setType(Material.BED_BLOCK);
      rightBlock.setData((byte) (team.getTeamBedData() + 8));

      // TRY TO COLOR THE BED
      try {
        final BlockState leftState = leftBlock.getState();
        ((Colorable) leftState).setColor(team.getTeamColor().getDyeColor());
        leftState.update(true);

        final BlockState rightState = rightBlock.getState();
        ((Colorable) rightState).setColor(team.getTeamColor().getDyeColor());
        rightState.update(true);
      } catch (Exception ignored) {
      }

      team.setBedStatus(true);
      team.getTeamGenerator().reset();

      // RESET THE TEAM CHEST
      ((Chest) team.getTeamChest().getBlock().getState()).getInventory().clear();
    }

    // RESET GENERATORS
    final ArenaRegion arenaRegion = arena.getArenaRegion();
    for (final ItemGenerator generator : arenaRegion.getGenerators()) {
      generator.reset();
    }

    // CLEAR PLACED BLOCKS, TURRETS, ITS PROJECTILES AND ALL ENTITIES SPAWNED BY PLAYERS
    arenaRegion.clearPlacedBlocks();
    arenaRegion.clearTurrets();
    arenaRegion.clearArenaEntities();
    arenaRegion.clearTurretProjectiles();

    final Pair<Location, Location> arenaBounds = arenaRegion.getArenaBounds();
    final Location arenaCenter = arenaRegion.getArenaCenter();

    arenaCenter.setY(arenaBounds.getLeft().getY());
    final double distance = arenaCenter.distance(arenaBounds.getLeft());

    arenaCenter.setY(127.5D);

    // CLEAR ALL OTHER ENTITIES, E.G. ITEMS
    for (final Entity entity : arenaCenter.getWorld().getNearbyEntities(arenaCenter, distance, 128, distance)) {
      if (entity instanceof Villager || entity instanceof Player) {
        continue;
      }

      if (!entity.getLocation().getChunk().isLoaded()) {
        entity.getLocation().getChunk().load(true);
      }

      entity.remove();
    }

    if (startAfter) {
      arena.setGamePhase(GamePhase.EMPTY);
      arena.startArena();
    }
  }

  public static void startArena(final Arena arena) {
	  
    for (final Team team : arena.getTeams().values()) {
      for (final User member : team.getAliveTeamMemebers()) {
        final Player player = member.getPlayer();

        player.setHealth(20.0D);
        player.setFoodLevel(20);

        player.teleport(team.getTeamSpawn());

        for (final Entry<Integer, ItemStack> entry : BedWarsConstants.START_ITEMS.get(team.getTeamColor()).entrySet()) {
          player.getInventory().setItem(entry.getKey(), entry.getValue());
        }

        member.setPickaxeTier(ToolTier.NONE);
        member.setAxeTier(ToolTier.NONE);

        ScoreboardUtils.startScoreboard(member, team, arena);
      }

      team.clearUpgrades();
      team.getTeamGenerator().startGeneration();
    }

    for (final ItemGenerator generator : arena.getArenaRegion().getGenerators()) {
      generator.startGeneration();
    }
  }

  public static void startDeathmatch(final Arena arena) {
    for (final Team team : arena.getTeams().values()) {
      for (final User member : team.getAliveTeamMemebers()) {
        member.getPlayer().teleport(team.getDeathmatchSpawn());
      }
    }

    new DeathmatchTask(arena).runTaskTimer(BedWars.getPlugin(), 0L, 10L);
  }

  public static boolean tryFinish(final Arena arena) {
    if (!arena.canFinish()) {
      return false;
    }

    arena.removeUpgrades();
    arena.stopArena();

    Team winningTeam = null;
    for (final Team team : arena.getTeams().values()) {
      if (team.getAliveTeamMemebers().size() != 0) {
        winningTeam = team;
        break;
      }
    }

    arena.broadcast(Replacer.of(Messages.ARENA_WINNING_TEAM).with("{TEAM}", winningTeam.getTeamColor().getTeamName()).toString());
    
    for (final User member : winningTeam.getTeamMembers().keySet()) {
      member.addXP(XpConstants.XP_FOR_ARENA_WIN);
      member.addMoney(BedWarsConstants.MONEY_FOR_ARENA_WIN);
    }

    arena.setGamePhase(GamePhase.FINISH);

    final Set<User> winners = winningTeam.getAliveTeamMemebers();
    new BukkitRunnable() {
      int tries = 0;

      @Override
      public void run() {
        if (tries == 5 || arena.getGamePhase() != GamePhase.FINISH) {
          this.cancel();
          return;
        }
        for (User user : winners) {
          MiscUtils.spawnRandomFirework(user.getPlayer().getLocation());
        }
        tries++;
      }
    }.runTaskTimer(BedWars.getPlugin(), 20, 20);

    arena.startArena();

    return true;
  }

  public static void kickPlayer(final User toKick, boolean tryEliminate, boolean tryFinish) {
    final Arena arena = toKick.getCurrentArena();
    final Player player = toKick.getPlayer();

    ScoreboardUtils.stopScoreboard(toKick);

    final Team team = arena.getUserTeam(toKick);
    team.removeTeamMember(toKick);

    for (final PotionEffect effect : player.getActivePotionEffects()) {
      player.removePotionEffect(effect.getType());
    }

    player.setGameMode(GameMode.SURVIVAL);
    player.getInventory().clear();
    player.getEnderChest().clear();

    player.setHealth(20.0D);
    player.setFoodLevel(20);

    arena.broadcast(Replacer.of(Messages.ARENA_LEAVE).with("{PLAYER}", player.getName()).toString());
    player.teleport(BedWars.getMainSpawn());

    toKick.setCurrentArena(null);

    // SHOW ALL PLAYERS THAT ARE NOT IN ANY ARENA
    for (final Player onlinePlayer : Bukkit.getOnlinePlayers()) {
      if (onlinePlayer.equals(player)) {
        continue;
      }

      final User onlineUser = UserData.getUser(onlinePlayer.getUniqueId());
      if (onlineUser.isInGame()) {
        if (!onlinePlayer.hasPermission("bedwars.hideplayer.bypass")) {
          onlinePlayer.hidePlayer(player);
        }

        if (!player.hasPermission("bedwars.hideplayer.bypass")) {
          player.hidePlayer(onlinePlayer);
        }

        onlinePlayer.getScoreboard().getTeams().forEach(sbTeam -> sbTeam.removeEntry(player.getName()));
      } else {
        onlinePlayer.showPlayer(player);
        player.showPlayer(onlinePlayer);
      }
    }

    if (tryEliminate) {
      TeamUtils.tryEliminateTeam(arena, team);
    }

    if (tryFinish) {
      tryFinish(arena);
    }
  }

  public static Entity spawnShop(final Location location, final ShopType shopType) {
    if (!location.getChunk().isLoaded()) {
      location.getChunk().load(true);
    }

    // REMOVE ALL ENTITIES THAT STAND IN CLOSE PROXIMITY TO THE NEWLY SPAWNED ONE
    for (final Entity entity : location.getWorld().getNearbyEntities(location, 0.1D, 0.1D, 0.1D)) {
      entity.remove();
    }

    final Villager villager = location.getWorld().spawn(location, Villager.class);

    villager.setAI(false);
    villager.setInvulnerable(true);
    villager.setCustomName(shopType.getShopName());
    villager.setCustomNameVisible(true);
    villager.setRemoveWhenFarAway(false);
    villager.setRecipes(new ArrayList<>());
    villager.setProfession(shopType.getVillagerProfession());

    return villager;
  }

}
