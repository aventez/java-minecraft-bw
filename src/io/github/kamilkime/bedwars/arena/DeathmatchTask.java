package io.github.kamilkime.bedwars.arena;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;

public class DeathmatchTask extends BukkitRunnable {

  private final Arena arena;
  private final Location baseLocation;

  double borderRadius = BedWarsConstants.DEATHMATCH_BORDER_RADIUS;

  public DeathmatchTask(final Arena arena) {
    this.arena = arena;

    final ArenaRegion region = arena.getArenaRegion();
    final Location center = region.getArenaCenter();

    center.setY(center.getY() - ((BedWarsConstants.DEATHMATCH_BORDER_HEIGHT - 1.0D) / 2.0D));
    this.baseLocation = center;
  }

  @Override
  public void run() {
    // SPAWN BORDER PARTICLES
    if (this.borderRadius >= 0.0D) {
      final Set<Location> circlePoints = new HashSet<>();
      for (double angle = 0.0D; angle < Math.PI * 2.0D; angle += BedWarsConstants.deathmatchBorderAngle(this.borderRadius)) {
        circlePoints.add(this.baseLocation.clone().add(this.borderRadius * Math.cos(angle), 0.0D, this.borderRadius * Math.sin(angle)));
      }

      for (final Location circlePoint : circlePoints) {
        for (double addedY = 0.0D; addedY <= BedWarsConstants.DEATHMATCH_BORDER_HEIGHT; addedY += 1.0D) {
          BuildMode.spawnRedstone(circlePoint.add(0.0D, 1.0D, 0.0D), Color.RED);
        }
      }
    }

    // DAMAGE PLAYERS STANDING OUTSIDE THE BORDER
    for (final Team team : this.arena.getTeams().values()) {
      for (final User user : team.getAliveTeamMemebers()) {
        final Player player = user.getPlayer();
        final Location playerLocation = player.getLocation();
        final Location center = this.baseLocation.clone();

        center.setY(playerLocation.getY());
        if (playerLocation.distance(center) > this.borderRadius) {
          player.damage(BedWarsConstants.DEATHMATCH_BORDER_DAMAGE);
        }
      }
    }

    if (ArenaUtils.tryFinish(this.arena)) {
      this.cancel();
      return;
    }

    this.borderRadius -= BedWarsConstants.DEATHMATCH_BORDER_DECREASE;
  }

}
