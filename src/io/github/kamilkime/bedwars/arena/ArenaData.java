package io.github.kamilkime.bedwars.arena;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import io.github.kamilkime.bedwars.data.DataSort;

public class ArenaData {

  private static final Map<String, Arena> GAME_ARENAS = new HashMap<>();

  public static Arena getArena(final String name, final boolean ignoreCase) {
    if (!ignoreCase) {
      return GAME_ARENAS.get(name);
    }

    for (final Entry<String, Arena> entry : GAME_ARENAS.entrySet()) {
      if (entry.getKey().equalsIgnoreCase(name)) {
        return entry.getValue();
      }
    }

    return null;
  }

  public static List<Arena> getSortedArenas() {
    final List<Arena> arenas = new ArrayList<>(GAME_ARENAS.values());
    arenas.sort((Comparator<Arena>) DataSort.ARENA.getComparator());
    return arenas;
  }

  public static Collection<Arena> getArenas() {
    return GAME_ARENAS.values();
  }

  public static void addArena(final String name, final Arena arena) {
    GAME_ARENAS.put(name, arena);
  }

  public static void removeArena(final String name) {
    GAME_ARENAS.remove(name);
  }

}
