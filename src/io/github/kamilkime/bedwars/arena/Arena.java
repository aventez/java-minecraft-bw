package io.github.kamilkime.bedwars.arena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.constant.BedWarsConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.XpConstants;
import io.github.kamilkime.bedwars.constant.arena.ArenaMode;
import io.github.kamilkime.bedwars.constant.arena.GamePhase;
import io.github.kamilkime.bedwars.constant.arena.JoinDenyReason;
import io.github.kamilkime.bedwars.constant.generator.GeneratorType;
import io.github.kamilkime.bedwars.constant.gui.PersonalLobbyUpgrade;
import io.github.kamilkime.bedwars.constant.gui.TeamLobbyUpgrade;
import io.github.kamilkime.bedwars.constant.team.TeamColor;
import io.github.kamilkime.bedwars.constant.team.upgrade.TeamUpgrade;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.scoreboard.ScoreboardUtils;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.TeamUtils;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;
import net.minecraft.server.v1_12_R1.EnumParticle;

public final class Arena {

  private final String arenaName;

  private final ArenaMode arenaMode;
  private final ArenaRegion arenaRegion;
  private final Map<TeamColor, Team> teams = new HashMap<>();
  private GamePhase gamePhase;
  private boolean enabled;
  private long phaseDuration;
  private BukkitTask arenaTask;

  public Arena(final String arenaName, final ArenaMode arenaMode, final Location minBound, final Location maxBound) {
    this.arenaName = arenaName;
    this.arenaMode = arenaMode;
    this.arenaRegion = new ArenaRegion(minBound, maxBound);

    for (final TeamColor teamColor : this.arenaMode.getTeamColors()) {
      this.teams.put(teamColor, new Team(teamColor));
    }

    ArenaData.addArena(arenaName, this);
  }

  public boolean startArena() {
    if (!this.enabled) {
      return false;
    }
    
    Bukkit.broadcastMessage("arena wystartowala");
    
    this.arenaTask = Bukkit.getScheduler().runTaskTimer(BedWars.getPlugin(), () -> {

      // UPDATE SCOREBOARDS
      ScoreboardUtils.updateScoreboard(this);

      // CHECK FOR TEAM TRAP ACTIVATION
      for (final Team team : this.teams.values()) {
        for (final Team targetTeam : this.teams.values()) {
          if (targetTeam.equals(team)) {
            if (!team.hasTeamUpgrade(TeamUpgrade.HEAL_POOL)) {
              continue;
            }
            for (final User targetUser : targetTeam.getAliveTeamMemebers()) {
              final Player targetPlayer = targetUser.getPlayer();
              if (targetPlayer.getLocation().distanceSquared(team.getTeamBed().getKey()) > BedWarsConstants.TEAM_TRAP_RADIUS_SQ) {
                continue;
              }
              targetPlayer.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 50, 0), true);
            }
            continue;
          }

          for (final User targetUser : targetTeam.getAliveTeamMemebers()) {
            final Player targetPlayer = targetUser.getPlayer();
            if (targetPlayer.getLocation().distanceSquared(team.getTeamBed().getKey()) > BedWarsConstants.TEAM_TRAP_RADIUS_SQ) {
              continue;
            }
            if (targetPlayer.hasMetadata("BedWarsTrapImmune")) {
              continue;
            }

            for(TeamLobbyUpgrade upgrade : team.getTeamLobbyUpgrades()) {
            	switch(upgrade) {
            		case ANOTHER_VILLAGER:
            			//wip
            			break;
            	}
            }
            
            for (TeamUpgrade upgrade : team.getTeamUpgrades()) {
              if (!upgrade.isTrap()) {
                continue;
              }

              //should we iterate for next upgrade? reiterating only when invisible trap is active and user is not invisible
              boolean reiterate = false;

              switch (upgrade) {
                case TRAP_BLINDNESS:
                  targetPlayer.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10 * 20, 0), true);
                  targetPlayer.playSound(targetPlayer.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 0.25F, 1.0F);
                  for (final User trapOwner : team.getAliveTeamMemebers()) {
                    trapOwner.getPlayer().sendTitle(Messages.ARENA_BLINDNESS_TRAP_ACTIVATED_TITLE, Messages.ARENA_BLINDNESS_TRAP_ACTIVATED_SUBTITLE, 5, 30, 5);
                  }
                  break;
                case TRAP_SLOW:
                  targetPlayer.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 10 * 20, 0), true);
                  targetPlayer.playSound(targetPlayer.getLocation(), Sound.ENTITY_SPLASH_POTION_THROW, 0.25F, 1.0F);
                  for (final User trapOwner : team.getAliveTeamMemebers()) {
                    trapOwner.getPlayer().sendTitle(Messages.ARENA_SLOW_TRAP_ACTIVATED_TITLE, Messages.ARENA_SLOW_TRAP_ACTIVATED_SUBTITLE, 5, 30, 5);
                  }
                  break;
                case TRAP_QUICK_RETURN:
                  targetPlayer.playSound(targetPlayer.getLocation(), Sound.BLOCK_ANVIL_DESTROY, 0.25F, 1.0F);
                  for (final User trapOwner : team.getAliveTeamMemebers()) {
                    trapOwner.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 15 * 20, 0), true);
                    trapOwner.getPlayer().sendTitle(Messages.ARENA_QUICK_RETURN_TRAP_ACTIVATED_TITLE, Messages.ARENA_QUICK_RETURN_TRAP_ACTIVATED_SUBTITLE, 5, 30, 5);
                  }
                  break;
                case TRAP_INVISIBLE_FINDER:
                  if (!targetPlayer.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
                    reiterate = true;
                    break;
                  }
                  for (User user : targetTeam.getAliveTeamMemebers()) {
                    user.getPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
                    user.getPlayer().sendTitle(Messages.ARENA_INVISIBLE_TRAP_REVEALED_TITLE, Messages.ARENA_INVISIBLE_TRAP_REVEALED_SUBTITLE, 5, 30, 5);
                  }

                  targetPlayer.playSound(targetPlayer.getLocation(), Sound.BLOCK_ANVIL_DESTROY, 0.25F, 1.0F);
                  for (final User trapOwner : team.getAliveTeamMemebers()) {
                    trapOwner.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 15 * 20, 0), true);
                    trapOwner.getPlayer().sendTitle(Messages.ARENA_INVISIBLE_TRAP_ACTIVATED_TITLE, Messages.ARENA_INVISIBLE_TRAP_ACTIVATED_SUBTITLE, 5, 30, 5);
                  }
                  break;
                default:
                  break;
              }
              if (!reiterate) {
                //apply only 1 trap per iteration
                team.removeTeamUpgrade(upgrade);
                break;
              }
            }
            break;
          }
        }
      }

      // CHECK FOR GAME START OR ITS CANCEL
      if (this.gamePhase == GamePhase.EMPTY) {
        if (this.getPlayerNumber() >= this.arenaMode.getMinPlayers()) {
          this.gamePhase = GamePhase.LOBBY;
          this.phaseDuration = 0L;

          ScoreboardUtils.updateScoreboard(this);
          return;
        }
      } else if (this.gamePhase == GamePhase.LOBBY) {
        if (this.getPlayerNumber() < this.arenaMode.getMinPlayers()) {
          this.gamePhase = GamePhase.EMPTY;
          this.phaseDuration = 0L;

          this.broadcast(Messages.ARENA_START_CANCEL);
          ScoreboardUtils.updateScoreboard(this);
          return;
        } else if (this.phaseDuration != GamePhase.LOBBY.getPhaseDuration()) {
          final long remaining = this.gamePhase.getPhaseDuration() - this.phaseDuration;
          if (this.phaseDuration % 10 == 0 || DataUtils.isInRange(remaining, 1L, 5L)) {
            this.broadcast(Replacer.of(Messages.ARENA_START_IN).with("{TIME}", remaining).toString());
            this.phaseDuration++;
            return;
          }
        }
      }

      this.phaseDuration++;

      // CHECK FOR PHASE CHANGE
      final long phaseDuration = this.gamePhase.getPhaseDuration();
      if (phaseDuration == -1 || phaseDuration > this.phaseDuration) {
        return;
      }

      this.gamePhase = GamePhase.next(this.gamePhase);
      this.phaseDuration = 0;

      if(this.getGamePhase() == gamePhase.START) {
	    for (final User user : this.getArenaPlayers()) {
	        if(user.hasPersonalLobbyUpgrade(PersonalLobbyUpgrade.EXTRA_ARROWS)) {
	        	EnumParticle e = EnumParticle.DRIP_LAVA;
	        	BedWars.getArrowTrails().addTrail(user.getPlayer(), e);
	        }
	      }
      }
      
      // MESSAGE OF A NEW PHASE
      if (!this.gamePhase.getPhaseMessage().isEmpty()) {
        this.broadcast(this.gamePhase.getPhaseMessage());
      }

      // ADD XP FOR A NEW PHASE
      for (final Team team : this.getTeams().values()) {
        for (final User alive : team.getAliveTeamMemebers()) {
          alive.addXP(XpConstants.getXpForGamePhase(this.gamePhase));
        }
      }

      // CHECK FOR SPECIFIC PHASES OR UPDATE GENERATORS
      if (this.gamePhase == GamePhase.RESET) {
        ArenaUtils.resetArena(this, true);
      } else if (this.gamePhase == GamePhase.START) {
        ArenaUtils.startArena(this);
      } else if (this.gamePhase == GamePhase.BEDS_DESTROYED) {
        this.teams.values().forEach(team -> TeamUtils.destroyBed(this, team, null));
      } else if (this.gamePhase == GamePhase.DEATHMATCH) {
        ArenaUtils.startDeathmatch(this);
      } else {
        final GeneratorType toBoost = this.gamePhase.toString().contains("DIAMOND") ? GeneratorType.DIAMOND : GeneratorType.EMERALD;
        this.getArenaRegion().getGenerators().stream().filter(g -> g.getType() == toBoost).forEach(ItemGenerator::addLevel);
      }
    }, 0L, 20L);

    return true;
  }

  public void stopArena() {
    if (this.arenaTask == null) {
      return;
    }

    this.arenaTask.cancel();
    this.phaseDuration = 0L;
  }

  public JoinDenyReason join(final User user, final Team team) {
    if (!this.enabled) {
      return JoinDenyReason.ARENA_DISABLED;
    }

    if (this.gamePhase != GamePhase.EMPTY && this.gamePhase != GamePhase.LOBBY) {
      return JoinDenyReason.GAME_RUNNING;
    }

    if (this.getPlayerNumber() == this.arenaMode.getMaxPlayers()) {
      return JoinDenyReason.ARENA_FULL;
    }

    if (team.getAliveTeamMemebers().size() == this.arenaMode.getTeamPlayers()) {
      return JoinDenyReason.TEAM_FULL;
    }

    team.addTeamMember(user);
    user.setCurrentArena(this);
    
    user.personalUpgrades.clear();
    
    final Player player = user.getPlayer();

    player.setGameMode(GameMode.SURVIVAL);
    player.getInventory().clear();
    player.getEnderChest().clear();

    player.setHealth(20.0D);
    player.setFoodLevel(20);

    player.teleport(this.arenaRegion.getMainLobbyLocation());

    this.broadcast(Replacer.of(Messages.ARENA_JOIN).with("{PLAYER}", user.getPlayer().getName()).with("{NUM}", this.getPlayerNumber())
        .with("{MAX}", this.arenaMode.getMaxPlayers()).toString());

    // HIDE ALL PLAYERS OTHER THAN THE SAME ARENA MEMBERS
    for (final Player onlinePlayer : Bukkit.getOnlinePlayers()) {
      if (onlinePlayer.equals(player)) {
        continue;
      }

      final User onlineUser = UserData.getUser(onlinePlayer.getUniqueId());
      if (this.equals(onlineUser.getCurrentArena())) {
        onlinePlayer.showPlayer(player);
        player.showPlayer(onlinePlayer);
      } else {
        if (!onlinePlayer.hasPermission("bedwars.hideplayer.bypass")) {
          onlinePlayer.hidePlayer(player);
        }

        if (!player.hasPermission("bedwars.hideplayer.bypass")) {
          player.hidePlayer(onlinePlayer);
        }
      }
    }

    return null;
  }

  public JoinDenyReason join(final User user) {
    Team teamToJoin = null;
    int numPlayers = Integer.MAX_VALUE;

    // CHOOSE A TEAM WITH LEAST PLAYERS
    for (final Team team : this.teams.values()) {
      if (team.getAliveTeamMemebers().size() < numPlayers) {
        teamToJoin = team;
        numPlayers = team.getAliveTeamMemebers().size();
      }
    }

    return join(user, teamToJoin);
  }

  public boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(final boolean enabled) {
    if (enabled && this.canBeEnabled() != null) {
      return;
    }

    this.enabled = enabled;
  }

  public String canBeEnabled() {
    if (this.arenaRegion.getMainLobbyLocation() == null) {
      return "arena main lobby missing";
    }

    if(this.arenaRegion.getShopLobbyLocation() == null) {
    	return "arena shop lobby missing";
    }
    
    if (this.arenaRegion.getSpectatorLobbyLocation() == null) {
      return "arena spectator lobby missing";
    }

    if (this.arenaRegion.getArenaCenter() == null) {
      return "arena center missing";
    }

    for (final Team team : this.teams.values()) {
      final String teamName = team.getTeamColor().getTeamName();

      if (team.getTeamSpawn() == null) {
        return teamName + " &cspawn missing";
      }

      if (team.getTeamChest() == null) {
        return teamName + " &cteam chest missing";
      }

      if (team.getDeathmatchSpawn() == null) {
        return teamName + " &cdeathmatch spawn missing";
      }

      if (team.getTeamGenerator() == null) {
        return teamName + " &cteam generator missing";
      }

      if (team.getTeamBed() == null) {
        return teamName + " &cbed missing";
      }
    }

    return null;
  }

  public String getArenaName() {
    return this.arenaName;
  }

  public ArenaMode getArenaMode() {
    return this.arenaMode;
  }

  public ArenaRegion getArenaRegion() {
    return this.arenaRegion;
  }

  public GamePhase getGamePhase() {
    return this.gamePhase;
  }

  public void setGamePhase(final GamePhase gamePhase) {
    this.gamePhase = gamePhase;
  }

  public long getPhaseDuration() {
    return this.phaseDuration;
  }

  public Map<TeamColor, Team> getTeams() {
    return new HashMap<>(this.teams);
  }

  public Set<User> getArenaPlayers() {
    final Set<User> players = new HashSet<>();
    for (final Team team : this.teams.values()) {
      players.addAll(team.getTeamMembers().keySet());
    }

    return players;
  }

  public int getPlayerNumber() {
    int number = 0;
    for (final Team team : this.teams.values()) {
      number += team.getTeamMembers().size();
    }

    return number;
  }

  public Team getUserTeam(final User user) {
    for (final Team team : this.teams.values()) {
      if (team.getTeamMembers().containsKey(user)) {
        return team;
      }
    }

    return null;
  }

  public boolean canFinish() {
    if (!this.gamePhase.isGame()) {
      return false;
    }

    int aliveTeams = 0;
    for (final Team team : this.teams.values()) {
      if (team.getAliveTeamMemebers().size() != 0) {
        aliveTeams++;
      }
    }

    return aliveTeams == 1;
  }

  public void removeUpgrades() {
    for (final User user : this.getArenaPlayers()) {
        if(user.hasPersonalLobbyUpgrade(PersonalLobbyUpgrade.EXTRA_ARROWS)) {
        	BedWars.getArrowTrails().removeTrail(user.getPlayer());
        }
    	user.personalUpgrades.clear();
      }
  }
  
  public void broadcast(final String message) {
    for (final User user : this.getArenaPlayers()) {
      user.getPlayer().sendMessage(message);
    }
  }

}
