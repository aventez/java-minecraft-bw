package io.github.kamilkime.bedwars;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import pl.plajerlair.commonsbox.minecraft.serialization.LocationSerializer;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.arena.ArenaUtils;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.command.ArenaCommand;
import io.github.kamilkime.bedwars.command.ShoutCommand;
import io.github.kamilkime.bedwars.command.UserCommand;
import io.github.kamilkime.bedwars.data.BedWarsData;
import io.github.kamilkime.bedwars.hook.PlaceholderAPIHook;
import io.github.kamilkime.bedwars.listener.block.BlockBreakListener;
import io.github.kamilkime.bedwars.listener.block.BlockIgniteListener;
import io.github.kamilkime.bedwars.listener.block.BlockPlaceListener;
import io.github.kamilkime.bedwars.listener.entity.CreatureSpawnListener;
import io.github.kamilkime.bedwars.listener.entity.EntityChangeBlockListener;
import io.github.kamilkime.bedwars.listener.entity.EntityDamageByEntityListener;
import io.github.kamilkime.bedwars.listener.entity.EntityDamageListener;
import io.github.kamilkime.bedwars.listener.entity.EntityExplodeListener;
import io.github.kamilkime.bedwars.listener.entity.EntityTargetLivingEntityListener;
import io.github.kamilkime.bedwars.listener.entity.FoodLevelChangeListener;
import io.github.kamilkime.bedwars.listener.entity.ProjectileHitListener;
import io.github.kamilkime.bedwars.listener.entity.ProjectileLaunchListener;
import io.github.kamilkime.bedwars.listener.inventory.GuiClickListener;
import io.github.kamilkime.bedwars.listener.inventory.ItemCraftListener;
import io.github.kamilkime.bedwars.listener.player.AsyncPlayerChatListener;
import io.github.kamilkime.bedwars.listener.player.ItemConsumeListener;
import io.github.kamilkime.bedwars.listener.player.PlayerCommandPreprocessListener;
import io.github.kamilkime.bedwars.listener.player.PlayerDeathListener;
import io.github.kamilkime.bedwars.listener.player.PlayerInteractListener;
import io.github.kamilkime.bedwars.listener.player.PlayerItemDamageListener;
import io.github.kamilkime.bedwars.listener.player.PlayerJoinListener;
import io.github.kamilkime.bedwars.listener.player.PlayerQuitListener;
import io.github.kamilkime.bedwars.listener.player.PlayerRespawnListener;
import io.github.kamilkime.bedwars.listener.player.Trail;
import net.minecraft.server.v1_12_R1.EnumParticle;

public final class BedWars extends JavaPlugin {

  private static BedWars plugin;
  private static WorldEditPlugin we;

  private Map<UUID, Trail> players = new HashMap<>();
  private static BedWars arrowTrails;
  
  public BedWars() {
    plugin = this;
  }

  public static BedWars getPlugin() {
    return plugin;
  }

  public static WorldEditPlugin getWE() {
    return we;
  }

  public static Location getMainSpawn() {
    return LocationSerializer.getLocation(plugin.getConfig().getString("Main-Spawn-Location"));
  }

  @Override
  public void onEnable() {
	  
	arrowTrails = this;
	  
    saveDefaultConfig();
    final PluginManager pluginManager = this.getServer().getPluginManager();

    if (pluginManager.getPlugin("HolographicDisplays") == null) {
      Bukkit.getLogger().warning("[BedWars] Missing dependency: HolographicDisplays");
      pluginManager.disablePlugin(this);
      return;
    }

    if (pluginManager.getPlugin("WorldEdit") == null) {
      Bukkit.getLogger().warning("[BedWars] Missing dependency: WorldEdit");
      pluginManager.disablePlugin(this);
      return;
    }

    we = (WorldEditPlugin) pluginManager.getPlugin("WorldEdit");

    BedWarsData.loadAll();

    if (pluginManager.getPlugin("PlaceholderAPI") != null) {
      PlaceholderAPIHook.initPlaceholderHook();
    }

    pluginManager.registerEvents(new BlockBreakListener(), this);
    pluginManager.registerEvents(new BlockIgniteListener(), this);
    pluginManager.registerEvents(new BlockPlaceListener(), this);

    pluginManager.registerEvents(new CreatureSpawnListener(), this);
    pluginManager.registerEvents(new EntityChangeBlockListener(), this);
    pluginManager.registerEvents(new EntityDamageByEntityListener(), this);
    pluginManager.registerEvents(new EntityDamageListener(), this);
    // pluginManager.registerEvents(new EntityDeathListener(), this);
    pluginManager.registerEvents(new EntityExplodeListener(), this);
    pluginManager.registerEvents(new EntityTargetLivingEntityListener(), this);
    pluginManager.registerEvents(new FoodLevelChangeListener(), this);
    pluginManager.registerEvents(new ProjectileHitListener(), this);
    pluginManager.registerEvents(new ProjectileLaunchListener(), this);

    pluginManager.registerEvents(new GuiClickListener(), this);
    pluginManager.registerEvents(new ItemCraftListener(), this);

    pluginManager.registerEvents(new AsyncPlayerChatListener(), this);
    pluginManager.registerEvents(new ItemConsumeListener(), this);
    pluginManager.registerEvents(new PlayerCommandPreprocessListener(), this);
    pluginManager.registerEvents(new PlayerDeathListener(), this);
    pluginManager.registerEvents(new PlayerInteractListener(), this);
    pluginManager.registerEvents(new PlayerItemDamageListener(), this);
    pluginManager.registerEvents(new PlayerJoinListener(), this);
    pluginManager.registerEvents(new PlayerQuitListener(), this);
    pluginManager.registerEvents(new PlayerRespawnListener(), this);
    
    pluginManager.registerEvents(new ProjectileLaunchListener(), this);
    
	new BukkitRunnable() {
		@Override
		public void run() {
			for (Trail t : players.values()) {
				t.tick();
			}
		}
	}.runTaskTimerAsynchronously(this, 0, 1);
    
    getCommand("arena").setExecutor(new ArenaCommand());
    getCommand("user").setExecutor(new UserCommand());
    getCommand("shout").setExecutor(new ShoutCommand());

    Bukkit.getScheduler().runTaskTimer(this, BedWarsData::saveUserData, 5L * 60L * 20L, 5L * 60L * 20L);
  }

  @Override
  public void onDisable() {
    BuildMode.clear();

    for (final Arena arena : ArenaData.getArenas()) {
      ArenaUtils.resetArena(arena, false);
    }

    BedWarsData.saveAll();
  }

	public void addTrail(Player p, EnumParticle e) {
		players.put(p.getUniqueId(), new Trail(e));
	}
	
	public Trail getTrail(Player p) {
		return players.get(p.getUniqueId());
	}
	
	public boolean hasTrail(Player p) {
		return players.containsKey(p.getUniqueId());
	}
	
	public static BedWars getArrowTrails() {
		return arrowTrails;
	}
	
	public void removeTrail(Player p) {
		players.remove(p.getUniqueId());
	}
  
}
