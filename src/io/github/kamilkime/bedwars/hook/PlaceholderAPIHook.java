package io.github.kamilkime.bedwars.hook;

import java.util.List;
import java.util.regex.Pattern;

import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.data.DataSort;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public final class PlaceholderAPIHook {

  private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("[%]([^%]+)[%]");

  private PlaceholderAPIHook() {
  }

  public static void initPlaceholderHook() {
    new BedWarsPlaceholder().register();
    Bukkit.getLogger().info("[BedWars] PlaceholderAPI hook has been enabled!");
  }

  public static String replacePlaceholders(final Player user, final String base) {
    return PlaceholderAPI.setPlaceholders(user, base, PLACEHOLDER_PATTERN);
  }

  private static class BedWarsPlaceholder extends PlaceholderExpansion {

    // bw_beds-1
    // bw_deaths-1
    // bw_finalkills-1
    // bw_kills-1
    // bw_level-1
    // bw_money-1
    // bw_xp-1

    @Override
    public String onPlaceholderRequest(final Player player, final String identifier) {
      final String[] split = identifier.split("-");
      if (split.length < 2) {
        return "";
      }

      List<User> sortedUsers;
      switch (split[0].toLowerCase()) {
        case "bed":
        case "beds":
          sortedUsers = UserData.getSortedUsers(DataSort.USER_BED);
          break;
        case "death":
        case "deaths":
          sortedUsers = UserData.getSortedUsers(DataSort.USER_DEATHS);
          break;
        case "finalkill":
        case "finalkills":
          sortedUsers = UserData.getSortedUsers(DataSort.USER_FINALKILLS);
          break;
        case "kill":
        case "kills":
          sortedUsers = UserData.getSortedUsers(DataSort.USER_KILLS);
          break;
        case "level":
        case "lvl":
          sortedUsers = UserData.getSortedUsers(DataSort.USER_LEVEL);
          break;
        case "money":
          sortedUsers = UserData.getSortedUsers(DataSort.USER_MONEY);
          break;
        case "xp":
          sortedUsers = UserData.getSortedUsers(DataSort.USER_XP);
          break;
        default:
          return "";
      }

      int index;
      try {
        index = Integer.parseInt(split[1]) - 1;
      } catch (NumberFormatException exception) {
        return "";
      }

      if (index < 0 || index >= sortedUsers.size()) {
        return "";
      }

      final User user = sortedUsers.get(index);
      String returnedString = UserData.getName(user.getUUID()) + " - ";

      switch (split[0].toLowerCase()) {
        case "bed":
        case "beds":
          returnedString += user.getDestroyedBeds();
          break;
        case "death":
        case "deaths":
          returnedString += user.getDeaths();
          break;
        case "finalkill":
        case "finalkills":
          returnedString += user.getFinalKills();
          break;
        case "kill":
        case "kills":
          returnedString += user.getKills();
          break;
        case "level":
        case "lvl":
          returnedString += user.getLevel();
          break;
        case "money":
          returnedString += user.getMoney();
          break;
        case "xp":
          returnedString += user.getXP();
          break;
      }

      return returnedString;
    }

    @Override
    public String getAuthor() {
      return "Kamilkime";
    }

    @Override
    public String getIdentifier() {
      return "bw";
    }

    @Override
    public String getPlugin() {
      return "BedWars";
    }

    @Override
    public String getVersion() {
      return BedWars.getPlugin().getDescription().getVersion();
    }
  }

}
