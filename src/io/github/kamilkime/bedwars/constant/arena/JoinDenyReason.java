package io.github.kamilkime.bedwars.constant.arena;

public enum JoinDenyReason {

  GAME_RUNNING("game is already running"),
  ARENA_DISABLED("arena is disabled"),
  ARENA_FULL("the arena is full"),
  TEAM_FULL("the team you tried to join is full");

  private final String reasonMessage;

  JoinDenyReason(final String reasonMessage) {
    this.reasonMessage = reasonMessage;
  }

  public String getReasonMessage() {
    return this.reasonMessage;
  }

}
