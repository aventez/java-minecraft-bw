package io.github.kamilkime.bedwars.constant.arena;

import java.util.HashMap;
import java.util.Map;

import io.github.kamilkime.bedwars.data.DataUtils;

public enum GamePhase {

  RESET(-1L, "", "", false),
  EMPTY(-1L, "Waiting for players", "", false),
  LOBBY(20L, "Waiting for start", "", false),
  START(3 * 60L, "Warm up", "&7&l[BedWars] &aLet the battle begin...", true),
  DIAMOND_II(3 * 60L, "Diamond II", "&7&l[BedWars] &bDiamond generators have been upgraded to level II", true),
  EMERALD_II(3 * 60L, "Emerald II", "&7&l[BedWars] &aEmerald generators have been upgraded to level II", true),
  DIAMOND_III(3 * 60L, "Diamond III", "&7&l[BedWars] &bDiamond generators have been upgraded to level III", true),
  EMERALD_III(3 * 60L, "Emerald III", "&7&l[BedWars] &aEmerald generators have been upgraded to level III", true),
  DIAMOND_IV(3 * 60L, "Diamond IV", "&7&l[BedWars] &bDiamond generators have been upgraded to level IV", true),
  EMERALD_IV(3 * 60L, "Emerald IV", "&7&l[BedWars] &aEmerald generators have been upgraded to level IV", true),
  BEDS_DESTROYED(3 * 60L, "Beds destroyed", "&7&l[BedWars] &cAll beds have been destroyed!", true),
  DEATHMATCH(-1L, "Deathmatch", "&7&l[BedWars] &cDEATHMATCH - let the best warrior win!", true),
  FINISH(10L, "Game finished", "", false);

  private static final Map<GamePhase, GamePhase> NEXT = new HashMap<>();

  static {
    NEXT.put(GamePhase.RESET, GamePhase.EMPTY);
    NEXT.put(GamePhase.EMPTY, GamePhase.LOBBY);
    NEXT.put(GamePhase.LOBBY, GamePhase.START);
    NEXT.put(GamePhase.START, GamePhase.DIAMOND_II);
    NEXT.put(GamePhase.DIAMOND_II, GamePhase.EMERALD_II);
    NEXT.put(GamePhase.EMERALD_II, GamePhase.DIAMOND_III);
    NEXT.put(GamePhase.DIAMOND_III, GamePhase.EMERALD_III);
    NEXT.put(GamePhase.EMERALD_III, GamePhase.DIAMOND_IV);
    NEXT.put(GamePhase.DIAMOND_IV, GamePhase.EMERALD_IV);
    NEXT.put(GamePhase.EMERALD_IV, GamePhase.BEDS_DESTROYED);
    NEXT.put(GamePhase.BEDS_DESTROYED, GamePhase.DEATHMATCH);
    NEXT.put(GamePhase.DEATHMATCH, GamePhase.FINISH);
    NEXT.put(GamePhase.FINISH, GamePhase.RESET);
  }

  private final long phaseDuration;
  private final String phaseName;
  private final String phaseMessage;
  private final boolean isGame;

  GamePhase(final long phaseDuration, final String phaseName, final String phaseMessage, final boolean isGame) {
    this.phaseDuration = phaseDuration;
    this.phaseName = phaseName;
    this.phaseMessage = DataUtils.color(phaseMessage);
    this.isGame = isGame;
  }

  public static GamePhase next(GamePhase now) {
    return NEXT.get(now);
  }

  public long getPhaseDuration() {
    return this.phaseDuration;
  }

  public String getPhaseName() {
    return this.phaseName;
  }

  public String getPhaseMessage() {
    return this.phaseMessage;
  }

  public boolean isGame() {
    return this.isGame;
  }

}
