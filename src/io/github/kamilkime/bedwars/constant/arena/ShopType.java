package io.github.kamilkime.bedwars.constant.arena;

import org.bukkit.entity.Villager.Profession;

import io.github.kamilkime.bedwars.constant.gui.GuiType;
import io.github.kamilkime.bedwars.data.DataUtils;

public enum ShopType {

  PERSONAL("&5Personal shop", Profession.PRIEST, GuiType.PERSONAL_QUICK_SHOP),
  TEAM("&4Team shop", Profession.LIBRARIAN, GuiType.TEAM),
  TEAM_LOBBY("&4Team shop lobby", Profession.LIBRARIAN, GuiType.TEAM_LOBBY);
	
  private final String shopName;
  private final Profession villagerProfession;
  private final GuiType guiType;

  ShopType(final String shopName, final Profession villagerProfession, final GuiType guiType) {
    this.shopName = DataUtils.color(shopName);
    this.villagerProfession = villagerProfession;
    this.guiType = guiType;
  }

  public String getShopName() {
    return this.shopName;
  }

  public Profession getVillagerProfession() {
    return this.villagerProfession;
  }

  public GuiType getGuiType() {
    return this.guiType;
  }

}
