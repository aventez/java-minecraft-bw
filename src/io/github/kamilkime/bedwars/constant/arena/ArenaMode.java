package io.github.kamilkime.bedwars.constant.arena;

import java.util.Arrays;
import java.util.List;

import io.github.kamilkime.bedwars.constant.team.TeamColor;

public enum ArenaMode {

  SOLO(0x01, 1, 2, 8, Arrays.asList(TeamColor.values())),
  DUO(0x02, 2, 4, 16, Arrays.asList(TeamColor.values())),
  TRIO(0x03, 3, 6, 12, Arrays.asList(TeamColor.RED, TeamColor.GREEN, TeamColor.BLUE, TeamColor.YELLOW)),
  QUAD(0x04, 4, 8, 16, Arrays.asList(TeamColor.RED, TeamColor.GREEN, TeamColor.BLUE, TeamColor.YELLOW));

  private final byte id;
  private final int teamPlayers;
  private final int minPlayers;
  private final int maxPlayers;

  private final List<TeamColor> teamColors;

  ArenaMode(final int id, final int teamPlayers, final int minPlayers, final int maxPlayers, final List<TeamColor> teamColors) {
    this.id = (byte) id;
    this.teamPlayers = teamPlayers;
    this.minPlayers = minPlayers;
    this.maxPlayers = maxPlayers;
    this.teamColors = teamColors;
  }

  public byte getID() {
    return this.id;
  }

  public int getTeamPlayers() {
    return this.teamPlayers;
  }

  public int getMinPlayers() {
    return this.minPlayers;
  }

  public int getMaxPlayers() {
    return this.maxPlayers;
  }

  public List<TeamColor> getTeamColors() {
    return this.teamColors;
  }

}
