package io.github.kamilkime.bedwars.constant.user;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.constant.gui.GuiConstants;
import io.github.kamilkime.bedwars.constant.gui.ItemBuilder;

public enum ToolTier {

  NONE(new ItemStack(Material.AIR, 1), new ItemStack(Material.AIR, 1), null),
  WOOD(GuiConstants.PERSONAL_TOOL_PICKAXE_WOOD, GuiConstants.PERSONAL_TOOL_AXE_WOOD, new ItemStack(Material.IRON_INGOT, 20)),
  STONE(GuiConstants.PERSONAL_TOOL_PICKAXE_STONE, GuiConstants.PERSONAL_TOOL_AXE_STONE, new ItemStack(Material.IRON_INGOT, 40)),
  IRON(GuiConstants.PERSONAL_TOOL_PICKAXE_IRON, GuiConstants.PERSONAL_TOOL_AXE_IRON, new ItemStack(Material.IRON_INGOT, 64)),
  DIAMOND_I(GuiConstants.PERSONAL_TOOL_PICKAXE_DIAMOND_I, GuiConstants.PERSONAL_TOOL_AXE_DIAMOND_I, new ItemStack(Material.GOLD_INGOT, 5)),
  DIAMOND_II(GuiConstants.PERSONAL_TOOL_PICKAXE_DIAMOND_II, GuiConstants.PERSONAL_TOOL_AXE_DIAMOND_II, new ItemStack(Material.GOLD_INGOT, 15));

  public static final ToolTier MAX_TIER = ToolTier.DIAMOND_II;
  private static final Map<ToolTier, ToolTier> prev = new HashMap<>();
  private static final Map<ToolTier, ToolTier> next = new HashMap<>();

  static {
    prev.put(ToolTier.NONE, ToolTier.NONE);
    prev.put(ToolTier.WOOD, ToolTier.WOOD);
    prev.put(ToolTier.STONE, ToolTier.WOOD);
    prev.put(ToolTier.IRON, ToolTier.STONE);
    prev.put(ToolTier.DIAMOND_I, ToolTier.IRON);
    prev.put(ToolTier.DIAMOND_II, ToolTier.DIAMOND_I);

    next.put(ToolTier.NONE, ToolTier.WOOD);
    next.put(ToolTier.WOOD, ToolTier.STONE);
    next.put(ToolTier.STONE, ToolTier.IRON);
    next.put(ToolTier.IRON, ToolTier.DIAMOND_I);
    next.put(ToolTier.DIAMOND_I, ToolTier.DIAMOND_II);
    next.put(ToolTier.DIAMOND_II, ToolTier.DIAMOND_II);
  }

  private final ItemStack pickaxeItem;
  private final ItemStack axeItem;
  private final ItemStack pickaxeGuiItem;
  private final ItemStack axeGuiItem;
  private final ItemStack cost;

  ToolTier(final ItemStack pickaxeItem, final ItemStack axeItem, final ItemStack cost) {
    this.pickaxeItem = ItemBuilder.of(pickaxeItem).clearName().clearLore().build();
    this.axeItem = ItemBuilder.of(axeItem).clearName().clearLore().build();

    this.pickaxeGuiItem = pickaxeItem;
    this.axeGuiItem = axeItem;

    this.cost = cost;
  }

  public static ToolTier getPrev(final ToolTier toolTier) {
    return prev.get(toolTier);
  }

  public static ToolTier getNext(final ToolTier toolTier) {
    return next.get(toolTier);
  }

  public ItemStack getPickaxeItem() {
    return this.pickaxeItem.clone();
  }

  public ItemStack getAxeItem() {
    return this.axeItem.clone();
  }

  public ItemStack getPickaxeGuiItem() {
    return this.pickaxeGuiItem.clone();
  }

  public ItemStack getAxeGuiItem() {
    return this.axeGuiItem.clone();
  }

  public ItemStack getCost() {
    return this.cost;
  }

}
