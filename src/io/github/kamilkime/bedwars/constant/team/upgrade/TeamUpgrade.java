package io.github.kamilkime.bedwars.constant.team.upgrade;

public enum TeamUpgrade {

  PROTECTION_I(false),
  PROTECTION_II(false),
  PROTECTION_III(false),
  PROTECTION_IV(false),
  SHARPNESS_I(false),
  HASTE_I(false),
  HASTE_II(false),
  GENERATOR_I(false),
  GENERATOR_II(false),
  GENERATOR_III(false),
  HEAL_POOL(false),
  TRAP_BLINDNESS(true),
  TRAP_QUICK_RETURN(true),
  TRAP_INVISIBLE_FINDER(true),
  TRAP_SLOW(true);

  private boolean trap;

  TeamUpgrade(boolean trap) {
    this.trap = trap;
  }

  public boolean isTrap() {
    return trap;
  }
}
