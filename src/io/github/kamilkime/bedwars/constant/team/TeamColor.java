package io.github.kamilkime.bedwars.constant.team;

import org.bukkit.DyeColor;

import io.github.kamilkime.bedwars.data.DataUtils;

public enum TeamColor {

  RED('c', DyeColor.RED, 0.25D, "&cRED", "&c[RED]", "&c&lR &r&c", "Red"),
  GREEN('a', DyeColor.LIME, 0.917D, "&aGREEN", "&a[GREEN]", "&a&lG &r&a", "Green"),
  BLUE('9', DyeColor.BLUE, 0.583D, "&9BLUE", "&9[BLUE]", "&9&lB &r&9", "Blue"),
  YELLOW('e', DyeColor.YELLOW, 0.292D, "&eYELLOW", "&e[YELLOW]", "&e&lY &r&e", "Yellow"),
  AQUA('b', DyeColor.CYAN, 0.542D, "&bAQUA", "&b[AQUA]", "&b&lA &r&b", "Aqua"),
  GRAY('8', DyeColor.GRAY, 0.417D, "&8GRAY", "&8[GRAY]", "&8&lS &r&8", "Gray"),
  PINK('d', DyeColor.PINK, 0.208D, "&dPINK", "&d[PINK]", "&d&lP &r&d", "Pink"),
  WHITE('f', DyeColor.WHITE, 32768.0D, "&fWHITE", "&f[WHITE]", "&f&lW &r&f", "White");

  private final char colorCode;
  private final DyeColor dyeColor;
  private final double noteColor;
  private final String teamName;
  private final String chatPerfix;
  private final String scoreboardPrefix;
  private final String simpleName;

  TeamColor(final char colorCode, final DyeColor dyeColor, final double noteColor, final String teamName, final String chatPerfix,
            final String scoreboardPrefix, final String simpleName) {
    this.colorCode = colorCode;
    this.dyeColor = dyeColor;
    this.noteColor = noteColor;
    this.teamName = DataUtils.color(teamName);
    this.chatPerfix = DataUtils.color(chatPerfix);
    this.scoreboardPrefix = DataUtils.color(scoreboardPrefix);
    this.simpleName = simpleName;
  }

  public char getColorCode() {
    return this.colorCode;
  }

  public DyeColor getDyeColor() {
    return this.dyeColor;
  }

  public double getNoteColor() {
    return this.noteColor;
  }

  public String getTeamName() {
    return this.teamName;
  }

  public String getChatPrefix() {
    return this.chatPerfix;
  }

  public String getScoreboardPrefix() {
    return this.scoreboardPrefix;
  }

  public String getSimpleName() {
    return this.simpleName;
  }

}
