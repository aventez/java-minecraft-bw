package io.github.kamilkime.bedwars.constant;

import java.util.ArrayList;
import java.util.List;

import io.github.kamilkime.bedwars.data.DataUtils;

public final class Messages {

  public static final String ARENA_START_CANCEL;
  public static final String ARENA_START_IN;
  public static final String ARENA_JOIN;
  public static final String ARENA_LEAVE;
  public static final String ARENA_WINNING_TEAM;
  public static final String ARENA_TEAM_ELIMINATED;
  public static final String ARENA_BED_DESTROYED_TITLE;
  public static final String ARENA_BED_DESTROYED_SUBTITLE;
  public static final String ARENA_BED_DESTROYED;
  public static final String ARENA_ONE_TURRET_PER_USER;

  public static final String ARENA_TURRET_PLACE_CANCEL;
  public static final String ARENA_PLACE_CANCEL_HEIGHT;
  public static final String ARENA_PLACE_CANCEL_TEAM_SPAWN;
  public static final String ARENA_PLACE_CANCEL_TEAM_DEATHMATCH_SPAWN;
  public static final String ARENA_PLACE_CANCEL_GENERATOR;

  public static final String ARENA_DEATH_TITLE;
  public static final String ARENA_DEATH_SUBTITLE;
  public static final String ARENA_DEATH;
  public static final String ARENA_FINAL_DEATH;
  public static final String ARENA_KILL;
  public static final String ARENA_FINAL_KILL;
  public static final String ARENA_ITEMS_TRANSFERED;
  public static final String ARENA_ITEM_TRANSFER_ENTRY;

  public static final String ARENA_SILVERFISH_LIMIT;
  public static final String ARENA_GOLEM_LIMIT;

  public static final String ARENA_BLINDNESS_TRAP_ACTIVATED_TITLE;
  public static final String ARENA_BLINDNESS_TRAP_ACTIVATED_SUBTITLE;
  public static final String ARENA_SLOW_TRAP_ACTIVATED_TITLE;
  public static final String ARENA_SLOW_TRAP_ACTIVATED_SUBTITLE;
  public static final String ARENA_INVISIBLE_TRAP_ACTIVATED_TITLE;
  public static final String ARENA_INVISIBLE_TRAP_ACTIVATED_SUBTITLE;
  public static final String ARENA_INVISIBLE_TRAP_REVEALED_TITLE;
  public static final String ARENA_INVISIBLE_TRAP_REVEALED_SUBTITLE;
  public static final String ARENA_QUICK_RETURN_TRAP_ACTIVATED_TITLE;
  public static final String ARENA_QUICK_RETURN_TRAP_ACTIVATED_SUBTITLE;

  public static final String ARENA_TRAP_IMMUNITY_ON;

  public static final String BUILDMODE_PERSONAL_SHOP_CREATED;
  public static final String BUILDMODE_TEAM_SHOP_CREATED;
  public static final String BUILDMODE_TEAM_SPAWN_CREATED;
  public static final String BUILDMODE_TEAM_DEATHMATCH_SPAWN_CREATED;
  public static final String BUILDMODE_TEAM_GENERATOR_CREATED;
  public static final String BUILDMODE_TEAM_CHEST_CREATED;
  public static final String BUILDMODE_TEAM_BED_CREATED;
  public static final String BUILDMODE_MAIN_LOBBY_CREATED;
  public static final String BUILDMODE_SPECTATOR_LOBBY_CREATED;
  public static final String BUILDMODE_ARENA_CENTER_CREATED;
  public static final String BUILDMODE_GENERATOR_CREATED;

  public static final String BUILDMODE_SHOP_REMOVED;
  public static final String BUILDMODE_TEAM_SPAWN_REMOVED;
  public static final String BUILDMODE_LOBBY_SHOP_CREATED;
  public static final String BUILDMODE_TEAM_DEATHMATCH_SPAWN_REMOVED;
  public static final String BUILDMODE_TEAM_GENERATOR_REMOVED;
  public static final String BUILDMODE_TEAM_CHEST_REMOVED;
  public static final String BUILDMODE_TEAM_BED_REMOVED;
  public static final String BUILDMODE_MAIN_LOBBY_REMOVED;
  public static final String BUILDMODE_SPECTATOR_LOBBY_REMOVED;
  public static final String BUILDMODE_ARENA_CENTER_REMOVED;
  public static final String BUILDMODE_GENERATOR_REMOVED;

  public static final String USER_LVL_UP;
  public static final String USER_BOMBER_FIRED;
  public static final String USER_CMD_NOT_ALLOWED;

  public static final String CMD_NO_PERMISSION;
  public static final String CMD_NOT_ONLINE;
  public static final String CMD_CORRECT_USAGE;
  public static final String CMD_UNKNOWN_ARGUMENT;
  public static final String CMD_NOT_A_NUMBER;

  public static final String CMD_NO_ARENA_FOUND;
  public static final String CMD_NO_TEAM_FOUND;
  public static final String CMD_NO_USER_FOUND;
  public static final String CMD_NO_PERK_FOUND;
  public static final String CMD_NO_ARENA_MODE_FOUND;
  public static final String CMD_NO_WE_SCHEMATIC_FOUND;
  public static final String CMD_NO_ARENA_SCHEMATIC_FOUND;
  public static final String CMD_NO_TEAM_ON_ARENA;
  public static final String CMD_UNKNOWN_MODE;

  public static final String CMD_ARENA_ALREADY_EXISTS;
  public static final String CMD_SCHEMATIC_ALREADY_EXISTS;

  public static final String CMD_NOT_IN_GAME_TO_LEAVE;
  public static final String CMD_NOT_IN_GAME_TO_SHOUT;
  public static final String CMD_ALREADY_IN_GAME;
  public static final String CMD_CANT_JOIN;

  public static final String CMD_BUILDING_ARENA;
  public static final String CMD_ARENA_INCORRECT;
  public static final String CMD_ARENA_CORRECT;
  public static final String CMD_ARENA_NOT_ENABLED;
  public static final String CMD_ARENA_NOT_DISABLED;
  public static final String CMD_CANT_COPY_ARENA;
  public static final String CMD_ARENA_COPIED;
  public static final String CMD_ARENA_COPY_ERROR;
  public static final String CMD_NO_WE_SELECTION;
  public static final String CMD_ARENA_CREATED;
  public static final String CMD_ARENA_DELETED;
  public static final String CMD_ARENA_DISABLED;
  public static final String CMD_ARENA_ENABLED;
  public static final String CMD_CANT_ENABLE_WHILE_BUILD;
  public static final String CMD_ARENA_PASTED;
  public static final String CMD_ARENA_PASTE_ERROR;
  public static final String CMD_START_BUILDING;
  public static final String CMD_STOP_BUILDING;

  public static final String CMD_USER_BEDS_CHANGED;
  public static final String CMD_USER_DEATHS_CHANGED;
  public static final String CMD_USER_FINALKILLS_CHANGED;
  public static final String CMD_USER_KILLS_CHANGED;
  public static final String CMD_USER_MONEY_CHANGED;
  public static final String CMD_USER_XP_CHANGED;
  public static final String CMD_USER_PERK_CHANGED;
  public static final String CMD_USER_RESET;

  public static final String CMD_SHOUT_NOT_ALIVE;

  public static final List<String> CMD_ARENA_HELP;
  public static final List<String> CMD_USER_HELP;
  
  public static final String SHOP_LOBBY_DISABLED;

  static {
    ARENA_START_CANCEL = DataUtils.color("&7&l[BedWars] &cGame start cancelled!");
    ARENA_START_IN = DataUtils.color("&7&l[BedWars] &aGame starts in &7{TIME} &aseconds");
    ARENA_JOIN = DataUtils.color("&7&l[BedWars] &a{PLAYER} joined the game &7({NUM}/{MAX})");
    ARENA_LEAVE = DataUtils.color("&7&l[BedWars] &a{PLAYER} left the game!");
    ARENA_WINNING_TEAM = DataUtils.color("&7&l[BedWars] &aTeam {TEAM} &ahas won the game!");
    ARENA_TEAM_ELIMINATED = DataUtils.color("&7&l[BedWars] &cTeam {TEAM} &chas been eliminated!");
    ARENA_BED_DESTROYED_TITLE = "&c&lLozko zniszczone!";
    ARENA_BED_DESTROYED_SUBTITLE = DataUtils.color("&7Juz wiecej sie nie odrodzisz po smierci!");
    ARENA_BED_DESTROYED = DataUtils.color("&7&l[BedWars] {TEAM} &cteams bed has been destroyed by {BREAKER}&c!");
    ARENA_ONE_TURRET_PER_USER = DataUtils.color("&7&l[BedWars] &cYou can only have one active turret!");

    ARENA_TURRET_PLACE_CANCEL = DataUtils.color("&7&l[BedWars] &cYou can only place turrets in &712 blocks &crange from your team spawn!");
    ARENA_PLACE_CANCEL_HEIGHT = DataUtils.color("&7&l[BedWars] &cYou can only place blocks up to &7Y=40&c!");
    ARENA_PLACE_CANCEL_TEAM_SPAWN = DataUtils.color("&7&l[BedWars] &cYou can't place blocks in &76 blocks &crange from a team spawn!");
    ARENA_PLACE_CANCEL_TEAM_DEATHMATCH_SPAWN = DataUtils.color("&7&l[BedWars] &cYou can't place blocks in &74 blocks &crange from a team deathmatch spawn!");
    ARENA_PLACE_CANCEL_GENERATOR = DataUtils.color("&7&l[BedWars] &cYou can't place blocks in &74 blocks &crange from a generator!");

    ARENA_DEATH_TITLE = DataUtils.color("&c&lZginales!");
    ARENA_DEATH_SUBTITLE = DataUtils.color("&7Odrodzisz sie za %time% sekund!");
    ARENA_DEATH = DataUtils.color("&7&l[BedWars] &c{PLAYER} died");
    ARENA_FINAL_DEATH = DataUtils.color("&7&l[BedWars] &c{PLAYER} died &7(FINAL DEATH)");
    ARENA_KILL = DataUtils.color("&7&l[BedWars] &c{PLAYER} was killed by {KILLER}");
    ARENA_FINAL_KILL = DataUtils.color("&7&l[BedWars] &c{PLAYER} was killed by {KILLER} &7(FINAL KILL)");
    ARENA_ITEMS_TRANSFERED = DataUtils.color("&7&l[BedWars] &aYou have aquired the following items:");
    ARENA_ITEM_TRANSFER_ENTRY = DataUtils.color("&7� &a{ITEM} &7x{AMOUNT}");

    ARENA_SILVERFISH_LIMIT = DataUtils.color("&7&l[BedWars] &cYou can only have &75 &csilverfish at a time!");
    ARENA_GOLEM_LIMIT = DataUtils.color("&7&l[BedWars] &cYou can only have &72 &cgolems at a time!");

    ARENA_BLINDNESS_TRAP_ACTIVATED_TITLE = DataUtils.color("&c&lINTRUDER ALERT");
    ARENA_BLINDNESS_TRAP_ACTIVATED_SUBTITLE = DataUtils.color("&7&lBLINDNESS TRAP ACTIVATED");
    ARENA_INVISIBLE_TRAP_ACTIVATED_TITLE = DataUtils.color("&c&lINTRUDER ALERT");
    ARENA_INVISIBLE_TRAP_ACTIVATED_SUBTITLE = DataUtils.color("&7&lINVISIBLE INTRUDER REVEALED!");
    ARENA_INVISIBLE_TRAP_REVEALED_TITLE = DataUtils.color("&c&lYOU ARE REVEALED");
    ARENA_INVISIBLE_TRAP_REVEALED_SUBTITLE = DataUtils.color("&7&lENEMY'S TRAP HAS BEEN ACTIVATED!");
    ARENA_SLOW_TRAP_ACTIVATED_TITLE = DataUtils.color("&c&lINTRUDER ALERT");
    ARENA_SLOW_TRAP_ACTIVATED_SUBTITLE = DataUtils.color("&7&lSLOW DIGGING TRAP ACTIVATED");
    ARENA_QUICK_RETURN_TRAP_ACTIVATED_TITLE = DataUtils.color("&c&lINTRUDER ALERT");
    ARENA_QUICK_RETURN_TRAP_ACTIVATED_SUBTITLE = DataUtils.color("&7&lSPEED I APPLIED, GO BACK!");

    ARENA_TRAP_IMMUNITY_ON = DataUtils.color("&7&l[BedWars] &aImmunity for all traps activated for &c60 seconds&a!");

    BUILDMODE_PERSONAL_SHOP_CREATED = DataUtils.color("&7&l[BedWars] &aPersonal shop created!");
    BUILDMODE_TEAM_SHOP_CREATED = DataUtils.color("&7&l[BedWars] &Team shop created!");
    BUILDMODE_TEAM_SPAWN_CREATED = DataUtils.color("&7&l[BedWars] &aSpawn of team {TEAM} &aset!");
    BUILDMODE_TEAM_DEATHMATCH_SPAWN_CREATED = DataUtils.color("&7&l[BedWars] &aDeathmatch spawn of team {TEAM} &aset!");
    BUILDMODE_TEAM_GENERATOR_CREATED = DataUtils.color("&7&l[BedWars] &aGenerator of team {TEAM} &aset!");
    BUILDMODE_TEAM_CHEST_CREATED = DataUtils.color("&7&l[BedWars] &aChest of team {TEAM} &aset!");
    BUILDMODE_TEAM_BED_CREATED = DataUtils.color("&7&l[BedWars] &aBed of team {TEAM} &aset!");
    BUILDMODE_MAIN_LOBBY_CREATED = DataUtils.color("&7&l[BedWars] &aArena main lobby location set!");
    BUILDMODE_SPECTATOR_LOBBY_CREATED = DataUtils.color("&7&l[BedWars] &aArena spectator lobby location set!");
    BUILDMODE_ARENA_CENTER_CREATED = DataUtils.color("&7&l[BedWars] &aArena center location set!");
    BUILDMODE_GENERATOR_CREATED = DataUtils.color("&7&l[BedWars] &aGenerator of type {TYPE} created!");
    BUILDMODE_LOBBY_SHOP_CREATED = DataUtils.color("&7&l[BedWars] &aPomyslnie stworzono sklep w lobby.");
    
    BUILDMODE_SHOP_REMOVED = DataUtils.color("&7&l[BedWars] &cShop of type {TYPE} removed!");
    BUILDMODE_TEAM_SPAWN_REMOVED = DataUtils.color("&7&l[BedWars] &cSpawn of team {TEAM} &cremoved!");
    BUILDMODE_TEAM_DEATHMATCH_SPAWN_REMOVED = DataUtils.color("&7&l[BedWars] &cDeathmatch spawn of team {TEAM} &cremoved!");
    BUILDMODE_TEAM_GENERATOR_REMOVED = DataUtils.color("&7&l[BedWars] &cGenerator of team {TEAM} &cremoved!");
    BUILDMODE_TEAM_CHEST_REMOVED = DataUtils.color("&7&l[BedWars] &cChest of team {TEAM} &cremoved!");
    BUILDMODE_TEAM_BED_REMOVED = DataUtils.color("&7&l[BedWars] &cBed of team {TEAM} &cremoved!");
    BUILDMODE_MAIN_LOBBY_REMOVED = DataUtils.color("&7&l[BedWars] &cArena main lobby location removed!");
    BUILDMODE_SPECTATOR_LOBBY_REMOVED = DataUtils.color("&7&l[BedWars] &cArena spectator lobby location removed!");
    BUILDMODE_ARENA_CENTER_REMOVED = DataUtils.color("&7&l[BedWars] &cArena center location removed!");
    BUILDMODE_GENERATOR_REMOVED = DataUtils.color("&7&l[BedWars] &cGenerator of type {TYPE} removed!");

    USER_LVL_UP = DataUtils.color("&7&l[BedWars] &aLVL UP! You now have level &7{LEVEL}");
    USER_BOMBER_FIRED = DataUtils.color("&7&l[BedWars] &aYour &7BOMBER &aperk has been activated!");
    USER_CMD_NOT_ALLOWED = DataUtils.color("&7&l[BedWars] &cYou can't use that command now!");

    CMD_NO_PERMISSION = DataUtils.color("&7&l[BedWars] &cNo permission!");
    CMD_NOT_ONLINE = DataUtils.color("&7&l[BedWars] &cYou must be an online player!");
    CMD_CORRECT_USAGE = DataUtils.color("&7&l[BedWars] &cCorrect usage: {USAGE}");
    CMD_UNKNOWN_ARGUMENT = DataUtils.color("&7&l[BedWars] &cUnknown argument, use &7{HELP} &cfor help!");
    CMD_NOT_A_NUMBER = DataUtils.color("&7&l[BedWars] &c{ARG} is not a valid number!");

    CMD_NO_ARENA_FOUND = DataUtils.color("&7&l[BedWars] &cThere is no arena with such name!");
    CMD_NO_TEAM_FOUND = DataUtils.color("&7&l[BedWars] &cNo team color found with name {NAME}");
    CMD_NO_USER_FOUND = DataUtils.color("&7&l[BedWars] &cThere is no user with such name!");
    CMD_NO_PERK_FOUND = DataUtils.color("&7&l[BedWars] &cThere is no perk with such name!");
    CMD_NO_ARENA_MODE_FOUND = DataUtils.color("&7&l[BedWars] &cNo arena mode found with name {NAME}");
    CMD_NO_WE_SCHEMATIC_FOUND = DataUtils.color("&7&l[BedWars] &cNo schematic file found under: {PATH}");
    CMD_NO_ARENA_SCHEMATIC_FOUND = DataUtils.color("&7&l[BedWars] &cThere is no such arena schematic as {SCHEM}");
    CMD_NO_TEAM_ON_ARENA = DataUtils.color("&7&l[BedWars] &cThis arena doesn't have that team!");
    CMD_ARENA_NOT_ENABLED = DataUtils.color("&7&l[BedWars] &cThe arena is not enabled!");

    CMD_ARENA_ALREADY_EXISTS = DataUtils.color("&7&l[BedWars] &cThere already is an arena with such name!");
    CMD_SCHEMATIC_ALREADY_EXISTS = DataUtils.color("&7&l[BedWars] &cThere already is a schematic for arena {ARENA}!");

    CMD_NOT_IN_GAME_TO_LEAVE = DataUtils.color("&7&l[BedWars] &cYou must be in a game to leave it!");
    CMD_NOT_IN_GAME_TO_SHOUT = DataUtils.color("&7&l[BedWars] &cYou must be in a game to shout!");
    CMD_ALREADY_IN_GAME = DataUtils.color("&7&l[BedWars] &cYou already are playing!");
    CMD_CANT_JOIN = DataUtils.color("&7&l[BedWars] &cYou couldn't join the arena due to: {REASON}");

    CMD_BUILDING_ARENA = DataUtils.color("&7&l[BedWars] &cYou can't play while building!");
    CMD_ARENA_INCORRECT = DataUtils.color("&7&l[BedWars] &cThe arena is incomplete due to: {REASON}");
    CMD_ARENA_CORRECT = DataUtils.color("&7&l[BedWars] &aThe arena is complete and can be enabled!");
    CMD_UNKNOWN_MODE = DataUtils.color("&7&l[BedWars] &cUnkown mode, available modes: add, remove, set");
    CMD_ARENA_NOT_DISABLED = DataUtils.color("&7&l[BedWars] &cThe arena is not disabled!");
    CMD_CANT_COPY_ARENA = DataUtils.color("&7&l[BedWars] &cArena cannot be copied due to: {REASON}");
    CMD_ARENA_COPIED = DataUtils.color("&7&l[BedWars] &aArena {ARENA} copied with schematic {SCHEM}");
    CMD_ARENA_COPY_ERROR = DataUtils.color("&7&l[BedWars] &cSchematic creation failed, see more info in the server console!");
    CMD_NO_WE_SELECTION = DataUtils.color("&7&l[BedWars] &cYou must select a WorldEdit area first!");
    CMD_ARENA_CREATED = DataUtils.color("&7&l[BedWars] &aArena with name {NAME} has been created, enabling build mode for it!");
    CMD_ARENA_DELETED = DataUtils.color("&7&l[BedWars] &cArena {ARENA} has been deleted!");
    CMD_ARENA_DISABLED = DataUtils.color("&7&l[BedWars] &cArena {ARENA} has been disabled!");
    CMD_ARENA_ENABLED = DataUtils.color("&7&l[BedWars] &aArena {ARENA} has been enabled!");
    CMD_CANT_ENABLE_WHILE_BUILD = DataUtils.color("&7&l[BedWars] &cArena cannot be enabled while someone is building it!");
    CMD_ARENA_PASTED = DataUtils.color("&7&l[BedWars] &aArena {ARENA} created from schematic {SCHEM} and started!");
    CMD_ARENA_PASTE_ERROR = DataUtils.color("&7&l[BedWars] &cArena creation failed, see more info in the server console!");

    CMD_START_BUILDING = DataUtils.color("&7&l[BedWars] &cYou've started building arena {ARENA}");
    CMD_STOP_BUILDING = DataUtils.color("&7&l[BedWars] &cYou've stopped building arena {ARENA}");

    CMD_USER_BEDS_CHANGED = DataUtils.color("&7&l[BedWars] &aDestroyed beds of user &7{USER} &achanged to &7{VALUE}&a!");
    CMD_USER_DEATHS_CHANGED = DataUtils.color("&7&l[BedWars] &aDeaths of user &7{USER} &achanged to &7{VALUE}&a!");
    CMD_USER_FINALKILLS_CHANGED = DataUtils.color("&7&l[BedWars] &aFinal kills beds of user &7{USER} &achanged to &7{VALUE}&a!");
    CMD_USER_KILLS_CHANGED = DataUtils.color("&7&l[BedWars] &aKills of user &7{USER} &achanged to &7{VALUE}&a!");
    CMD_USER_MONEY_CHANGED = DataUtils.color("&7&l[BedWars] &aMoney of user &7{USER} &achanged to &7{VALUE}&a!");
    CMD_USER_XP_CHANGED = DataUtils.color("&7&l[BedWars] &aXP of user &7{USER} &achanged to &7{VALUE}&a!");
    CMD_USER_PERK_CHANGED = DataUtils.color("&7&l[BedWars] &aPerk level of &7{PERK} &afor user &7{USER} &achanged to &7{VALUE}&a!");
    CMD_USER_RESET = DataUtils.color("&7&l[BedWars] &aStats of user &7{USER} &ahave been fully reset!");

    CMD_SHOUT_NOT_ALIVE = DataUtils.color("&7&l[BedWars] &cYou must be alive to use &7/shout");

    CMD_ARENA_HELP = new ArrayList<>();
    CMD_ARENA_HELP.add(DataUtils.color("&8---------------------------------------------------"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena check <arenaName> &8- &7Checks the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena copy <arenaName> <schemName> &8- &7Copies the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena create <arenaName> <arenaMode> &8- &7Creates the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena delete <arenaName> &8- &7Deleted the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena disable <arenaName> &8- &7Disables the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena edit <arenaName> &8- &7Toggles build mode for the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena enable <arenaName> &8- &7Enables the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena help &8- &7Shows these messages"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena join <arenaName> [teamColor] &8- &7Joins the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena leave &8- &7Leaves the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena list &8- &7Lists all arenas"));
    CMD_ARENA_HELP.add(DataUtils.color("&a/arena paste <newArenaName> <schemName> &8- &7Pastes the arena"));
    CMD_ARENA_HELP.add(DataUtils.color("&8---------------------------------------------------"));

    CMD_USER_HELP = new ArrayList<>();
    CMD_USER_HELP.add(DataUtils.color("&8---------------------------------------------------"));
    CMD_USER_HELP.add(DataUtils.color("&a/user beds <mode> <userName> <value> &8- &7Changes the number of users destroyed beds"));
    CMD_USER_HELP.add(DataUtils.color("&a/user deaths <mode> <userName> <value> &8- &7Changes the number of users deaths"));
    CMD_USER_HELP.add(DataUtils.color("&a/user finalkills <mode> <userName> <value> &8- &7Changes the number of users final kills"));
    CMD_USER_HELP.add(DataUtils.color("&a/user help &8- &7Shows these messages"));
    CMD_USER_HELP.add(DataUtils.color("&a/user kills <mode> <userName> <value> &8- &7Changes the number of users kills"));
    CMD_USER_HELP.add(DataUtils.color("&a/user money <mode> <userName> <value> &8- &7Changes the users money"));
    CMD_USER_HELP.add(DataUtils.color("&a/user perk <perkName> <userName> <level> &8- &7Changes the users perk level"));
    CMD_USER_HELP.add(DataUtils.color("&a/user reset <userName> &8- &7Resets all users stats"));
    CMD_USER_HELP.add(DataUtils.color("&a/user stats <userName> &8- &7Shows users stats"));
    CMD_USER_HELP.add(DataUtils.color("&a/user xp <mode> <userName> <value> &8- &7Changes the users xp"));
    CMD_USER_HELP.add(DataUtils.color("&8---------------------------------------------------"));
    
    SHOP_LOBBY_DISABLED = DataUtils.color("&7&l[BedWars] &aSklep jest wylaczony.");
  }

  private Messages() {
  }

}
