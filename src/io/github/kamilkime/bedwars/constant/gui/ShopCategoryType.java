package io.github.kamilkime.bedwars.constant.gui;

import io.github.kamilkime.bedwars.data.DataUtils;

/**
 * @author Plajer
 * <p>
 * Created at 21.02.2019
 */
public enum ShopCategoryType {

  BLOCK(DataUtils.color("&5&lBlocks"), 1), WEAPON(DataUtils.color("&5&lWeapons"), 2),
  BOW(DataUtils.color("&5&lBows"), 4), TURRET(DataUtils.color("&5&lTurrets"), 7),
  POTION(DataUtils.color("&5&lPotions"), 5), UTILITY(DataUtils.color("&5&lUtilities"), 6),
  TOOL(DataUtils.color("&5&lTools"), 3),
  ;

  private String inventoryName;
  private int position;

  ShopCategoryType(String inventoryName, int position) {
    this.inventoryName = inventoryName;
    this.position = position;
  }

  public String getInventoryName() {
    return inventoryName;
  }

  public int getPosition() {
    return position;
  }

}
