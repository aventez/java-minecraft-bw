package io.github.kamilkime.bedwars.constant.gui;

import java.util.HashMap;
import java.util.Map;

import io.github.kamilkime.bedwars.data.DataUtils;

public enum GuiType {

  TEAM(DataUtils.color("&4&lTeam upgrades"), 9 * 5),
  TEAM_LOBBY(DataUtils.color("&4&lTeam upgrades (lobby)"), 9 * 3),
  TEAM_TRAPS(DataUtils.color("&0Queue a trap"), 9 * 3),
  PERSONAL_BLOCK(DataUtils.color("Blocks"), 9 * 6),
  PERSONAL_WEAPON(DataUtils.color("Melee and Armor"), 9 * 6),
  PERSONAL_BOW(DataUtils.color("Ranged"), 9 * 6),
  PERSONAL_TURRET(DataUtils.color("Turrets"), 9 * 6),
  PERSONAL_POTION(DataUtils.color("Potions"), 9 * 6),
  PERSONAL_UTILITY(DataUtils.color("Utilities"), 9 * 6),
  PERSONAL_TOOL(DataUtils.color("Tools"), 9 * 6),
  PERSONAL_QUICK_SHOP(DataUtils.color("Quick Shop"), 9 * 6),
  QUICK_SHOP_PERSONALIZER(DataUtils.color("Adding to Quick Buy..."), 9 * 6);

  private static final Map<String, GuiType> GUI_TYPES = new HashMap<>();

  static {
    for (final GuiType guiType : GuiType.values()) {
      GUI_TYPES.put(guiType.getGuiName(), guiType);
    }
  }

  private final String guiName;
  private final int guiSize;

  GuiType(final String guiName, final int guiSize) {
    this.guiName = guiName;
    this.guiSize = guiSize;
  }

  public static GuiType getGuiType(final String guiName) {
    return GUI_TYPES.get(guiName);
  }

  public String getGuiName() {
    return this.guiName;
  }

  public int getGuiSize() {
    return this.guiSize;
  }

}
