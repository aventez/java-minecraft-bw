package io.github.kamilkime.bedwars.constant.gui;

public enum PersonalLobbyUpgrade {
	EXTRA_ARROWS(),
	POSTERS(),
	LAST_KILL_EFFECTS(),
	LAST_WORDS(),
	ARROW_BACK(),
	FIRE_ARROW(),
	FAST_START(),
	EXPERT_WEAPON(),
	ANTI_FALL(),
	MEDIC(),
	ARROW_POWERFUL();
}