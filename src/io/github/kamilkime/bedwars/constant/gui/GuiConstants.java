package io.github.kamilkime.bedwars.constant.gui;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.constant.team.TeamColor;
import io.github.kamilkime.bedwars.constant.gui.TeamLobbyUpgrade;
import io.github.kamilkime.bedwars.constant.team.upgrade.TeamUpgrade;
import io.github.kamilkime.bedwars.constant.user.ToolTier;
import io.github.kamilkime.bedwars.data.QuickShopItem;
import io.github.kamilkime.bedwars.gui.GuiItem;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.upgrade.TeamUpgradeUtils;
import io.github.kamilkime.bedwars.turret.impl.ArrowTurret;
import io.github.kamilkime.bedwars.turret.impl.FireballTurret;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public final class GuiConstants {

  public static final Map<TeamColor, ItemStack> TEAM_WOOLS = new HashMap<>();
  public static final Map<TeamColor, ItemStack> TEAM_STAINED_CLAY = new HashMap<>();
  public static final Map<TeamColor, ItemStack> TEAM_STAINED_GLASS = new HashMap<>();

  public static final ItemStack BACK_TO_PERSONAL;
  public static final ItemStack CURRENT_CATEGORY_SELECTOR;

  public static final ItemStack QUICK_SHOP_PERSONALIZER_EMPTY_SLOT;
  public static final ItemStack QUICK_SHOP_MENU_EMPTY_SLOT;

  public static final ItemStack PERSONAL_QUICK_SHOP;
  public static final ItemStack PERSONAL_BLOCK;
  public static final ItemStack PERSONAL_WEAPON;
  public static final ItemStack PERSONAL_BOW;
  public static final ItemStack PERSONAL_TURRET;
  public static final ItemStack PERSONAL_POTION;
  public static final ItemStack PERSONAL_UTILITY;
  public static final ItemStack PERSONAL_TOOL;

  public static final ItemStack PERSONAL_BLOCK_WOOL;
  public static final ItemStack PERSONAL_BLOCK_WOOD;
  public static final ItemStack PERSONAL_BLOCK_ENDSTONE;
  public static final ItemStack PERSONAL_BLOCK_CLAY;
  public static final ItemStack PERSONAL_BLOCK_GLASS;
  public static final ItemStack PERSONAL_BLOCK_OBSIDIAN;

  public static final ItemStack PERSONAL_WEAPON_CHAIN_ARMOR;
  public static final ItemStack PERSONAL_WEAPON_IRON_ARMOR;
  public static final ItemStack PERSONAL_WEAPON_DIAMOND_ARMOR;
  public static final ItemStack PERSONAL_WEAPON_STONE_SWORD;
  public static final ItemStack PERSONAL_WEAPON_IRON_SWORD;
  public static final ItemStack PERSONAL_WEAPON_DIAMOND_SWORD;

  public static final ItemStack PERSONAL_BOW_NORMAL;
  public static final ItemStack PERSONAL_BOW_POWER;
  public static final ItemStack PERSONAL_BOW_POWER_PUNCH;
  public static final ItemStack PERSONAL_BOW_ARROW;

  public static final ItemStack PERSONAL_POTION_INVISIBILITY;
  public static final ItemStack PERSONAL_POTION_JUMP;
  public static final ItemStack PERSONAL_POTION_HASTE;
  public static final ItemStack PERSONAL_POTION_RESISTANCE;

  public static final ItemStack PERSONAL_UTILITY_APPLE;
  public static final ItemStack PERSONAL_UTILITY_FIREBALL;
  public static final ItemStack PERSONAL_UTILITY_TNT;
  public static final ItemStack PERSONAL_UTILITY_SILVERFISH;
  public static final ItemStack PERSONAL_UTILITY_GOLEM;
  public static final ItemStack PERSONAL_UTILITY_WATER;
  public static final ItemStack PERSONAL_UTILITY_PEARL;
  public static final ItemStack PERSONAL_UTILITY_EGG;
  public static final ItemStack PERSONAL_UTILITY_MILK;

  public static final ItemStack PERSONAL_TOOL_SHEARS;
  public static final ItemStack PERSONAL_TOOL_PICKAXE_WOOD;
  public static final ItemStack PERSONAL_TOOL_PICKAXE_STONE;
  public static final ItemStack PERSONAL_TOOL_PICKAXE_IRON;
  public static final ItemStack PERSONAL_TOOL_PICKAXE_DIAMOND_I;
  public static final ItemStack PERSONAL_TOOL_PICKAXE_DIAMOND_II;
  public static final ItemStack PERSONAL_TOOL_PICKAXE_MAX;
  public static final ItemStack PERSONAL_TOOL_AXE_WOOD;
  public static final ItemStack PERSONAL_TOOL_AXE_STONE;
  public static final ItemStack PERSONAL_TOOL_AXE_IRON;
  public static final ItemStack PERSONAL_TOOL_AXE_DIAMOND_I;
  public static final ItemStack PERSONAL_TOOL_AXE_DIAMOND_II;
  public static final ItemStack PERSONAL_TOOL_AXE_MAX;

  public static final ItemStack TEAM_PROTECTION_I;
  public static final ItemStack TEAM_PROTECTION_II;
  public static final ItemStack TEAM_PROTECTION_III;
  public static final ItemStack TEAM_PROTECTION_IV;
  public static final ItemStack TEAM_PROTECTION_MAX;
  public static final ItemStack TEAM_SHARPNESS_I;
  public static final ItemStack TEAM_SHARPNESS_MAX;
  public static final ItemStack TEAM_HASTE_I;
  public static final ItemStack TEAM_HASTE_II;
  public static final ItemStack TEAM_HASTE_MAX;
  public static final ItemStack TEAM_GENERATOR_I;
  public static final ItemStack TEAM_GENERATOR_II;
  public static final ItemStack TEAM_GENERATOR_III;
  public static final ItemStack TEAM_GENERATOR_MAX;
  public static final ItemStack HEAL_POOL;

  //traps
  public static final ItemStack TRAPS_MENU;
  public static final ItemStack NO_TRAP_HERE;
  public static final ItemStack BLINDNESS_TRAP_OFF;
  public static final ItemStack BLINDNESS_TRAP_ON;
  public static final ItemStack QUICK_RETURN_TRAP_OFF;
  public static final ItemStack QUICK_RETURN_TRAP_ON;
  public static final ItemStack INVISIBLE_FINDER_TRAP_OFF;
  public static final ItemStack INVISIBLE_FINDER_TRAP_ON;
  public static final ItemStack SLOW_TRAP_OFF;
  public static final ItemStack SLOW_TRAP_ON;
  
  //team lobby
  public static final ItemStack LOBBY_ZOMBIE;
  public static final ItemStack LOBBY_ZOMBIE_BOUGHT;
  public static final ItemStack LOBBY_EXTRA_ARROW;
  public static final ItemStack LOBBY_EXTRA_ARROW_BOUGHT;

  public static final Map<GuiType, Map<Integer, GuiItem>> GUI_TEMPLATES = new HashMap<>();

  static {
    for (final TeamColor teamColor : TeamColor.values()) {
      TEAM_WOOLS.put(teamColor, ItemBuilder.of(Material.WOOL).amount(16).data(teamColor.getDyeColor().getWoolData()).build());
      TEAM_STAINED_CLAY.put(teamColor, ItemBuilder.of(Material.STAINED_CLAY).amount(16).data(teamColor.getDyeColor().getWoolData()).build());
      TEAM_STAINED_GLASS.put(teamColor, ItemBuilder.of(Material.STAINED_GLASS).amount(8).data(teamColor.getDyeColor().getWoolData()).build());
    }

    //todo all better lores?
    BACK_TO_PERSONAL = ItemBuilder.of(Material.STAINED_GLASS_PANE).data((short) 14).name("&c&lBack").build();
    CURRENT_CATEGORY_SELECTOR = ItemBuilder.of(Material.STAINED_GLASS_PANE).data((short) 5).name(" ").lore("&8⬆ &7Categories", "&8⬇ &7Items").build();

    QUICK_SHOP_PERSONALIZER_EMPTY_SLOT = ItemBuilder.of(Material.STAINED_GLASS_PANE).data((short) 14).name("&cEmpty slot!").lore("&eClick to set!").build();
    QUICK_SHOP_MENU_EMPTY_SLOT = ItemBuilder.of(Material.STAINED_GLASS_PANE).data((short) 14).name("&cEmpty slot!").lore("&7This is a Quick Buy Slot!", "&bSneak Click &7any item in", "&7the shop to add it here.").build();

    PERSONAL_QUICK_SHOP = ItemBuilder.of(Material.NETHER_STAR).name("&aQuick Shop").lore("&eClick to view!").build();
    PERSONAL_BLOCK = ItemBuilder.of(Material.WOOL).name("&aBlocks").lore("&eClick to view!").build();
    PERSONAL_WEAPON = ItemBuilder.of(Material.DIAMOND_SWORD).name("&aMelee and Armor").lore("&eClick to view!").build();
    PERSONAL_BOW = ItemBuilder.of(Material.BOW).name("&aRanged").lore("&eClick to view!").build();
    PERSONAL_TURRET = ItemBuilder.of(Material.DISPENSER).name("&aTurrets").lore("&eClick to view!").build();
    PERSONAL_POTION = ItemBuilder.of(Material.POTION).name("&aPotions").lore("&eClick to view!").build();
    PERSONAL_UTILITY = ItemBuilder.of(Material.TNT).name("&aUtilities").lore("&eClick to view!").build();
    PERSONAL_TOOL = ItemBuilder.of(Material.DIAMOND_PICKAXE).name("&aTools").lore("&eClick to view!").build();

    PERSONAL_BLOCK_WOOL = ItemBuilder.of(Material.WOOL).amount(16).name("&cWool")
        .lore("&7Cost: &f4 Iron", "", "&7Great for bridging across", "&7islands. Turns into your team's", "&7color.").build();
    PERSONAL_BLOCK_WOOD = ItemBuilder.of(Material.WOOD).amount(12).name("&cWood")
        .lore("&7Cost: &64 Gold").build();
    PERSONAL_BLOCK_ENDSTONE = ItemBuilder.of(Material.ENDER_STONE).amount(12).name("&cEnd Stone")
        .lore("&7Cost: &f16 Iron", "", "&7Solid block to defend your bed.").build();
    PERSONAL_BLOCK_CLAY = ItemBuilder.of(Material.STAINED_CLAY).amount(16).name("&cHardened Clay")
        .lore("&7Cost: &f8 Iron", "", "&7Basic block to defend your bed.").build();
    PERSONAL_BLOCK_GLASS = ItemBuilder.of(Material.STAINED_GLASS).amount(8).name("&cStained Glass")
        .lore("&7Cost: &68 Gold").build();
    PERSONAL_BLOCK_OBSIDIAN = ItemBuilder.of(Material.OBSIDIAN).amount(8).name("&cObsidian")
        .lore("&7Cost: &26 Emerald", "", "&7Extreme protection for your bed.").build();

    PERSONAL_WEAPON_CHAIN_ARMOR = ItemBuilder.of(Material.CHAINMAIL_CHESTPLATE).name("&cPermanent Chainmail Armor")
        .lore("&7Cost: &f35 Iron", "", "&7Chainmail leggings and boots", "&7which you will always spawn", "&7with.").build();
    PERSONAL_WEAPON_IRON_ARMOR = ItemBuilder.of(Material.IRON_CHESTPLATE).name("&cPermanent Iron Armor")
        .lore("&7Cost: &610 Gold", "", "&7Iron leggings and boots which", "&7you will always spawn with.").build();
    PERSONAL_WEAPON_DIAMOND_ARMOR = ItemBuilder.of(Material.DIAMOND_CHESTPLATE).name("&cPermanent Diamond Armor")
        .lore("&7Cost: &28 Emerald", "", "&7Diamond leggings and boots which", "&7you will always crush with.").build();

    PERSONAL_WEAPON_STONE_SWORD = ItemBuilder.of(Material.STONE_SWORD).name("&cStone Sword")
        .lore("&7Cost: &f15 Iron").build();
    PERSONAL_WEAPON_IRON_SWORD = ItemBuilder.of(Material.IRON_SWORD).name("&cIron Sword")
        .lore("&7Cost: &68 Gold").build();
    PERSONAL_WEAPON_DIAMOND_SWORD = ItemBuilder.of(Material.DIAMOND_SWORD).name("&cDiamond Sword")
        .lore("&7Cost: &25 Emerald").build();

    PERSONAL_BOW_NORMAL = ItemBuilder.of(Material.BOW).name("&cBow")
        .lore("&7Cost: &610 Gold").build();
    PERSONAL_BOW_POWER = ItemBuilder.of(Material.BOW).name("&cBow (Power I)")
        .lore("&7Cost: &616 Gold").enchant(Enchantment.ARROW_DAMAGE, 1).build();
    PERSONAL_BOW_POWER_PUNCH = ItemBuilder.of(Material.BOW).name("&cBow (Power II, Punch I)")
        .lore("&7Cost: &624 Gold").enchant(Enchantment.ARROW_DAMAGE, 2).enchant(Enchantment.ARROW_KNOCKBACK, 1).build();
    PERSONAL_BOW_ARROW = ItemBuilder.of(Material.ARROW).amount(8).name("&cArrow")
        .lore("&7Cost: &63 Gold").build();

    PERSONAL_POTION_INVISIBILITY = ItemBuilder.of(Material.POTION).name("&cInvisibility II Potion (60 seconds)")
        .lore("&7Cost: &21 emerald").potion(PotionType.UNCRAFTABLE).addPotion(new PotionEffect(PotionEffectType.INVISIBILITY, 60 * 20, 1, false, false)).build();
    PERSONAL_POTION_JUMP = ItemBuilder.of(Material.POTION).name("&cJump II Potion (60 seconds)")
        .lore("&7Cost: &21 emerald").potion(PotionType.UNCRAFTABLE).addPotion(new PotionEffect(PotionEffectType.JUMP, 60 * 20, 1, false, false)).build();
    PERSONAL_POTION_HASTE = ItemBuilder.of(Material.POTION).name("&cHaste II Potion (60 seconds)")
        .lore("&7Cost: &21 emerald").potion(PotionType.UNCRAFTABLE).addPotion(new PotionEffect(PotionEffectType.FAST_DIGGING, 60 * 20, 1, false, false)).build();
    PERSONAL_POTION_RESISTANCE = ItemBuilder.of(Material.POTION).name("&cResistance II Potion (60 seconds)")
        .lore("&7Cost: &21 emerald").potion(PotionType.UNCRAFTABLE).addPotion(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 60 * 20, 1, false, false)).build();

    PERSONAL_UTILITY_APPLE = ItemBuilder.of(Material.GOLDEN_APPLE).name("&cGolden Apple")
        .lore("&7Cost: &63 Gold", "", "&7Well-rounded healing.").build();
    PERSONAL_UTILITY_FIREBALL = ItemBuilder.of(Material.FIREBALL).name("&cFireball")
        .lore("&7Cost: &65 Gold").build();
    PERSONAL_UTILITY_TNT = ItemBuilder.of(Material.TNT).lore("&7Cost: &65 Gold").build();
    PERSONAL_UTILITY_SILVERFISH = ItemBuilder.of(Material.SNOW_BALL).name("&cBedbug")
        .lore("&7Cost: &f25 Iron", "", "&7Spawns silverfish where the", "&7snowball lands to distract your", "&7enemies.").build();
    PERSONAL_UTILITY_GOLEM = ItemBuilder.of(Material.MONSTER_EGG).name("&cDream Defender")
        .lore("&7Cost: &f100 Iron").build();
    PERSONAL_UTILITY_WATER = ItemBuilder.of(Material.WATER_BUCKET).name("&cWater Bucket")
        .lore("&7Cost: &21 emerald").build();
    PERSONAL_UTILITY_PEARL = ItemBuilder.of(Material.ENDER_PEARL).name("&cEnder Pearl")
        .lore("&7Cost: &23 Emerald").build();
    PERSONAL_UTILITY_EGG = ItemBuilder.of(Material.EGG).name("&cBridge Egg")
        .lore("&7Cost: &22 Emerald").build();
    PERSONAL_UTILITY_MILK = ItemBuilder.of(Material.MILK_BUCKET).name("&cMagic Milk")
        .lore("&7Cost: &22 Emerald", "", "&7After drinking you'll be immune", "&7to all the traps on enemies", "&7islands for 60 seconds!").build();

    PERSONAL_TOOL_SHEARS = ItemBuilder.of(Material.SHEARS).name("&cShears")
        .lore("&7Cost: &f30 Iron").build();
    PERSONAL_TOOL_PICKAXE_WOOD = ItemBuilder.of(Material.WOOD_PICKAXE).name("&cWooden Pickaxe")
        .lore("&7Cost: &f20 Iron").build();
    PERSONAL_TOOL_PICKAXE_STONE = ItemBuilder.of(Material.STONE_PICKAXE).name("&cStone Pickaxe")
        .lore("&7Cost: &f40 Iron").build();
    PERSONAL_TOOL_PICKAXE_IRON = ItemBuilder.of(Material.IRON_PICKAXE).name("&cIron Pickaxe")
        .lore("&7Cost: &f64 Iron").build();
    PERSONAL_TOOL_PICKAXE_DIAMOND_I = ItemBuilder.of(Material.DIAMOND_PICKAXE).name("&cDiamond Pickaxe (Efficiency I)")
        .lore("&7Cost: &65 Gold").enchant(Enchantment.DIG_SPEED, 1).build();
    PERSONAL_TOOL_PICKAXE_DIAMOND_II = ItemBuilder.of(Material.DIAMOND_PICKAXE).name("&cDiamond Pickaxe (Efficiency II)")
        .lore("&7Cost: &615 Gold").enchant(Enchantment.DIG_SPEED, 3).build();
    PERSONAL_TOOL_PICKAXE_MAX = ItemBuilder.of(Material.DIAMOND_PICKAXE).name("&cDiamond Pickaxe (Efficiency III)")
        .lore("&aMax tier").enchant(Enchantment.DIG_SPEED, 3).build();

    PERSONAL_TOOL_AXE_WOOD = ItemBuilder.of(Material.WOOD_AXE).name("&cWooden Axe")
        .lore("&7Cost: &f20 Iron").build();
    PERSONAL_TOOL_AXE_STONE = ItemBuilder.of(Material.STONE_AXE).name("&cStone Axe")
        .lore("&7Cost: &f40 Iron").build();
    PERSONAL_TOOL_AXE_IRON = ItemBuilder.of(Material.IRON_AXE).name("&cIron Axe")
        .lore("&7Cost: &f64 Iron").build();
    PERSONAL_TOOL_AXE_DIAMOND_I = ItemBuilder.of(Material.DIAMOND_AXE).name("&cDiamond Axe (Efficiency I)")
        .lore("&7Cost: &65 Gold").enchant(Enchantment.DIG_SPEED, 1).build();
    PERSONAL_TOOL_AXE_DIAMOND_II = ItemBuilder.of(Material.DIAMOND_AXE).name("&cDiamond Axe (Efficiency II)")
        .lore("&7Cost: &715 Gold").enchant(Enchantment.DIG_SPEED, 3).build();
    PERSONAL_TOOL_AXE_MAX = ItemBuilder.of(Material.DIAMOND_AXE).name("&cDiamond Axe (Efficiency III)")
        .lore("&aMax tier").enchant(Enchantment.DIG_SPEED, 3).build();


    //todo better names
    TEAM_PROTECTION_I = ItemBuilder.of(Material.DIAMOND_CHESTPLATE)
        .lore("&9Protection I for the team", "&9Cost: &b4 diamonds").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
    TEAM_PROTECTION_II = ItemBuilder.of(Material.DIAMOND_CHESTPLATE)
        .lore("&9Protection II for the team", "&9Cost: &b8 diamonds").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2).build();
    TEAM_PROTECTION_III = ItemBuilder.of(Material.DIAMOND_CHESTPLATE)
        .lore("&9Protection III for the team", "&9Cost: &b12 diamonds").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3).build();
    TEAM_PROTECTION_IV = ItemBuilder.of(Material.DIAMOND_CHESTPLATE)
        .lore("&9Protection IV for the team", "&9Cost: &b16 diamonds").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4).build();
    TEAM_PROTECTION_MAX = ItemBuilder.of(Material.DIAMOND_CHESTPLATE)
        .lore("&aMax protection").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4).build();

    TEAM_SHARPNESS_I = ItemBuilder.of(Material.DIAMOND_SWORD).lore("&9Sharpness I for the team", "&9Cost: &b4 diamonds").enchant(Enchantment.DAMAGE_ALL, 1).build();
    TEAM_SHARPNESS_MAX = ItemBuilder.of(Material.DIAMOND_SWORD).lore("&aMax sharpness").enchant(Enchantment.DAMAGE_ALL, 2).build();

    TEAM_HASTE_I = ItemBuilder.of(Material.GOLD_PICKAXE).lore("&9Haste I for the team", "&9Cost: &b4 diamonds").enchant(Enchantment.DIG_SPEED, 1).build();
    TEAM_HASTE_II = ItemBuilder.of(Material.GOLD_PICKAXE).lore("&9Haste II for the team", "&9Cost: &b8 diamonds").enchant(Enchantment.DIG_SPEED, 2).build();
    TEAM_HASTE_MAX = ItemBuilder.of(Material.GOLD_PICKAXE).lore("&aMax haste").enchant(Enchantment.DIG_SPEED, 2).build();

    TEAM_GENERATOR_I = ItemBuilder.of(Material.GOLD_INGOT).lore("&9Generator I for the team", "&9Cost: &b6 diamonds").build();
    TEAM_GENERATOR_II = ItemBuilder.of(Material.GOLD_INGOT).lore("&9Generator II for the team", "&9Cost: &b12 diamonds").build();
    TEAM_GENERATOR_III = ItemBuilder.of(Material.GOLD_INGOT).lore("&9Generator III for the team", "&9Cost: &b18 diamonds").build();
    TEAM_GENERATOR_MAX = ItemBuilder.of(Material.GOLD_INGOT).lore("&aMax generator").build();
    HEAL_POOL = ItemBuilder.of(Material.REDSTONE_BLOCK).name("&cHeal Pool").lore("&7Daje regeneracje I na wyspie", "&9Cost: &b1 Diamond").build();

    TRAPS_MENU = ItemBuilder.of(Material.LEATHER).name("&eZakup pulapki")
        .lore("&7Zakupione pulapki pojawia", "&7sie po prawej", "", "&eKliknij by przegladac!").build();
    NO_TRAP_HERE = ItemBuilder.of(Material.STAINED_GLASS).data((short) 8).name("&cBrak pulapki!")
        .lore("&7Pierwszy wrog ktory wejdzie do twojej bazy", "&7aktywuje ta pulapke!", "", "&7Kupujac pulapke dodasz ja", "&7do tej kolejki zakupionych pulapek").build();
    BLINDNESS_TRAP_OFF = ItemBuilder.of(Material.REDSTONE_TORCH_ON)
        .lore("&9Blindness trap").build();
    BLINDNESS_TRAP_ON = ItemBuilder.of(Material.REDSTONE_TORCH_ON)
        .lore("&9Blindness trap active").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
    QUICK_RETURN_TRAP_OFF = ItemBuilder.of(Material.ENDER_PEARL)
        .lore("&9Quick return").build();
    QUICK_RETURN_TRAP_ON = ItemBuilder.of(Material.EYE_OF_ENDER)
        .lore("&9Quick return trap active").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
    INVISIBLE_FINDER_TRAP_OFF = ItemBuilder.of(Material.GLASS_BOTTLE)
        .lore("&9Invisible finder trap").build();
    INVISIBLE_FINDER_TRAP_ON = ItemBuilder.of(Material.POTION)
        .lore("&9Invisible finder trap active", "&9Jesli to czytasz to mi sie nudzi").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
    SLOW_TRAP_OFF = ItemBuilder.of(Material.DIRT)
        .lore("&9Slow trap").build();
    SLOW_TRAP_ON = ItemBuilder.of(Material.SOUL_SAND)
        .lore("&9Slow trap active").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();

    LOBBY_ZOMBIE = ItemBuilder.of(Material.MONSTER_EGG)
    		.name("&7Zmiana sklepikarza na Zombie")
            .lore("&9Koszt: &e500 monet", "&9Status: &cNie wykupiono").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
    LOBBY_ZOMBIE_BOUGHT = ItemBuilder.of(Material.MONSTER_EGG)
    		.name("&7Zmiana sklepikarza na Zombie")
    		.lore("&9Koszt: &e500 monet", "&9Status: &2Wykupiono").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
    
    LOBBY_EXTRA_ARROW = ItemBuilder.of(Material.ARROW)
    		.name("&7Efekt particles za strzalami")
    		.lore("&9Koszt: &e500 monet", "&9Status: &cNie wykupiono").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
    LOBBY_EXTRA_ARROW_BOUGHT = ItemBuilder.of(Material.ARROW)
    		.name("&7Efekt particles za strzalami")
    		.lore("&9Koszt: &e500 monet", "&9Status: &2Wykupiono").enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
    
    // PERSONAL_BLOCK GUI =========================================================================================
    final Map<Integer, GuiItem> personal_block = new HashMap<>();

    personal_block.put(19, new GuiItem(u -> PERSONAL_BLOCK_WOOL, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BLOCK, 19);
        return;
      }
      chargeItems(e, new ItemStack(Material.IRON_INGOT, 4), TEAM_WOOLS.get(u.getCurrentArena().getUserTeam(u).getTeamColor()));
    }));

    personal_block.put(20, new GuiItem(u -> PERSONAL_BLOCK_WOOD, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BLOCK, 20);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 4), PERSONAL_BLOCK_WOOD);
    }));

    personal_block.put(21, new GuiItem(u -> PERSONAL_BLOCK_ENDSTONE, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BLOCK, 21);
        return;
      }
      chargeItems(e, new ItemStack(Material.IRON_INGOT, 16), PERSONAL_BLOCK_ENDSTONE);
    }));

    personal_block.put(22, new GuiItem(u -> PERSONAL_BLOCK_CLAY, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BLOCK, 22);
        return;
      }
      chargeItems(e, new ItemStack(Material.IRON_INGOT, 8), TEAM_STAINED_CLAY.get(u.getCurrentArena().getUserTeam(u).getTeamColor()));
    }));

    personal_block.put(23, new GuiItem(u -> PERSONAL_BLOCK_GLASS, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BLOCK, 23);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 8), TEAM_STAINED_GLASS.get(u.getCurrentArena().getUserTeam(u).getTeamColor()));
    }));

    personal_block.put(24, new GuiItem(u -> PERSONAL_BLOCK_OBSIDIAN, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BLOCK, 24);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 6), PERSONAL_BLOCK_OBSIDIAN);
    }));

    applyCategorySelector(personal_block, GuiType.PERSONAL_BLOCK);

    GUI_TEMPLATES.put(GuiType.PERSONAL_BLOCK, personal_block);

    // PERSONAL_WEAPON GUI =======================================================================================
    final Map<Integer, GuiItem> personal_weapon = new HashMap<>();

    personal_weapon.put(19, new GuiItem(u -> PERSONAL_WEAPON_CHAIN_ARMOR, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_WEAPON, 19);
        return;
      }
      final Material armorType = e.getWhoClicked().getEquipment().getLeggings().getType();
      if (armorType != Material.LEATHER_LEGGINGS) {
        return;
      }

      if (chargeItems(e, new ItemStack(Material.IRON_INGOT, 35), null)) {
        final EntityEquipment playerEq = e.getWhoClicked().getEquipment();
        playerEq.setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
        playerEq.setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
        TeamUpgradeUtils.addProtection(u);
      }
    }));

    personal_weapon.put(20, new GuiItem(u -> PERSONAL_WEAPON_IRON_ARMOR, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_WEAPON, 20);
        return;
      }
      final Material armorType = e.getWhoClicked().getEquipment().getLeggings().getType();
      if (armorType == Material.IRON_LEGGINGS || armorType == Material.DIAMOND_LEGGINGS) {
        return;
      }

      if (chargeItems(e, new ItemStack(Material.GOLD_INGOT, 10), null)) {
        final EntityEquipment playerEq = e.getWhoClicked().getEquipment();
        playerEq.setBoots(new ItemStack(Material.IRON_BOOTS));
        playerEq.setLeggings(new ItemStack(Material.IRON_LEGGINGS));
        TeamUpgradeUtils.addProtection(u);
      }
    }));

    personal_weapon.put(21, new GuiItem(u -> PERSONAL_WEAPON_DIAMOND_ARMOR, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_WEAPON, 21);
        return;
      }
      final Material armorType = e.getWhoClicked().getEquipment().getLeggings().getType();
      if (armorType == Material.DIAMOND_LEGGINGS) {
        return;
      }

      if (chargeItems(e, new ItemStack(Material.EMERALD, 8), null)) {
        final EntityEquipment playerEq = e.getWhoClicked().getEquipment();
        playerEq.setBoots(new ItemStack(Material.DIAMOND_BOOTS));
        playerEq.setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
        TeamUpgradeUtils.addProtection(u);
      }
    }));

    personal_weapon.put(23, new GuiItem(u -> PERSONAL_WEAPON_STONE_SWORD, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_WEAPON, 23);
        return;
      }
      chargeItems(e, new ItemStack(Material.IRON_INGOT, 15), PERSONAL_WEAPON_STONE_SWORD);
      TeamUpgradeUtils.addSharpness(u);
    }));

    personal_weapon.put(24, new GuiItem(u -> PERSONAL_WEAPON_IRON_SWORD, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_WEAPON, 24);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 8), PERSONAL_WEAPON_IRON_SWORD);
      TeamUpgradeUtils.addSharpness(u);
    }));

    personal_weapon.put(25, new GuiItem(u -> PERSONAL_WEAPON_DIAMOND_SWORD, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_WEAPON, 25);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 5), PERSONAL_WEAPON_DIAMOND_SWORD);
      TeamUpgradeUtils.addSharpness(u);
    }));

    applyCategorySelector(personal_weapon, GuiType.PERSONAL_WEAPON);
    GUI_TEMPLATES.put(GuiType.PERSONAL_WEAPON, personal_weapon);

    // PERSONAL_BOW ===============================================================================================
    final Map<Integer, GuiItem> personal_bow = new HashMap<>();

    personal_bow.put(19, new GuiItem(u -> PERSONAL_BOW_NORMAL, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BOW, 19);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 10), PERSONAL_BOW_NORMAL);
    }));

    personal_bow.put(20, new GuiItem(u -> PERSONAL_BOW_POWER, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BOW, 20);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 16), PERSONAL_BOW_POWER);
    }));

    personal_bow.put(21, new GuiItem(u -> PERSONAL_BOW_POWER_PUNCH, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BOW, 21);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 24), PERSONAL_BOW_POWER_PUNCH);
    }));

    personal_bow.put(22, new GuiItem(u -> PERSONAL_BOW_ARROW, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_BOW, 22);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 3), PERSONAL_BOW_ARROW);
    }));

    applyCategorySelector(personal_bow, GuiType.PERSONAL_BOW);
    GUI_TEMPLATES.put(GuiType.PERSONAL_BOW, personal_bow);

    // PERSONAL_TURRET ============================================================================================
    final Map<Integer, GuiItem> personal_turret = new HashMap<>();

    personal_turret.put(19, new GuiItem(u -> ArrowTurret.getGuiItem(), (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_TURRET, 19);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 1), ArrowTurret.getItem(), false, false);
    }));

    personal_turret.put(20, new GuiItem(u -> FireballTurret.getGuiItem(), (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_TURRET, 20);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 1), FireballTurret.getItem(), false, false);
    }));

    applyCategorySelector(personal_turret, GuiType.PERSONAL_TURRET);
    GUI_TEMPLATES.put(GuiType.PERSONAL_TURRET, personal_turret);

    // PERSONAL_POTION ============================================================================================
    final Map<Integer, GuiItem> personal_potion = new HashMap<>();

    personal_potion.put(19, new GuiItem(u -> PERSONAL_POTION_INVISIBILITY, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_POTION, 19);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 1), PERSONAL_POTION_INVISIBILITY, false, true);
    }));

    personal_potion.put(20, new GuiItem(u -> PERSONAL_POTION_JUMP, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_POTION, 20);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 1), PERSONAL_POTION_JUMP, false, true);
    }));

    personal_potion.put(21, new GuiItem(u -> PERSONAL_POTION_RESISTANCE, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_POTION, 21);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 1), PERSONAL_POTION_RESISTANCE, false, true);
    }));

    personal_potion.put(22, new GuiItem(u -> PERSONAL_POTION_HASTE, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_POTION, 22);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 1), PERSONAL_POTION_HASTE, false, true);
    }));

    applyCategorySelector(personal_potion, GuiType.PERSONAL_POTION);
    GUI_TEMPLATES.put(GuiType.PERSONAL_POTION, personal_potion);

    // PERSONAL_UTILITY ===========================================================================================
    final Map<Integer, GuiItem> personal_utility = new HashMap<>();

    personal_utility.put(19, new GuiItem(u -> PERSONAL_UTILITY_APPLE, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 19);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 3), PERSONAL_UTILITY_APPLE);
    }));

    personal_utility.put(20, new GuiItem(u -> PERSONAL_UTILITY_FIREBALL, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 20);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 5), PERSONAL_UTILITY_FIREBALL, false, true);
    }));

    personal_utility.put(21, new GuiItem(u -> PERSONAL_UTILITY_TNT, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 21);
        return;
      }
      chargeItems(e, new ItemStack(Material.GOLD_INGOT, 5), PERSONAL_UTILITY_TNT);
    }));

    personal_utility.put(22, new GuiItem(u -> PERSONAL_UTILITY_SILVERFISH, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 22);
        return;
      }
      chargeItems(e, new ItemStack(Material.IRON_INGOT, 25), PERSONAL_UTILITY_SILVERFISH, false, true);
    }));

    personal_utility.put(23, new GuiItem(u -> PERSONAL_UTILITY_GOLEM, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 23);
        return;
      }
      chargeItems(e, new ItemStack(Material.IRON_INGOT, 100), PERSONAL_UTILITY_GOLEM, false, true);
    }));

    personal_utility.put(24, new GuiItem(u -> PERSONAL_UTILITY_WATER, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 24);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 1), PERSONAL_UTILITY_WATER);
    }));

    personal_utility.put(25, new GuiItem(u -> PERSONAL_UTILITY_PEARL, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 25);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 3), PERSONAL_UTILITY_PEARL);
    }));

    personal_utility.put(28, new GuiItem(u -> PERSONAL_UTILITY_EGG, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 28);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 2), PERSONAL_UTILITY_EGG);
    }));

    personal_utility.put(29, new GuiItem(u -> PERSONAL_UTILITY_MILK, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_UTILITY, 29);
        return;
      }
      chargeItems(e, new ItemStack(Material.EMERALD, 2), PERSONAL_UTILITY_MILK);
    }));

    applyCategorySelector(personal_utility, GuiType.PERSONAL_UTILITY);
    GUI_TEMPLATES.put(GuiType.PERSONAL_UTILITY, personal_utility);

    // PERSONAL_TOOL ==============================================================================================
    final Map<Integer, GuiItem> personal_tool = new HashMap<>();

    personal_tool.put(19, new GuiItem(u -> PERSONAL_TOOL_SHEARS, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_TOOL, 19);
        return;
      }
      chargeItems(e, new ItemStack(Material.IRON_INGOT, 30), PERSONAL_TOOL_SHEARS);
    }));

    personal_tool.put(20, new GuiItem(u -> {
      if (u.getPickaxeTier() == ToolTier.MAX_TIER) {
        return PERSONAL_TOOL_PICKAXE_MAX;
      } else {
        return ToolTier.getNext(u.getPickaxeTier()).getPickaxeGuiItem();
      }
    }, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_TOOL, 20);
        return;
      }
      if (u.getPickaxeTier() == ToolTier.MAX_TIER) {
        return;
      }

      final ToolTier nextTier = ToolTier.getNext(u.getPickaxeTier());
      if (chargeItems(e, nextTier.getCost(), nextTier.getPickaxeItem())) {
        u.getPlayer().getInventory().removeItem(u.getPickaxeTier().getPickaxeItem());
        u.setPickaxeTier(nextTier);
        openGui(GuiType.PERSONAL_TOOL, u);
      }
    }));

    personal_tool.put(21, new GuiItem(u -> {
      if (u.getAxeTier() == ToolTier.MAX_TIER) {
        return PERSONAL_TOOL_AXE_MAX;
      } else {
        return ToolTier.getNext(u.getAxeTier()).getAxeGuiItem();
      }
    }, (u, e) -> {
      if (e.getClick() == ClickType.SHIFT_LEFT || !u.isInGame()) {
        openQuickShopEditor(u, e.getCurrentItem(), GuiType.PERSONAL_TOOL, 21);
        return;
      }
      if (u.getAxeTier() == ToolTier.MAX_TIER) {
        return;
      }

      final ToolTier nextTier = ToolTier.getNext(u.getAxeTier());
      if (chargeItems(e, nextTier.getCost(), nextTier.getAxeItem())) {
        u.getPlayer().getInventory().removeItem(u.getAxeTier().getAxeItem());
        u.setAxeTier(nextTier);
        openGui(GuiType.PERSONAL_TOOL, u);
      }
    }));

    applyCategorySelector(personal_tool, GuiType.PERSONAL_TOOL);
    GUI_TEMPLATES.put(GuiType.PERSONAL_TOOL, personal_tool);

    // TEAM (LOBBY)
    final Map<Integer, GuiItem> teamLobby = new HashMap<>();
    
    teamLobby.put(10, new GuiItem(u -> {
        final Team userTeam = u.getCurrentArena().getUserTeam(u);

        if (userTeam.hasTeamLobbyUpgrade(TeamLobbyUpgrade.ANOTHER_VILLAGER)) {
          return LOBBY_ZOMBIE_BOUGHT;
        } else {
          return LOBBY_ZOMBIE;
        }
      }, (u, e) -> {
        buyUpgrade(e, TeamLobbyUpgrade.ANOTHER_VILLAGER, 500);
        openGui(GuiType.TEAM_LOBBY, u);
      }));
   
    teamLobby.put(11, new GuiItem(u -> {
        if (u.hasPersonalLobbyUpgrade(PersonalLobbyUpgrade.EXTRA_ARROWS)) {
          return LOBBY_EXTRA_ARROW_BOUGHT;
        } else {
          return LOBBY_EXTRA_ARROW;
        }
      }, (u, e) -> {
    	buyPersonalUpdate(e, PersonalLobbyUpgrade.EXTRA_ARROWS, 500);
        openGui(GuiType.TEAM_LOBBY, u);
      }));
    
    
    //separator
    for (int i = 18; i < 27; i++) {
      teamLobby.put(i, new GuiItem(u -> ItemBuilder.of(Material.STAINED_GLASS_PANE).data((short) 7).name(" ").build()));
    }
    GUI_TEMPLATES.put(GuiType.TEAM_LOBBY, teamLobby);
    
    // TEAM  ======================================================================================================
    final Map<Integer, GuiItem> team = new HashMap<>();

    team.put(10, new GuiItem(u -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_IV)) {
        return TEAM_PROTECTION_MAX;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_III)) {
        return TEAM_PROTECTION_IV;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_II)) {
        return TEAM_PROTECTION_III;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_I)) {
        return TEAM_PROTECTION_II;
      } else {
        return TEAM_PROTECTION_I;
      }
    }, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      ItemStack cost;
      TeamUpgrade toAdd;

      if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_IV)) {
        return;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_III)) {
        cost = new ItemStack(Material.DIAMOND, 16);
        toAdd = TeamUpgrade.PROTECTION_IV;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_II)) {
        cost = new ItemStack(Material.DIAMOND, 12);
        toAdd = TeamUpgrade.PROTECTION_III;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.PROTECTION_I)) {
        cost = new ItemStack(Material.DIAMOND, 8);
        toAdd = TeamUpgrade.PROTECTION_II;
      } else {
        cost = new ItemStack(Material.DIAMOND, 4);
        toAdd = TeamUpgrade.PROTECTION_I;
      }

      if (chargeItems(e, cost, null)) {
        userTeam.addTeamUpgrade(toAdd);
        for (final User member : userTeam.getAliveTeamMemebers()) {
          TeamUpgradeUtils.addProtection(member);
        }
      }

      openGui(GuiType.TEAM, u);
    }));

    team.put(11, new GuiItem(u -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.SHARPNESS_I)) {
        return TEAM_SHARPNESS_MAX;
      } else {
        return TEAM_SHARPNESS_I;
      }
    }, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      ItemStack cost;
      TeamUpgrade toAdd;

      if (userTeam.hasTeamUpgrade(TeamUpgrade.SHARPNESS_I)) {
        return;
      } else {
        cost = new ItemStack(Material.DIAMOND, 4);
        toAdd = TeamUpgrade.SHARPNESS_I;
      }

      if (chargeItems(e, cost, null)) {
        userTeam.addTeamUpgrade(toAdd);
        for (final User member : userTeam.getAliveTeamMemebers()) {
          TeamUpgradeUtils.addSharpness(member);
        }
      }

      openGui(GuiType.TEAM, u);
    }));

    team.put(12, new GuiItem(u -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.HASTE_II)) {
        return TEAM_HASTE_MAX;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.HASTE_I)) {
        return TEAM_HASTE_II;
      } else {
        return TEAM_HASTE_I;
      }
    }, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      ItemStack cost;
      TeamUpgrade toAdd;

      if (userTeam.hasTeamUpgrade(TeamUpgrade.HASTE_II)) {
        return;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.HASTE_I)) {
        cost = new ItemStack(Material.DIAMOND, 8);
        toAdd = TeamUpgrade.HASTE_II;
      } else {
        cost = new ItemStack(Material.DIAMOND, 4);
        toAdd = TeamUpgrade.HASTE_I;
      }

      if (chargeItems(e, cost, null)) {
        userTeam.addTeamUpgrade(toAdd);
        for (final User member : userTeam.getAliveTeamMemebers()) {
          TeamUpgradeUtils.addHaste(member);
        }
      }

      openGui(GuiType.TEAM, u);
    }));

    team.put(13, new GuiItem(u -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.GENERATOR_III)) {
        return TEAM_GENERATOR_MAX;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.GENERATOR_II)) {
        return TEAM_GENERATOR_III;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.GENERATOR_I)) {
        return TEAM_GENERATOR_II;
      } else {
        return TEAM_GENERATOR_I;
      }
    }, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      TeamUpgrade toAdd;
      ItemStack cost;

      if (userTeam.hasTeamUpgrade(TeamUpgrade.GENERATOR_III)) {
        return;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.GENERATOR_II)) {
        cost = new ItemStack(Material.DIAMOND, 18);
        toAdd = TeamUpgrade.GENERATOR_III;
      } else if (userTeam.hasTeamUpgrade(TeamUpgrade.GENERATOR_I)) {
        cost = new ItemStack(Material.DIAMOND, 12);
        toAdd = TeamUpgrade.GENERATOR_II;
      } else {
        cost = new ItemStack(Material.DIAMOND, 6);
        toAdd = TeamUpgrade.GENERATOR_I;
      }

      if (chargeItems(e, cost, null)) {
        userTeam.addTeamUpgrade(toAdd);
        userTeam.getTeamGenerator().addLevel();
      }

      openGui(GuiType.TEAM, u);
    }));

    team.put(14, new GuiItem(u -> HEAL_POOL, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.HEAL_POOL)) {
        return;
      }
      if (chargeItems(e, new ItemStack(Material.DIAMOND, 1), null)) {
        userTeam.addTeamUpgrade(TeamUpgrade.HEAL_POOL);
      }

      openGui(GuiType.TEAM, u);
    }));

    team.put(15, new GuiItem(u -> TRAPS_MENU, (u, e) -> openGui(GuiType.TEAM_TRAPS, u)));

    //separator
    for (int i = 18; i < 27; i++) {
      team.put(i, new GuiItem(u -> ItemBuilder.of(Material.STAINED_GLASS_PANE).data((short) 7).name(" ").build()));
    }
    GUI_TEMPLATES.put(GuiType.TEAM, team);

    // TEAM TRAPS  ======================================================================================================
    final Map<Integer, GuiItem> teamTraps = new HashMap<>();

    teamTraps.put(10, new GuiItem(u -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.TRAP_BLINDNESS)) {
        return BLINDNESS_TRAP_ON;
      } else {
        return ItemBuilder.of(BLINDNESS_TRAP_OFF.clone()).addLoreLine("&9Cost: &b"
            + userTeam.getPriceForNextTrapUpgrade(TeamUpgrade.TRAP_BLINDNESS) + " Diamond").build();
      }
    }, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.TRAP_BLINDNESS)) {
        return;
      } else {
        if (chargeItems(e, new ItemStack(Material.DIAMOND, userTeam.getPriceForNextTrapUpgrade(TeamUpgrade.TRAP_BLINDNESS)), null)) {
          userTeam.addTeamUpgrade(TeamUpgrade.TRAP_BLINDNESS);
          userTeam.setUpgradesPurchased(TeamUpgrade.TRAP_BLINDNESS, userTeam.getUpgradesPurchased(TeamUpgrade.TRAP_BLINDNESS) + 1);
        }
      }

      openGui(GuiType.TEAM_TRAPS, u);
    }));

    teamTraps.put(11, new GuiItem(u -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.TRAP_SLOW)) {
        return SLOW_TRAP_ON;
      } else {
        return ItemBuilder.of(SLOW_TRAP_OFF.clone()).addLoreLine("&9Cost: &b"
            + userTeam.getPriceForNextTrapUpgrade(TeamUpgrade.TRAP_SLOW) + " Diamond").build();
      }
    }, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.TRAP_SLOW)) {
        return;
      } else {
        if (chargeItems(e, new ItemStack(Material.DIAMOND, userTeam.getPriceForNextTrapUpgrade(TeamUpgrade.TRAP_SLOW)), null)) {
          userTeam.addTeamUpgrade(TeamUpgrade.TRAP_SLOW);
          userTeam.setUpgradesPurchased(TeamUpgrade.TRAP_SLOW, userTeam.getUpgradesPurchased(TeamUpgrade.TRAP_SLOW) + 1);
        }
      }

      openGui(GuiType.TEAM_TRAPS, u);
    }));

    teamTraps.put(12, new GuiItem(u -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.TRAP_INVISIBLE_FINDER)) {
        return INVISIBLE_FINDER_TRAP_ON;
      } else {
        return ItemBuilder.of(INVISIBLE_FINDER_TRAP_OFF.clone()).addLoreLine("&9Cost: &b"
            + userTeam.getPriceForNextTrapUpgrade(TeamUpgrade.TRAP_INVISIBLE_FINDER) + " Diamond").build();
      }
    }, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.TRAP_INVISIBLE_FINDER)) {
        return;
      } else {
        if (chargeItems(e, new ItemStack(Material.DIAMOND, userTeam.getPriceForNextTrapUpgrade(TeamUpgrade.TRAP_INVISIBLE_FINDER)), null)) {
          userTeam.addTeamUpgrade(TeamUpgrade.TRAP_INVISIBLE_FINDER);
          userTeam.setUpgradesPurchased(TeamUpgrade.TRAP_INVISIBLE_FINDER, userTeam.getUpgradesPurchased(TeamUpgrade.TRAP_INVISIBLE_FINDER) + 1);
        }
      }

      openGui(GuiType.TEAM_TRAPS, u);
    }));

    teamTraps.put(13, new GuiItem(u -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.TRAP_QUICK_RETURN)) {
        return QUICK_RETURN_TRAP_ON;
      } else {
        return ItemBuilder.of(QUICK_RETURN_TRAP_OFF.clone()).addLoreLine("&9Cost: &b"
            + userTeam.getPriceForNextTrapUpgrade(TeamUpgrade.TRAP_QUICK_RETURN) + " Diamond").build();
      }
    }, (u, e) -> {
      final Team userTeam = u.getCurrentArena().getUserTeam(u);

      if (userTeam.hasTeamUpgrade(TeamUpgrade.TRAP_QUICK_RETURN)) {
        return;
      } else {
        if (chargeItems(e, new ItemStack(Material.DIAMOND, userTeam.getPriceForNextTrapUpgrade(TeamUpgrade.TRAP_QUICK_RETURN)), null)) {
          userTeam.addTeamUpgrade(TeamUpgrade.TRAP_QUICK_RETURN);
          userTeam.setUpgradesPurchased(TeamUpgrade.TRAP_QUICK_RETURN, userTeam.getUpgradesPurchased(TeamUpgrade.TRAP_QUICK_RETURN) + 1);
        }
      }

      openGui(GuiType.TEAM_TRAPS, u);
    }));

    teamTraps.put(26, new GuiItem(u -> BACK_TO_PERSONAL, (u, e) -> openGui(GuiType.TEAM, u)));
    GUI_TEMPLATES.put(GuiType.TEAM_TRAPS, teamTraps);
  }

  private GuiConstants() {
  }

  private static void applyCategorySelector(Map<Integer, GuiItem> positions, GuiType type) {
    positions.put(0, new GuiItem(u -> PERSONAL_QUICK_SHOP, (u, e) -> {
      openGui(GuiType.PERSONAL_QUICK_SHOP, u);
    }));
    positions.put(1, new GuiItem(u -> PERSONAL_BLOCK, (u, e) -> {
      openGui(GuiType.PERSONAL_BLOCK, u);
    }));
    positions.put(2, new GuiItem(u -> PERSONAL_WEAPON, (u, e) -> {
      openGui(GuiType.PERSONAL_WEAPON, u);
    }));
    positions.put(3, new GuiItem(u -> PERSONAL_TOOL, (u, e) -> {
      openGui(GuiType.PERSONAL_TOOL, u);
    }));
    positions.put(4, new GuiItem(u -> PERSONAL_BOW, (u, e) -> {
      openGui(GuiType.PERSONAL_BOW, u);
    }));
    positions.put(5, new GuiItem(u -> PERSONAL_POTION, (u, e) -> {
      openGui(GuiType.PERSONAL_POTION, u);
    }));
    positions.put(6, new GuiItem(u -> PERSONAL_UTILITY, (u, e) -> {
      openGui(GuiType.PERSONAL_UTILITY, u);
    }));
    positions.put(7, new GuiItem(u -> PERSONAL_TURRET, (u, e) -> {
      openGui(GuiType.PERSONAL_TURRET, u);
    }));

    switch (type) {
      case PERSONAL_QUICK_SHOP:
        positions.put(9, new GuiItem(u -> CURRENT_CATEGORY_SELECTOR));
        break;
      case PERSONAL_BLOCK:
        positions.put(10, new GuiItem(u -> CURRENT_CATEGORY_SELECTOR));
        break;
      case PERSONAL_WEAPON:
        positions.put(11, new GuiItem(u -> CURRENT_CATEGORY_SELECTOR));
        break;
      case PERSONAL_TOOL:
        positions.put(12, new GuiItem(u -> CURRENT_CATEGORY_SELECTOR));
        break;
      case PERSONAL_BOW:
        positions.put(13, new GuiItem(u -> CURRENT_CATEGORY_SELECTOR));
        break;
      case PERSONAL_POTION:
        positions.put(14, new GuiItem(u -> CURRENT_CATEGORY_SELECTOR));
        break;
      case PERSONAL_UTILITY:
        positions.put(15, new GuiItem(u -> CURRENT_CATEGORY_SELECTOR));
        break;
      case PERSONAL_TURRET:
        positions.put(16, new GuiItem(u -> CURRENT_CATEGORY_SELECTOR));
        break;
    }
    for (int i = 9; i < 18; i++) {
      if (positions.containsKey(i)) {
        continue;
      }
      positions.put(i, new GuiItem(u -> ItemBuilder.of(Material.STAINED_GLASS_PANE).data((short) 7).name(" ").lore("&8⬆ &7Categories", "&8⬇ &7Items").build()));
    }
  }

  private static ItemStack formatAsQuickShopSelectorItem(ItemStack item) {
    return ItemBuilder.of(item.getType()).data(item.getData().getData()).amount(item.getAmount()).name(item.getItemMeta().getDisplayName()).lore("&eClick to replace!").build();
  }

  public static void openQuickShopEditor(User user, ItemStack itemStack, GuiType type, int accessor) {
    Inventory inventory = Bukkit.createInventory(null, GuiType.QUICK_SHOP_PERSONALIZER.getGuiSize(), GuiType.QUICK_SHOP_PERSONALIZER.getGuiName());
    inventory.setItem(4, new GuiItem(u -> itemStack).getItem(user));

    for (Map.Entry<Integer, QuickShopItem> entry : user.getQuickShopData().getQuickShopItems().entrySet()) {
      ItemStack stack = GUI_TEMPLATES.get(entry.getValue().getMainType()).get(entry.getValue().getAccessor()).getItem(user);
      inventory.setItem(entry.getKey(), new GuiItem(u -> formatAsQuickShopSelectorItem(stack)).getItem(user));
    }
    for (int i = 19; i < 44; i++) {
      if (i == 26 || i == 35) {
        i += 2;
      }
      if (inventory.getItem(i) != null) {
        continue;
      }
      inventory.setItem(i, new GuiItem(u -> QUICK_SHOP_PERSONALIZER_EMPTY_SLOT).getItem(user));
    }
    Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> {
      user.getPlayer().openInventory(inventory);
      user.getQuickShopData().setEventQuickShopItem(new QuickShopItem(type, accessor));
    });
  }

  public static void openGui(final GuiType guiType, final User user) {
    final Inventory inventory = Bukkit.createInventory(null, guiType.getGuiSize(), guiType.getGuiName());

    //todo gui type handling event
    if (guiType == GuiType.PERSONAL_QUICK_SHOP) {
      Map<Integer, GuiItem> positions = new HashMap<>();
      for (Map.Entry<Integer, QuickShopItem> entry : user.getQuickShopData().getQuickShopItems().entrySet()) {
        positions.put(entry.getKey(), GUI_TEMPLATES.get(entry.getValue().getMainType()).get(entry.getValue().getAccessor()));
      }
      for (int i = 19; i < 44; i++) {
        if (i == 26 || i == 35) {
          i += 2;
        }
        if (positions.containsKey(i)) {
          continue;
        }
        //todo different item?
        positions.put(i, new GuiItem(u -> QUICK_SHOP_PERSONALIZER_EMPTY_SLOT));
      }
      applyCategorySelector(positions, GuiType.PERSONAL_QUICK_SHOP);
      for (Map.Entry<Integer, GuiItem> entry : positions.entrySet()) {
        inventory.setItem(entry.getKey(), entry.getValue().getItem(user));
      }
      Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> user.getPlayer().openInventory(inventory));
      return;
    } else if(guiType == GuiType.TEAM_LOBBY) {

        final Map<Integer, GuiItem> guiItems = GUI_TEMPLATES.get(guiType);
        if (guiItems == null || guiItems.isEmpty()) {
          return;
        }

        for (final Entry<Integer, GuiItem> itemEntry : guiItems.entrySet()) {
          inventory.setItem(itemEntry.getKey(), itemEntry.getValue().getItem(user));
        }

    	Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> user.getPlayer().openInventory(inventory));
    	return;
    }

    final Map<Integer, GuiItem> guiItems = GUI_TEMPLATES.get(guiType);
    if (guiItems == null || guiItems.isEmpty()) {
      return;
    }

    for (final Entry<Integer, GuiItem> itemEntry : guiItems.entrySet()) {
      inventory.setItem(itemEntry.getKey(), itemEntry.getValue().getItem(user));
    }

    if (guiType == GuiType.TEAM) {
      prepareListOfTrapsButtons(inventory, user);
    }

    Bukkit.getScheduler().runTask(BedWars.getPlugin(), () -> user.getPlayer().openInventory(inventory));
  }

  private static boolean chargeItems(final InventoryClickEvent event, final ItemStack cost, final ItemStack toGive) {
    return chargeItems(event, cost, toGive, true, true);
  }

  private static boolean buyPersonalUpdate(final InventoryClickEvent event, PersonalLobbyUpgrade upgrade, long cost) {
	  final Player player = (Player) event.getWhoClicked();
	  final UUID uuid = player.getUniqueId();
	  final User user = UserData.getUser(uuid);
	  final Team team = user.getCurrentArena().getUserTeam(user);

	  //fail
	  if(user.getMoney() < cost || user.hasPersonalLobbyUpgrade(upgrade)) {
		  player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_HURT, 0.4F, 1.0F);
		  return false;
	  }
	  
	  //success
	  user.removeMoney(cost);
	  user.addPersonalLobbyUpgrade(upgrade);

	  player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 0.4F, 1.0F);
	  return true;	  
  }
  
  private static boolean buyUpgrade(final InventoryClickEvent event, TeamLobbyUpgrade upgrade, long cost) {
	  final Player player = (Player) event.getWhoClicked();
	  final UUID uuid = player.getUniqueId();
	  final User user = UserData.getUser(uuid);
	  final Team team = user.getCurrentArena().getUserTeam(user);

	  //fail
	  if(user.getMoney() < cost || team.hasTeamLobbyUpgrade(upgrade)) {
		  player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_HURT, 0.4F, 1.0F);
		  return false;
	  }
	  
	  //success
	  user.removeMoney(cost);
	  team.addTeamLobbyUpgrade(upgrade);

	  player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 0.4F, 1.0F);
	  return true;
  }
  
  private static boolean chargeItems(final InventoryClickEvent event, final ItemStack cost, final ItemStack toGive, final boolean stripName, final boolean stripLore) {
    final Player player = (Player) event.getWhoClicked();
    final Inventory playerInventory = player.getInventory();

    if (playerInventory.containsAtLeast(cost, cost.getAmount())) {
      playerInventory.removeItem(cost);

      if (toGive != null) {
        final ItemBuilder itemBuilder = ItemBuilder.of(toGive);

        if (stripName) {
          itemBuilder.clearName();
        }

        if (stripLore) {
          itemBuilder.clearLore();
        }

        playerInventory.addItem(itemBuilder.build());
      }

      player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 0.4F, 1.0F);
      return true;
    } else {
      player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_HURT, 0.4F, 1.0F);
      return false;
    }
  }

  private static void prepareListOfTrapsButtons(Inventory inventory, User user) {
    int limit = 0;
    for (TeamUpgrade upgrade : user.getCurrentArena().getUserTeam(user).getTeamUpgrades()) {
      if (!upgrade.isTrap()) {
        continue;
      }
      inventory.setItem(30 + limit, new GuiItem(u -> getItemByTrap(upgrade), (u, e) -> e.setCancelled(true)).getItem(user));
      if (limit == 2) {
        return;
      }
      limit++;
    }
    for (int i = 0; i < 3; i++) {
      if (inventory.getItem(30 + i) != null) {
        continue;
      }
      inventory.setItem(30 + i, NO_TRAP_HERE);
    }
  }

  /**
   * Get item stack represented by it's TeamUpgrade trap
   *
   * @param upgrade trap to return upgrade for, only traps!
   * @return itemstack represented by this trap upgrade
   */
  private static ItemStack getItemByTrap(TeamUpgrade upgrade) {
    switch (upgrade) {
      case TRAP_BLINDNESS:
        return BLINDNESS_TRAP_ON;
      case TRAP_QUICK_RETURN:
        return QUICK_RETURN_TRAP_ON;
      case TRAP_INVISIBLE_FINDER:
        return INVISIBLE_FINDER_TRAP_ON;
      case TRAP_SLOW:
        return SLOW_TRAP_ON;
      default:
        return new ItemStack(Material.AIR);
    }
  }

}
