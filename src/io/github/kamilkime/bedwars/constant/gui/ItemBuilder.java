package io.github.kamilkime.bedwars.constant.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import io.github.kamilkime.bedwars.data.DataUtils;

public final class ItemBuilder {

  private ItemStack item;
  private ItemMeta itemMeta;
  private boolean hideFlags;

  private ItemBuilder(final ItemStack baseItem) {
    this.item = baseItem;
    this.itemMeta = this.item.getItemMeta();
    this.hideFlags = true;
  }

  public static ItemBuilder of(final Material baseType) {
    return new ItemBuilder(new ItemStack(baseType));
  }

  public static ItemBuilder of(final ItemStack baseItem) {
    return new ItemBuilder(baseItem.clone());
  }

  public ItemBuilder amount(final int amount) {
    this.item.setAmount(amount);
    return this;
  }

  public ItemBuilder data(final short data) {
    this.item.setDurability(data);
    return this;
  }

  public ItemBuilder name(final String name) {
    if (this.itemMeta != null) {
      this.itemMeta.setDisplayName(DataUtils.color(name));
    }

    return this;
  }

  public ItemBuilder clearName() {
    if (this.itemMeta != null) {
      this.itemMeta.setDisplayName(null);
    }

    return this;
  }

  public ItemBuilder lore(final String... lore) {
    if (this.itemMeta != null) {
      this.itemMeta.setLore(DataUtils.color(Arrays.asList(lore)));
    }

    return this;
  }

  public ItemBuilder addLoreLine(final String line) {
    if (this.itemMeta != null) {
      final List<String> lore = this.itemMeta.hasLore() ? this.itemMeta.getLore() : new ArrayList<>();
      lore.add(DataUtils.color(line));
      this.itemMeta.setLore(lore);
    }

    return this;
  }

  public ItemBuilder addLoreLines(final String... lines) {
    for (final String line : lines) {
      this.addLoreLine(line);
    }

    return this;
  }

  public ItemBuilder clearLore() {
    if (this.itemMeta != null) {
      this.itemMeta.setLore(null);
    }

    return this;
  }

  public ItemBuilder enchant(final Enchantment enchantment, final int level) {
    if (this.itemMeta != null) {
      this.itemMeta.addEnchant(enchantment, level, true);
    }

    return this;
  }

  public ItemBuilder color(final Color color) {
    if (this.itemMeta instanceof LeatherArmorMeta) {
      ((LeatherArmorMeta) this.itemMeta).setColor(color);
    }

    return this;
  }

  public ItemBuilder potion(final PotionType potionType, final boolean extended, final boolean upgraded) {
    if (this.itemMeta instanceof PotionMeta) {
      ((PotionMeta) this.itemMeta).setBasePotionData(new PotionData(potionType, extended, upgraded));
    }

    return this;
  }

  public ItemBuilder potion(final PotionType potionType) {
    return this.potion(potionType, false, false);
  }

  public ItemBuilder addPotion(final PotionEffect potionEffect) {
    if (this.itemMeta instanceof PotionMeta) {
      ((PotionMeta) this.itemMeta).addCustomEffect(potionEffect, true);
    }

    return this;
  }

  public ItemBuilder dontHideFlags() {
    this.hideFlags = false;
    return this;
  }

  public ItemStack build() {
    if (this.itemMeta != null && this.hideFlags) {
      this.itemMeta.addItemFlags(ItemFlag.HIDE_PLACED_ON, ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DESTROYS, ItemFlag.HIDE_UNBREAKABLE,
          ItemFlag.HIDE_ENCHANTS);
    }

    this.item.setItemMeta(this.itemMeta);
    return this.item;
  }

}
