package io.github.kamilkime.bedwars.constant;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.constant.gui.ItemBuilder;
import io.github.kamilkime.bedwars.constant.team.TeamColor;

public final class BuildModeConstants {

  public static final Map<TeamColor, ItemStack> TEAM_DYES = new HashMap<>();
  public static final Map<ItemStack, TeamColor> TEAM_DYES_INVERTED = new HashMap<>();

  public static final ItemStack BUILDMODE_TEAM_SPAWN;
  public static final ItemStack BUILDMODE_TEAM_DEATHMATCH_SPAWN;
  public static final ItemStack BUILDMODE_TEAM_BED;
  public static final ItemStack BUILDMODE_TEAM_CHEST;
  public static final ItemStack BUILDMODE_DESTROY;

  public static final ItemStack BUILDMODE_LOBBY;
  public static final ItemStack BUILDMODE_LOBBY_MAIN;
  public static final ItemStack BUILDMODE_LOBBY_SPECTATOR;
  public static final ItemStack BUILDMODE_LOBBY_CENTER;
  public static final ItemStack BUILDMODE_LOBBY_SHOP;
  
  public static final ItemStack BUILDMODE_GENERATOR;
  public static final ItemStack BUILDMODE_GENERATOR_TEAM;
  public static final ItemStack BUILDMODE_GENERATOR_DIAMOND;
  public static final ItemStack BUILDMODE_GENERATOR_EMERALD;

  public static final ItemStack BUILDMODE_SHOP;
  public static final ItemStack BUILDMODE_SHOP_PERSONAL;
  public static final ItemStack BUILDMODE_SHOP_TEAM;

  public static final ItemStack BUILDMODE_WORLDEDIT_TELEPORTER;

  static {
    for (final TeamColor teamColor : TeamColor.values()) {
      final ItemStack teamDye = ItemBuilder.of(Material.INK_SACK).data(teamColor.getDyeColor().getDyeData()).build();

      TEAM_DYES.put(teamColor, teamDye);
      TEAM_DYES_INVERTED.put(teamDye, teamColor);
    }

    BUILDMODE_TEAM_SPAWN = ItemBuilder.of(Material.WOOL).name("&a&lSet team spawn").build();
    BUILDMODE_TEAM_DEATHMATCH_SPAWN = ItemBuilder.of(Material.STAINED_CLAY).name("&7&lSet team deathmatch spawn").build();
    BUILDMODE_TEAM_BED = ItemBuilder.of(Material.BED).name("&9&lSet team bed").build();
    BUILDMODE_TEAM_CHEST = ItemBuilder.of(Material.CHEST).name("&6&lSet team chest").build();
    BUILDMODE_DESTROY = ItemBuilder.of(Material.BARRIER).name("&c&lDestroy a location").build();

    BUILDMODE_LOBBY = ItemBuilder.of(Material.GLASS).name("&f&lSet arena lobby").build();
    BUILDMODE_LOBBY_MAIN = ItemBuilder.of(Material.GLASS).name("&f&lSet arena main lobby").build();
    BUILDMODE_LOBBY_SPECTATOR = ItemBuilder.of(Material.PUMPKIN).name("&6&lSet arena spectator lobby").build();
    BUILDMODE_LOBBY_CENTER = ItemBuilder.of(Material.TNT).name("&c&lSet arena center").build();
    BUILDMODE_LOBBY_SHOP = ItemBuilder.of(Material.GOLD_BLOCK).name("&c&lSet global lobby shop").build();
    
    BUILDMODE_GENERATOR = ItemBuilder.of(Material.DIAMOND_BLOCK).name("&b&lCreate a generator").build();
    BUILDMODE_GENERATOR_TEAM = ItemBuilder.of(Material.IRON_BLOCK).name("&7&lCreate a team generator").build();
    BUILDMODE_GENERATOR_DIAMOND = ItemBuilder.of(Material.DIAMOND_BLOCK).name("&b&lCreate a diamond generator").build();
    BUILDMODE_GENERATOR_EMERALD = ItemBuilder.of(Material.EMERALD_BLOCK).name("&a&lCreate an emerald generator").build();

    BUILDMODE_SHOP = ItemBuilder.of(Material.MOB_SPAWNER).name("&5&lCreate a shop").build();
    BUILDMODE_SHOP_PERSONAL = ItemBuilder.of(Material.IRON_INGOT).name("&6&lCreate a personal shop").build();
    BUILDMODE_SHOP_TEAM = ItemBuilder.of(Material.DIAMOND).name("&a&lCreate a team shop").build();

    BUILDMODE_WORLDEDIT_TELEPORTER = ItemBuilder.of(Material.COMPASS).name("&8&lWorldEdit teleporter").build();
  }

  private BuildModeConstants() {
  }

}
