package io.github.kamilkime.bedwars.constant.generator;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.generator.DroppedItem;

public enum GeneratorType {

  TEAM(4, "&cTeam generator {LEVEL}", new ItemStack(Material.IRON_BLOCK),
      new DroppedItem(Material.IRON_INGOT, new long[] {3L, 2L, 1L, 1L}, "&7Next iron ingot in {TIME}s", 48),
      new DroppedItem(Material.GOLD_INGOT, new long[] {10L, 7L, 4L, 2L}, "&6Next gold ingot in {TIME}s", 16),
      new DroppedItem(Material.EMERALD, new long[] {-1L, -1L, 7L, 4L}, "&aNext emerald in {TIME}s", 8)),

  DIAMOND(4, "&bDiamond generator {LEVEL}", new ItemStack(Material.DIAMOND_BLOCK),
      new DroppedItem(Material.DIAMOND, new long[] {30L, 20L, 10L, 5L}, "&bNext diamond in {TIME}s", 8)),

  EMERALD(4, "&aEmerald generator {LEVEL}", new ItemStack(Material.EMERALD_BLOCK),
      new DroppedItem(Material.EMERALD, new long[] {45L, 35L, 20L, 10L}, "&aNext emerald in {TIME}s", 8));

  private final int maxLevel;
  private final String generatorName;
  private final ItemStack icon;
  private final DroppedItem[] drops;

  GeneratorType(final int maxLevel, final String generatorName, final ItemStack icon, final DroppedItem... drops) {
    this.maxLevel = maxLevel;
    this.generatorName = DataUtils.color(generatorName);
    this.icon = icon;
    this.drops = drops;
  }

  public int getMaxLevel() {
    return this.maxLevel;
  }

  public String getGeneratorName() {
    return this.generatorName;
  }

  public ItemStack getIcon() {
    return this.icon.clone();
  }

  public DroppedItem[] getDrops() {
    return this.drops;
  }

}
