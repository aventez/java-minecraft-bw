package io.github.kamilkime.bedwars.constant.task;

import java.util.Locale;

import org.bukkit.entity.Creature;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;

public final class ArenaEntityTask {

  private final Creature creature;
  private final Arena arena;

  private final char teamColorCode;
  private double timeAliveLeft;

  public ArenaEntityTask(final Creature creature, final Arena arena, final Team team, final User user, final double lifetime) {
    this.creature = creature;
    this.creature.setCustomNameVisible(true);

    this.arena = arena;
    this.teamColorCode = team.getTeamColor().getColorCode();
    this.timeAliveLeft = lifetime;

    this.arena.getArenaRegion().addArenaEntity(this.creature, user);
    targetNearestEnemy(creature, arena, team);
  }

  public static Player targetNearestEnemy(final Creature creature, final Arena arena, final Team friendlyTeam) {
    double lowestDistanceToTarget = Double.MAX_VALUE;
    Player toTarget = null;

    for (final User target : arena.getArenaPlayers()) {
      final Team targetTeam = arena.getUserTeam(target);
      if (friendlyTeam.equals(targetTeam) || !targetTeam.isAlive(target)) {
        continue;
      }

      final Player targetPlayer = target.getPlayer();
      final double distanceToTargetPlayer = targetPlayer.getLocation().distanceSquared(creature.getLocation());

      if (lowestDistanceToTarget > distanceToTargetPlayer) {
        lowestDistanceToTarget = distanceToTargetPlayer;
        toTarget = targetPlayer;
      }
    }

    creature.setTarget(toTarget);
    return toTarget;
  }

  public void start() {
    new BukkitRunnable() {

      @Override
      public void run() {
        if (creature.isDead() || timeAliveLeft <= 0.0D) {
          ArenaEntityTask.this.creature.remove();
          ArenaEntityTask.this.arena.getArenaRegion().removeArenaEntity(ArenaEntityTask.this.creature);

          this.cancel();
          return;
        }

        ArenaEntityTask.this.creature.setCustomName(DataUtils.color(String.format(Locale.US, "&" + teamColorCode +
            "%.1f &7HP (&" + teamColorCode + "%.1fs&7)", ArenaEntityTask.this.creature.getHealth(), ArenaEntityTask.this.timeAliveLeft)));

        timeAliveLeft -= 0.05D;
      }
    }.runTaskTimer(BedWars.getPlugin(), 0L, 1L);
  }

}
