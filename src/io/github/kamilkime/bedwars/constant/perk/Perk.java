package io.github.kamilkime.bedwars.constant.perk;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Perk {

  BOMBER("Bombowiec", 10, 0.05D, 0.1D, 0.15D, 0.20D, 0.25D, 0.3D, 0.4D, 0.5D, 0.6D, 0.75D);

  private static final Map<String, Perk> FROM_NAME = new HashMap<>();

  static {
    FROM_NAME.put(Perk.BOMBER.toString(), Perk.BOMBER);
    FROM_NAME.put(Perk.BOMBER.getPerkName().toUpperCase(), Perk.BOMBER);
  }

  private final String perkName;
  private final int maxLvl;
  private final double[] perkBonus;

  Perk(final String perkName, final int maxLvl, final double... perkBonus) {
    this.perkName = perkName;
    this.maxLvl = maxLvl;
    this.perkBonus = perkBonus;
  }

  public static Perk fromName(final String name) {
    return FROM_NAME.get(name.toUpperCase());
  }

  public String getPerkName() {
    return this.perkName;
  }

  public int getMaxLevel() {
    return this.maxLvl;
  }

  public double[] getPerkBonuses() {
    return Arrays.copyOf(this.perkBonus, this.perkBonus.length);
  }

  public double getPerkBonus(final int level) {
    if (level < 1 || level > this.perkBonus.length) {
      return 0.0D;
    }

    return this.perkBonus[level - 1];
  }

}
