package io.github.kamilkime.bedwars.constant;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import io.github.kamilkime.bedwars.constant.arena.GamePhase;
import io.github.kamilkime.bedwars.data.DataUtils;

public final class XpConstants {

  public static final long XP_FOR_KILL = 100L;
  public static final long XP_FOR_FINAL_KILL = 250L;
  public static final long XP_FOR_BED_DESTROY = 500L;
  public static final long XP_FOR_ARENA_WIN = 1000L;

  private static final Map<GamePhase, Long> XP_FOR_GAME_PHASE = new HashMap<>();
  private static final Map<Long, Long> TOTAL_XP_FOR_LVL = new HashMap<>();
  private static final Map<Integer, String> CHAT_PREFIXES = new LinkedHashMap<>(12);

  static {
    // XP given after each phase, to alive players
    XP_FOR_GAME_PHASE.put(GamePhase.DIAMOND_II, 50L);
    XP_FOR_GAME_PHASE.put(GamePhase.DIAMOND_III, 50L);
    XP_FOR_GAME_PHASE.put(GamePhase.DIAMOND_IV, 50L);
    XP_FOR_GAME_PHASE.put(GamePhase.EMERALD_II, 50L);
    XP_FOR_GAME_PHASE.put(GamePhase.EMERALD_III, 50L);
    XP_FOR_GAME_PHASE.put(GamePhase.EMERALD_IV, 50L);
    XP_FOR_GAME_PHASE.put(GamePhase.BEDS_DESTROYED, 150L);
    XP_FOR_GAME_PHASE.put(GamePhase.DEATHMATCH, 200L);
    XP_FOR_GAME_PHASE.put(GamePhase.FINISH, 100L);

    // MUST be given from lowest to biggest
    // CHAT_PREFIXES.put(fromLvl, "Prefix");
    CHAT_PREFIXES.put(1000, DataUtils.color("&6[{LVL}] "));
    CHAT_PREFIXES.put(900, DataUtils.color("&a[{LVL}] "));
    CHAT_PREFIXES.put(800, DataUtils.color("&d[{LVL}] "));
    CHAT_PREFIXES.put(700, DataUtils.color("&c[{LVL}] "));
    CHAT_PREFIXES.put(600, DataUtils.color("&e[{LVL}] "));
    CHAT_PREFIXES.put(500, DataUtils.color("&2[{LVL}] "));
    CHAT_PREFIXES.put(400, DataUtils.color("&b[{LVL}] "));
    CHAT_PREFIXES.put(300, DataUtils.color("&5[{LVL}] "));
    CHAT_PREFIXES.put(200, DataUtils.color("&9[{LVL}] "));
    CHAT_PREFIXES.put(100, DataUtils.color("&f[{LVL}] "));
    CHAT_PREFIXES.put(50, DataUtils.color("&7[{LVL}] "));
    CHAT_PREFIXES.put(0, DataUtils.color("&4[{LVL}] "));
  }

  private XpConstants() {
  }

  public static long getXpForGamePhase(final GamePhase phase) {
    return XP_FOR_GAME_PHASE.getOrDefault(phase, 0L);
  }

  public static long getXpForLvl(final long lvl) {
    return ((lvl / 50L) * 50000L) + ((lvl % 50L) * 1000L);
  }

  public static long getTotalXpForLvl(final long lvl) {
    if (lvl <= 0L) {
      return 0L;
    }

    final Long xp = TOTAL_XP_FOR_LVL.get(lvl);
    if (xp != null) {
      return xp;
    }

    final long totalXp = getXpForLvl(lvl) + getTotalXpForLvl(lvl - 1L);
    TOTAL_XP_FOR_LVL.put(lvl, totalXp);

    return totalXp;
  }

  public static String getChatPrefix(final long lvl) {
    for (final Entry<Integer, String> prefix : CHAT_PREFIXES.entrySet()) {
      if (lvl >= prefix.getKey()) {
        return prefix.getValue();
      }
    }

    return "";
  }

}
