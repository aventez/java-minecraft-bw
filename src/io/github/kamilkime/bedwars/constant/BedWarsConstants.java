package io.github.kamilkime.bedwars.constant;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.constant.gui.ItemBuilder;
import io.github.kamilkime.bedwars.constant.team.TeamColor;

public final class BedWarsConstants {

  public static final Map<TeamColor, Map<Integer, ItemStack>> START_ITEMS = new HashMap<>();
  public static final Map<EntityType, Integer> ENTITY_LIMITS = new HashMap<>();

  public static final Set<String[]> ALLOWED_COMMANDS = new HashSet<>();

  // IN TICKS
  public static final long RESPAWN_DELAY = 100L;

  public static final long MONEY_FOR_KILL = 5L;
  public static final long MONEY_FOR_FINAL_KILL = 20L;
  public static final long MONEY_FOR_BED_DESTROY = 50L;
  public static final long MONEY_FOR_ARENA_WIN = 100L;

  public static final double DEATHMATCH_BORDER_RADIUS = 30.0D;
  public static final double DEATHMATCH_BORDER_DECREASE = 0.5D;
  public static final double DEATHMATCH_BORDER_DAMAGE = 0.5D;
  public static final double DEATHMATCH_BORDER_HEIGHT = 13.0D;

  public static final double ARENA_NO_BUILD_TEAM_SPAWN_RADIUS_SQ = 6.0D * 6.0D;
  public static final double ARENA_NO_BUILD_TEAM_DEATHMATCH_SPAWN_RADIUS_SQ = 4.0D * 4.0D;
  public static final double ARENA_NO_BUILD_GENERATOR_RADIUS_SQ = 4.0D * 4.0D;
  public static final double ARENA_TURRET_PLACE_RADIUS_SQ = 12.0D * 12.0D;

  public static final double TEAM_TRAP_RADIUS_SQ = 15.0D * 15.0D;

  public static final int MAX_BUILD_HEIGHT = 40;

  static {
    ENTITY_LIMITS.put(EntityType.SILVERFISH, 5);
    ENTITY_LIMITS.put(EntityType.IRON_GOLEM, 2);

    for (final TeamColor teamColor : TeamColor.values()) {
      final Map<Integer, ItemStack> teamStartItems = new HashMap<>();

      teamStartItems.put(0, ItemBuilder.of(Material.WOOD_SWORD).build());
      teamStartItems.put(39, ItemBuilder.of(Material.LEATHER_HELMET).color(teamColor.getDyeColor().getColor()).build());
      teamStartItems.put(38, ItemBuilder.of(Material.LEATHER_CHESTPLATE).color(teamColor.getDyeColor().getColor()).build());
      teamStartItems.put(37, ItemBuilder.of(Material.LEATHER_LEGGINGS).color(teamColor.getDyeColor().getColor()).build());
      teamStartItems.put(36, ItemBuilder.of(Material.LEATHER_BOOTS).color(teamColor.getDyeColor().getColor()).build());

      START_ITEMS.put(teamColor, teamStartItems);
    }

    ALLOWED_COMMANDS.add("/arena".split(" "));
    ALLOWED_COMMANDS.add("/user".split(" "));
    ALLOWED_COMMANDS.add("/helpop".split(" "));
    ALLOWED_COMMANDS.add("/shout".split(" "));
  }

  private BedWarsConstants() {
  }

  public static double deathmatchBorderAngle(final double currentBorderRadius) {
    final double degAngle = 14.8D - 0.845D * currentBorderRadius + 0.0125D * currentBorderRadius * currentBorderRadius;
    return Math.toRadians(degAngle);
  }

}
