package io.github.kamilkime.bedwars.generator;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.data.DataUtils;

public class DroppedItem {

  private final ItemStack item;
  private final long[] generationDelays;
  private final String hologramTextLine;
  private final Integer generationLimit;

  public DroppedItem(final ItemStack item, final long[] generationDelays, final String hologramTextLine, final Integer generationLimit) {
    this.item = item;
    this.generationDelays = generationDelays;
    this.hologramTextLine = DataUtils.color(hologramTextLine);
    this.generationLimit = generationLimit;
  }

  public DroppedItem(final Material itemType, final long[] generationDelays, final String hologramTextLine, final Integer generationLimit) {
    this(new ItemStack(itemType), generationDelays, hologramTextLine, generationLimit);
  }

  public ItemStack getItem() {
    return this.item/*.clone()*/;
  }

  public long[] getGenerationDelays() {
    return this.generationDelays;
  }

  public String getHologramTextLine() {
    return this.hologramTextLine;
  }

  public Integer getGenerationLimit() {
    return this.generationLimit;
  }

}
