package io.github.kamilkime.bedwars.generator;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.constant.generator.GeneratorType;
import io.github.kamilkime.bedwars.data.DataUtils;
import io.github.kamilkime.bedwars.data.Replacer;

public final class ItemGenerator {

  private final GeneratorType generatorType;

  private final Location generationLocation;
  private final World generationWorld;

  private int generatorLevel;
  private int generationTime;
  private int lastHologramIndex;

  private BukkitTask generatorTask;

  private Hologram generatorHologram;

  public ItemGenerator(final GeneratorType generatorType, final Location generationLocation) {
    this.generatorType = generatorType;
    this.generationLocation = generationLocation;
    this.generationWorld = this.generationLocation.getWorld();
    this.generatorLevel = 1;
  }

  public GeneratorType getType() {
    return this.generatorType;
  }

  public Location getLocation() {
    return this.generationLocation == null ? null : this.generationLocation.clone();
  }

  public boolean addLevel() {
    if (this.generatorLevel == this.generatorType.getMaxLevel()) {
      return false;
    }

    this.generatorLevel++;

    this.stopGeneration();
    this.startGeneration();

    return true;
  }

  public void startGeneration() {
    this.generatorHologram = HologramsAPI.createHologram(BedWars.getPlugin(), this.generationLocation.clone().add(0.0D, 3.0D, 0.0D));
    this.generatorHologram.appendTextLine(Replacer.of(this.generatorType.getGeneratorName()).with("{LEVEL}", DataUtils.toRoman(this.generatorLevel)).toString());
    this.generatorHologram.appendItemLine(this.generatorType.getIcon());
    this.lastHologramIndex = 0;

    this.generatorTask = Bukkit.getScheduler().runTaskTimer(BedWars.getPlugin(), () -> {
      int index = 1;

      for (final DroppedItem drop : this.generatorType.getDrops()) {
        final long period = drop.getGenerationDelays()[this.generatorLevel - 1];
        final long remaining = this.generationTime % period;

        if (period != -1) {
          final String line = Replacer.of(drop.getHologramTextLine()).with("{TIME}", period - remaining).toString();

          if (this.lastHologramIndex > index) {
            ((TextLine) this.generatorHologram.getLine(index++)).setText(line);
          } else {
            this.generatorHologram.insertTextLine(index++, line);
          }

          if (remaining == 0) {
            int droppedItems = 0;
            for (final Entity entity : this.generationWorld.getNearbyEntities(this.generationLocation, 2.0D, 2.0D, 2.0D)) {
              if (!(entity instanceof Item)) {
                continue;
              }

              final ItemStack item = ((Item) entity).getItemStack();
              if (item.isSimilar(drop.getItem())) {
                droppedItems += item.getAmount();
              }
            }

            if (droppedItems < drop.getGenerationLimit()) {
              this.generationWorld.dropItem(this.generationLocation, drop.getItem());
            }
          }
        }
      }

      this.lastHologramIndex = index;
      this.generationTime++;
    }, 0L, 20L);
  }

  public void stopGeneration() {
    if (this.generatorTask == null) {
      return;
    }

    this.generatorTask.cancel();
    this.generationTime = 0;

    this.generatorHologram.delete();
  }

  public void reset() {
    this.stopGeneration();
    this.generatorLevel = 1;
  }

}
