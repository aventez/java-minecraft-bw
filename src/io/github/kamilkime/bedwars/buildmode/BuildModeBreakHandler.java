package io.github.kamilkime.bedwars.buildmode;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.constant.BuildModeConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.TeamUtils;
import io.github.kamilkime.bedwars.user.UserData;

public final class BuildModeBreakHandler {

  private BuildModeBreakHandler() {
  }

  public static void handleBlockBreak(final BlockBreakEvent event) {
    final Player player = event.getPlayer();
    final ItemStack handItem = player.getInventory().getItemInMainHand();

    if (!BuildModeConstants.BUILDMODE_DESTROY.equals(handItem)) {
      return;
    }

    final Arena arena = UserData.getUser(player.getUniqueId()).getCurrentArena();
    final Block block = event.getBlock();
    final Location location = block.getLocation();

    event.setCancelled(true);

    if (block.getType() == Material.CHEST) {
      for (final Team team : arena.getTeams().values()) {
        if (location.equals(team.getTeamChest())) {
          team.setTeamChest(null);
          player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_CHEST_REMOVED).with("{TEAM}", team.getTeamColor().getTeamName()).toString());

          block.setType(Material.AIR);
          return;
        }
      }
    } else if (block.getType() == Material.BED_BLOCK) {
      for (final Team team : arena.getTeams().values()) {
        final Pair<Location, Location> teamBed = team.getTeamBed();
        if (teamBed == null) {
          continue;
        }

        if (location.equals(teamBed.getLeft()) || location.equals(teamBed.getRight())) {
          player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_BED_REMOVED).with("{TEAM}", team.getTeamColor().getTeamName()).toString());

          TeamUtils.breakBedBlocks(teamBed);
          team.setTeamBed(null);

          return;
        }
      }
    }
  }

}
