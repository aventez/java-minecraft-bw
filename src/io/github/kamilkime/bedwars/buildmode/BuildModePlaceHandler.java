package io.github.kamilkime.bedwars.buildmode;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaRegion;
import io.github.kamilkime.bedwars.constant.BuildModeConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.arena.ArenaMode;
import io.github.kamilkime.bedwars.constant.generator.GeneratorType;
import io.github.kamilkime.bedwars.constant.team.TeamColor;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.UserData;

public final class BuildModePlaceHandler {

  private BuildModePlaceHandler() {
  }

  public static void handleBlockPlace(final BlockPlaceEvent event) {
    final ItemStack handItem = event.getItemInHand();
    if (handItem == null || handItem.getType() == Material.AIR) {
      return;
    }

    final Player player = event.getPlayer();
    final Arena arena = UserData.getUser(player.getUniqueId()).getCurrentArena();
    final ArenaMode arenaMode = arena.getArenaMode();
    final ArenaRegion arenaRegion = arena.getArenaRegion();
    final Block placedBlock = event.getBlockPlaced();
    final Location location = placedBlock.getLocation().add(0.5D, 0.5D, 0.5D);

    if (BuildModeConstants.BUILDMODE_LOBBY.equals(handItem)) {
      giveLobbys(player);
    } else if (BuildModeConstants.BUILDMODE_LOBBY_MAIN.equals(handItem)) {
      arenaRegion.setMainLobbyLocation(location);
      player.sendMessage(Messages.BUILDMODE_MAIN_LOBBY_CREATED);
      BuildMode.giveStartInventory(player);
    } else if (BuildModeConstants.BUILDMODE_LOBBY_SHOP.equals(handItem)) {
      arenaRegion.setShopLobbyLocation(location);
      player.sendMessage(Messages.BUILDMODE_LOBBY_SHOP_CREATED);
    } else if (BuildModeConstants.BUILDMODE_LOBBY_SPECTATOR.equals(handItem)) {
      arenaRegion.setSpectatorLobbyLocation(location);
      player.sendMessage(Messages.BUILDMODE_SPECTATOR_LOBBY_CREATED);
      BuildMode.giveStartInventory(player);
    } else if (BuildModeConstants.BUILDMODE_LOBBY_CENTER.equals(handItem)) {
      arenaRegion.setArenaCenter(location);
      player.sendMessage(Messages.BUILDMODE_ARENA_CENTER_CREATED);
      BuildMode.giveStartInventory(player);
    } else if (BuildModeConstants.BUILDMODE_TEAM_SPAWN.equals(handItem)) {
      giveTeamColors(player, arenaMode);
      return;
    } else if (BuildModeConstants.BUILDMODE_TEAM_DEATHMATCH_SPAWN.equals(handItem)) {
      giveTeamColors(player, arenaMode);
      return;
    } else if (BuildModeConstants.BUILDMODE_TEAM_BED.equals(handItem)) {
      giveTeamColors(player, arenaMode);
      return;
    } else if (BuildModeConstants.BUILDMODE_TEAM_CHEST.equals(handItem)) {
      final Material blockType = placedBlock.getType();
      final byte blockData = placedBlock.getData();

      Bukkit.getScheduler().runTaskLater(BedWars.getPlugin(), () -> {
        placedBlock.setType(blockType);
        placedBlock.setData(blockData);
      }, 2L);

      giveTeamColors(player, arenaMode);

      event.setCancelled(true);
      return;
    } else if (BuildModeConstants.BUILDMODE_GENERATOR.equals(handItem)) {
      giveGenerators(player);
    } else if (BuildModeConstants.BUILDMODE_GENERATOR_TEAM.equals(handItem)) {
      giveTeamColors(player, arenaMode);
      return;
    } else if (BuildModeConstants.BUILDMODE_GENERATOR_DIAMOND.equals(handItem)) {
      arenaRegion.addGenerator(new ItemGenerator(GeneratorType.DIAMOND, location));
      player.sendMessage(Replacer.of(Messages.BUILDMODE_GENERATOR_CREATED).with("{TYPE}", "DIAMOND").toString());
      BuildMode.giveStartInventory(player);
    } else if (BuildModeConstants.BUILDMODE_GENERATOR_EMERALD.equals(handItem)) {
      arenaRegion.addGenerator(new ItemGenerator(GeneratorType.EMERALD, location));
      player.sendMessage(Replacer.of(Messages.BUILDMODE_GENERATOR_CREATED).with("{TYPE}", "EMERALD").toString());
      BuildMode.giveStartInventory(player);
    } else if (BuildModeConstants.BUILDMODE_SHOP.equals(handItem)) {
      final CreatureSpawner spawner = (CreatureSpawner) event.getBlockPlaced().getState();

      spawner.setSpawnedType(EntityType.VILLAGER);
      spawner.setDelay(Integer.MAX_VALUE);
      spawner.update(true);

      giveShops(player);
      return;
    } else if (BuildModeConstants.BUILDMODE_DESTROY.equals(handItem)) {
      event.setCancelled(true);

      if (location.equals(arenaRegion.getMainLobbyLocation())) {
        arenaRegion.setMainLobbyLocation(null);
        player.sendMessage(Messages.BUILDMODE_MAIN_LOBBY_REMOVED);
        return;
      }

      if (location.equals(arenaRegion.getSpectatorLobbyLocation())) {
        arenaRegion.setSpectatorLobbyLocation(null);
        player.sendMessage(Messages.BUILDMODE_SPECTATOR_LOBBY_REMOVED);
        return;
      }

      if (location.equals(arenaRegion.getArenaCenter())) {
        arenaRegion.setArenaCenter(null);
        player.sendMessage(Messages.BUILDMODE_ARENA_CENTER_REMOVED);
        return;
      }

      for (final ItemGenerator generator : arenaRegion.getGenerators()) {
        if (location.equals(generator.getLocation())) {
          arenaRegion.removeGenerator(generator);
          player.sendMessage(Replacer.of(Messages.BUILDMODE_GENERATOR_REMOVED).with("{TYPE}", generator.getType().toString()).toString());
          return;
        }
      }

      for (final Team team : arena.getTeams().values()) {
        if (location.equals(team.getTeamSpawn())) {
          team.setTeamSpawn(null);
          player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_SPAWN_REMOVED).with("{TEAM}", team.getTeamColor().getTeamName()).toString());
          return;
        }

        if (location.equals(team.getDeathmatchSpawn())) {
          team.setDeathmatchSpawn(null);
          player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_DEATHMATCH_SPAWN_REMOVED).with("{TEAM}", team.getTeamColor().getTeamName()).toString());
          return;
        }

        final ItemGenerator teamGenerator = team.getTeamGenerator();
        if (teamGenerator != null && location.equals(teamGenerator.getLocation())) {
          team.setTeamGenerator(null);
          player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_GENERATOR_REMOVED).with("{TEAM}", team.getTeamColor().getTeamName()).toString());
          return;
        }
      }
    } else {
      return;
    }

    event.setCancelled(true);
  }

  private static void giveTeamColors(final Player player, final ArenaMode arenaMode) {
    final Inventory playerInventory = player.getInventory();
    playerInventory.clear();

    int slot = 0;
    for (final TeamColor color : arenaMode.getTeamColors()) {
      playerInventory.setItem(slot++, BuildModeConstants.TEAM_DYES.get(color));
    }
  }

  private static void giveLobbys(final Player player) {
    final Inventory playerInventory = player.getInventory();
    playerInventory.clear();

    playerInventory.setItem(2, BuildModeConstants.BUILDMODE_LOBBY_MAIN);
    playerInventory.setItem(4, BuildModeConstants.BUILDMODE_LOBBY_SPECTATOR);
    playerInventory.setItem(6, BuildModeConstants.BUILDMODE_LOBBY_CENTER);
    playerInventory.setItem(7, BuildModeConstants.BUILDMODE_LOBBY_SHOP);
  }

  private static void giveGenerators(final Player player) {
    final Inventory playerInventory = player.getInventory();
    playerInventory.clear();

    playerInventory.setItem(2, BuildModeConstants.BUILDMODE_GENERATOR_TEAM);
    playerInventory.setItem(4, BuildModeConstants.BUILDMODE_GENERATOR_DIAMOND);
    playerInventory.setItem(6, BuildModeConstants.BUILDMODE_GENERATOR_EMERALD);
  }

  private static void giveShops(final Player player) {
    final Inventory playerInventory = player.getInventory();
    playerInventory.clear();

    playerInventory.setItem(3, BuildModeConstants.BUILDMODE_SHOP_PERSONAL);
    playerInventory.setItem(5, BuildModeConstants.BUILDMODE_SHOP_TEAM);
  }

}
