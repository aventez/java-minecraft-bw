package io.github.kamilkime.bedwars.buildmode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitTask;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaRegion;
import io.github.kamilkime.bedwars.constant.BuildModeConstants;
import io.github.kamilkime.bedwars.constant.generator.GeneratorType;
import io.github.kamilkime.bedwars.constant.team.TeamColor;
import io.github.kamilkime.bedwars.data.BedWarsData;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;

public class BuildMode {

  private static final Set<User> BUILDERS = new HashSet<>();
  private static final Map<Arena, BukkitTask> ARENA_TASKS = new HashMap<>();

  public static void clear() {
    for (final User user : BUILDERS) {
      removeBuilder(user);
    }
  }

  public static boolean isWorkedOn(final Arena arena) {
    return ARENA_TASKS.containsKey(arena);
  }

  public static boolean isBuilding(final User user) {
    return BUILDERS.contains(user);
  }

  public static void addBuilder(final User user, final Arena arena) {
    BUILDERS.add(user);
    user.setCurrentArena(arena);

    giveStartInventory(user.getPlayer());

    if (!ARENA_TASKS.containsKey(arena)) {
      startArenaTask(arena);
    }

    Location teleportLocation = arena.getArenaRegion().getArenaCenter();
    if (teleportLocation == null) {
      teleportLocation = arena.getArenaRegion().getArenaBounds().getRight();
    }

    user.getPlayer().teleport(teleportLocation);
  }

  public static void removeBuilder(final User user) {
    BUILDERS.remove(user);

    final Arena userArena = user.getCurrentArena();
    boolean stopArenaTask = true;

    for (final User builder : BUILDERS) {
      if (builder.getCurrentArena().equals(userArena)) {
        stopArenaTask = false;
        break;
      }
    }

    if (stopArenaTask) {
      ARENA_TASKS.remove(userArena).cancel();
      BedWarsData.saveArena(userArena);
    }

    user.getPlayer().getInventory().clear();
    user.setCurrentArena(null);
  }

  public static void giveStartInventory(final Player player) {
    final Inventory playerInventory = player.getInventory();

    playerInventory.clear();
    playerInventory.setItem(0, BuildModeConstants.BUILDMODE_LOBBY);
    playerInventory.setItem(1, BuildModeConstants.BUILDMODE_GENERATOR);
    playerInventory.setItem(2, BuildModeConstants.BUILDMODE_SHOP);
    playerInventory.setItem(3, BuildModeConstants.BUILDMODE_TEAM_BED);
    playerInventory.setItem(4, BuildModeConstants.BUILDMODE_TEAM_SPAWN);
    playerInventory.setItem(5, BuildModeConstants.BUILDMODE_TEAM_DEATHMATCH_SPAWN);
    playerInventory.setItem(6, BuildModeConstants.BUILDMODE_TEAM_CHEST);
    playerInventory.setItem(7, BuildModeConstants.BUILDMODE_DESTROY);
    playerInventory.setItem(8, BuildModeConstants.BUILDMODE_WORLDEDIT_TELEPORTER);
  }

  private static void startArenaTask(final Arena arena) {
    ARENA_TASKS.put(arena, Bukkit.getScheduler().runTaskTimerAsynchronously(BedWars.getPlugin(), () -> {
      for (final Team team : arena.getTeams().values()) {
        final TeamColor teamColor = team.getTeamColor();
        final Color color = teamColor.getDyeColor().getColor();

        if (team.getTeamBed() != null) {
          spawnRedstone(team.getTeamBed().getLeft().add(0.5D, 1.0D, 0.5D), color);
        }

        if (team.getTeamSpawn() != null) {
          spawnRedstone(team.getTeamSpawn(), color);
        }

        if (team.getDeathmatchSpawn() != null) {
          spawnNote(team.getDeathmatchSpawn(), teamColor.getNoteColor());
        }

        if (team.getTeamChest() != null) {
          spawnRedstone(team.getTeamChest().add(0.5D, 1.25D, 0.5D), color);
        }

        if (team.getTeamGenerator() != null) {
          spawnNote(team.getTeamGenerator().getLocation(), teamColor.getNoteColor());
        }
      }

      final ArenaRegion region = arena.getArenaRegion();
      if (region.getMainLobbyLocation() != null) {
        spawnParticle(Particle.END_ROD, region.getMainLobbyLocation());
      }

      if (region.getSpectatorLobbyLocation() != null) {
        spawnParticle(Particle.FLAME, region.getSpectatorLobbyLocation());
      }

      if (region.getArenaCenter() != null) {
        spawnParticle(Particle.SMOKE_NORMAL, region.getArenaCenter());
      }

      for (final ItemGenerator generator : region.getGenerators()) {
        if (generator.getType() == GeneratorType.DIAMOND) {
          spawnParticle(Particle.DRIP_WATER, generator.getLocation());
        } else {
          spawnParticle(Particle.VILLAGER_HAPPY, generator.getLocation());
        }
      }
    }, 0L, 5L));
  }

  public static void spawnRedstone(final Location location, final Color color) {
    location.getWorld().spawnParticle(Particle.REDSTONE, location, 0, color.getRed() == 0 ? 0.01D : color.getRed() / 255.0D,
        color.getGreen() / 255.0D, color.getBlue() / 255.0D, 1.0D);
  }

  private static void spawnNote(final Location location, final double noteColor) {
    location.getWorld().spawnParticle(Particle.NOTE, location, 0, noteColor, 0.0D, 0.0D, 1.0D);
  }

  private static void spawnParticle(final Particle particle, final Location location) {
    location.getWorld().spawnParticle(particle, location, 1, 0.0D, 0.0D, 0.0D, 0.0D);
  }

}
