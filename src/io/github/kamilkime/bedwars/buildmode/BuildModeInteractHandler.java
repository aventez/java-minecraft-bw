package io.github.kamilkime.bedwars.buildmode;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Colorable;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaRegion;
import io.github.kamilkime.bedwars.arena.ArenaUtils;
import io.github.kamilkime.bedwars.constant.BuildModeConstants;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.arena.ShopType;
import io.github.kamilkime.bedwars.constant.generator.GeneratorType;
import io.github.kamilkime.bedwars.data.Replacer;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.team.TeamUtils;
import io.github.kamilkime.bedwars.user.UserData;

public final class BuildModeInteractHandler {

  private BuildModeInteractHandler() {
  }

  public static void handleInteract(final PlayerInteractEvent event) {
    if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
      return;
    }

    final Player player = event.getPlayer();
    final ItemStack handItem = player.getInventory().getItemInMainHand();

    if (handItem == null || handItem.getType() == Material.AIR) {
      return;
    }

    final Arena arena = UserData.getUser(player.getUniqueId()).getCurrentArena();
    final ArenaRegion arenaRegion = arena.getArenaRegion();
    final Block clickedBlock = event.getClickedBlock();
    final Location location = clickedBlock.getLocation();

    if (BuildModeConstants.BUILDMODE_SHOP_PERSONAL.equals(handItem)) {
      if (clickedBlock.getType() == Material.MOB_SPAWNER) {
        clickedBlock.setType(Material.AIR);

        final Location shopLocation = clickedBlock.getLocation().add(0.5D, 0.0D, 0.5D);
        shopLocation.setYaw((player.getLocation().getYaw() + 180.0F) % 360.0F);
        arenaRegion.addShop(ArenaUtils.spawnShop(shopLocation, ShopType.PERSONAL), ShopType.PERSONAL);

        player.sendMessage(Messages.BUILDMODE_PERSONAL_SHOP_CREATED);
        BuildMode.giveStartInventory(player);
      }
    } else if (BuildModeConstants.BUILDMODE_SHOP_TEAM.equals(handItem)) {
      if (clickedBlock.getType() == Material.MOB_SPAWNER) {
        clickedBlock.setType(Material.AIR);

        final Location shopLocation = clickedBlock.getLocation().add(0.5D, 0.0D, 0.5D);
        shopLocation.setYaw((player.getLocation().getYaw() + 180.0F) % 360.0F);
        arenaRegion.addShop(ArenaUtils.spawnShop(shopLocation, ShopType.TEAM), ShopType.TEAM);

        player.sendMessage(Messages.BUILDMODE_TEAM_SHOP_CREATED);
        BuildMode.giveStartInventory(player);
      }
    } else if (handItem.getType() == Material.INK_SACK) {
      final Team team = arena.getTeams().get(BuildModeConstants.TEAM_DYES_INVERTED.get(handItem));
      final String teamName = team.getTeamColor().getTeamName();

      if (clickedBlock.getType() == BuildModeConstants.BUILDMODE_TEAM_SPAWN.getType()) {
        clickedBlock.setType(Material.AIR);
        team.setTeamSpawn(location.add(0.5D, 0.5D, 0.5D));
        player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_SPAWN_CREATED).with("{TEAM}", teamName).toString());
      } else if (clickedBlock.getType() == BuildModeConstants.BUILDMODE_TEAM_DEATHMATCH_SPAWN.getType()) {
        clickedBlock.setType(Material.AIR);
        team.setDeathmatchSpawn(location.add(0.5D, 0.5D, 0.5D));
        player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_DEATHMATCH_SPAWN_CREATED).with("{TEAM}", teamName).toString());
      } else if (clickedBlock.getType() == BuildModeConstants.BUILDMODE_GENERATOR_TEAM.getType()) {
        clickedBlock.setType(Material.AIR);
        team.setTeamGenerator(new ItemGenerator(GeneratorType.TEAM, location.add(0.5D, 0.5D, 0.5D)));
        player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_GENERATOR_CREATED).with("{TEAM}", teamName).toString());
      } else if (clickedBlock.getType() == Material.CHEST) {
        team.setTeamChest(location);
        player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_CHEST_CREATED).with("{TEAM}", teamName).toString());
      } else if (clickedBlock.getType() == Material.BED_BLOCK) {
        if (clickedBlock.getData() > 3) {
          final Block adjacentBlock = clickedBlock.getRelative(TeamUtils.getBedFaceFromData((byte) (clickedBlock.getData() - 8)).getOppositeFace());
          team.setTeamBed(Pair.of(adjacentBlock.getLocation(), adjacentBlock.getData()));
        } else {
          team.setTeamBed(Pair.of(location, clickedBlock.getData()));
        }

        try {
          final Pair<Location, Location> bed = team.getTeamBed();

          final BlockState leftState = bed.getLeft().getBlock().getState();
          ((Colorable) leftState).setColor(team.getTeamColor().getDyeColor());
          leftState.update(true);

          final BlockState rightState = bed.getRight().getBlock().getState();
          ((Colorable) rightState).setColor(team.getTeamColor().getDyeColor());
          rightState.update(true);
        } catch (Exception ignored) {
        }

        player.sendMessage(Replacer.of(Messages.BUILDMODE_TEAM_BED_CREATED).with("{TEAM}", teamName).toString());
      } else {
        return;
      }

      BuildMode.giveStartInventory(player);
    } else {
      return;
    }

    event.setCancelled(true);
  }

  public static void handleEntityInteract(final PlayerInteractAtEntityEvent event) {
    final Entity shopEntity = event.getRightClicked();
    final Player player = event.getPlayer();
    final ItemStack handItem = player.getInventory().getItemInMainHand();
    final Arena arena = UserData.getUser(player.getUniqueId()).getCurrentArena();
    final ArenaRegion arenaRegion = arena.getArenaRegion();

    if (BuildModeConstants.BUILDMODE_DESTROY.equals(handItem)) {
      if (arenaRegion.getShops().containsKey(shopEntity)) {
        player.sendMessage(Replacer.of(Messages.BUILDMODE_SHOP_REMOVED).with("{TYPE}", arenaRegion.removeShop(shopEntity).toString()).toString());
        shopEntity.remove();
        event.setCancelled(true);
      }
    }
  }

}
