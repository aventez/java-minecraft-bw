package io.github.kamilkime.bedwars.data;

import org.apache.commons.lang.StringUtils;

public final class Replacer {

  private String message;

  private Replacer(final String message) {
    this.message = message;
  }

  public static Replacer of(final String message) {
    return new Replacer(message);
  }

  public Replacer with(final String toReplace, final String replacement) {
    this.message = StringUtils.replace(this.message, toReplace, replacement);
    return this;
  }

  public Replacer with(final String toReplace, final int replacement) {
    this.message = StringUtils.replace(this.message, toReplace, Integer.toString(replacement));
    return this;
  }

  public Replacer with(final String toReplace, final long replacement) {
    this.message = StringUtils.replace(this.message, toReplace, Long.toString(replacement));
    return this;
  }

  @Override
  public String toString() {
    return this.message;
  }

}
