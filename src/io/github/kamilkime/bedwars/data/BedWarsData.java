package io.github.kamilkime.bedwars.data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;

import io.github.kamilkime.bedwars.BedWars;
import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.arena.ArenaData;
import io.github.kamilkime.bedwars.arena.ArenaRegion;
import io.github.kamilkime.bedwars.arena.ArenaUtils;
import io.github.kamilkime.bedwars.constant.arena.ArenaMode;
import io.github.kamilkime.bedwars.constant.arena.ShopType;
import io.github.kamilkime.bedwars.constant.generator.GeneratorType;
import io.github.kamilkime.bedwars.constant.perk.Perk;
import io.github.kamilkime.bedwars.generator.ItemGenerator;
import io.github.kamilkime.bedwars.team.Team;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public final class BedWarsData {

  public static final File ARENAS_FOLDER = new File(BedWars.getPlugin().getDataFolder(), "arenas");
  public static final File USERS_FOLDER = new File(BedWars.getPlugin().getDataFolder(), "users");

  public static final File NAME_CACHE_FILE = new File(BedWars.getPlugin().getDataFolder(), "names.cache");

  private BedWarsData() {
  }

  public static void checkFiles() {
    if (!ARENAS_FOLDER.exists()) {
      ARENAS_FOLDER.mkdirs();
    }

    if (!USERS_FOLDER.exists()) {
      USERS_FOLDER.mkdirs();
    }
  }

  public static void loadAll() {
    checkFiles();
    loadArenas();
    loadUsers();
    loadNameCache();
  }

  private static void loadArenas() {
    for (final File arenaFile : ARENAS_FOLDER.listFiles()) {
      final YamlConfiguration arenaYaml = YamlConfiguration.loadConfiguration(arenaFile);

      // CONSTRUTOR SETTINGS
      final String arenaName = arenaFile.getName().substring(0, arenaFile.getName().length() - 6);
      final ArenaMode arenaMode = ArenaMode.valueOf(arenaYaml.getString("arenaMode"));
      final Location minBound = DataUtils.stringToLocation(arenaYaml.getString("region.minBound"));
      final Location maxBound = DataUtils.stringToLocation(arenaYaml.getString("region.maxBound"));

      final Arena arena = new Arena(arenaName, arenaMode, minBound, maxBound);

      // ARENA REGION
      final ArenaRegion arenaRegion = arena.getArenaRegion();
      arenaRegion.setMainLobbyLocation(DataUtils.stringToLocation(arenaYaml.getString("region.mainLobbyLocation")));
      arenaRegion.setShopLobbyLocation(DataUtils.stringToLocation(arenaYaml.getString("region.shopLobbyLocation")));
      arenaRegion.setSpectatorLobbyLocation(DataUtils.stringToLocation(arenaYaml.getString("region.spectatorLobbyLocation")));
      arenaRegion.setArenaCenter(DataUtils.stringToLocation(arenaYaml.getString("region.arenaCenter")));
      arenaRegion.getArenaCenter().getWorld().setGameRuleValue("doDaylightCycle", "false");
      arenaRegion.getArenaCenter().getWorld().setTime(1000L);

      // CREATE SHOP IN LOBBY	
      arenaRegion.addShop(ArenaUtils.spawnShop(DataUtils.stringToLocation(arenaYaml.getString("region.shopLobbyLocation")), ShopType.TEAM_LOBBY), ShopType.TEAM_LOBBY);
      
      // ARENA SHOPS
      final List<String> shopList = arenaYaml.getStringList("region.shops");
      if (shopList != null && !shopList.isEmpty()) {
        for (final String shop : shopList) {
          final String[] split = shop.split("\\|");
          final ShopType shopType = ShopType.valueOf(split[1]);

          arenaRegion.addShop(ArenaUtils.spawnShop(DataUtils.stringToLocation(split[0]), shopType), shopType);
        }
      }

      // ARENA GENERATORS
      final List<String> generatorList = arenaYaml.getStringList("region.generators");
      if (generatorList != null && !generatorList.isEmpty()) {
        for (final String generator : generatorList) {
          final String[] split = generator.split("\\|");
          arenaRegion.addGenerator(new ItemGenerator(GeneratorType.valueOf(split[1]), DataUtils.stringToLocation(split[0])));
        }
      }

      // ARENA TEAMS
      for (final Team team : arena.getTeams().values()) {
        final ConfigurationSection teamSection = arenaYaml.getConfigurationSection("teams." + team.getTeamColor().toString());
        if (teamSection == null) {
          continue;
        }

        final Location generationLocation = DataUtils.stringToLocation(teamSection.getString("teamGenerator"));
        if (generationLocation != null) {
          team.setTeamGenerator(new ItemGenerator(GeneratorType.TEAM, generationLocation));
        }

        team.setTeamSpawn(DataUtils.stringToLocation(teamSection.getString("teamSpawn")));
        team.setDeathmatchSpawn(DataUtils.stringToLocation(teamSection.getString("deathmatchSpawn")));

        team.setTeamChest(DataUtils.stringToLocation(teamSection.getString("teamChest")));
        if (team.getTeamChest() != null && team.getTeamChest().getBlock().getType() != Material.CHEST) {
          team.getTeamChest().getBlock().setType(Material.CHEST);
        }

        final String teamBed = teamSection.getString("teamBed");
        if (teamBed != null && !teamBed.isEmpty()) {
          final String[] bed = teamBed.split("\\|");
          team.setTeamBed(Pair.of(DataUtils.stringToLocation(bed[0]), Byte.parseByte(bed[1])));
        }
      }

      if (arenaYaml.getBoolean("enabled") && arena.canBeEnabled() == null) {
        arena.setEnabled(true);
        ArenaUtils.resetArena(arena, true);
      }
    }
  }

  private static void loadUsers() {
    for (final File userFile : USERS_FOLDER.listFiles()) {
      final YamlConfiguration userYaml = YamlConfiguration.loadConfiguration(userFile);

      final UUID uuid = UUID.fromString(userFile.getName().substring(0, userFile.getName().length() - 5));
      final long money = userYaml.getLong("money");
      final long kills = userYaml.getLong("kills");
      final long finalKills = userYaml.getLong("finalKills");
      final long deaths = userYaml.getLong("deaths");
      final long destroyedBeds = userYaml.getLong("destroyedBeds");
      final long level = userYaml.getLong("level");
      final long xp = userYaml.getLong("xp");

      final User user = new User(uuid, money, kills, finalKills, deaths, destroyedBeds, level, xp);

      final ConfigurationSection perkSection = userYaml.getConfigurationSection("perkLevels");
      if (perkSection != null) {
        final Set<String> keys = perkSection.getKeys(false);
        if (keys != null && !keys.isEmpty()) {
          for (final String perkName : keys) {
            user.setPerkLevel(Perk.valueOf(perkName), userYaml.getInt("perkLevels." + perkName));
          }
        }
      }

      QuickShopData quickShopData = new QuickShopData();
      final ConfigurationSection quickShopItemSection = userYaml.getConfigurationSection("quickShopItem");
      if (quickShopItemSection != null) {
        final Set<String> keys = quickShopItemSection.getKeys(false);
        if (keys != null && !keys.isEmpty()) {
          for (final String key : keys) {
            quickShopData.getQuickShopItems().put(Integer.parseInt(key), QuickShopItem.deserialize(userYaml.getString("quickShopItem." + key)));
          }
        }
      }
      user.setQuickShopData(quickShopData);
    }
  }

  private static void loadNameCache() {
    if (!NAME_CACHE_FILE.exists()) {
      return;
    }

    try {
      final Scanner cacheScanner = new Scanner(NAME_CACHE_FILE);
      while (cacheScanner.hasNextLine()) {
        final String[] nextLine = cacheScanner.nextLine().split(" ");
        UserData.createMatch(nextLine[0], UUID.fromString(nextLine[1]));
      }

      cacheScanner.close();
    } catch (IOException exception) {
      Bukkit.getLogger().warning("[BedWars] Failed to load name cache data from its file!");
    }
  }

  public static void saveAll() {
    saveUserData();
    saveArenas();
  }

  public static void saveUserData() {
    checkFiles();
    saveUsers();
    saveNameCache();
  }

  private static void saveArenas() {
    final Collection<Arena> arenas = ArenaData.getArenas();
    if (arenas.isEmpty()) {
      return;
    }

    for (final Arena arena : arenas) {
      saveArena(arena);
    }
  }

  public static void saveArena(final Arena arena) {
    final File arenaFile = new File(ARENAS_FOLDER, arena.getArenaName() + ".arena");
    if (!arenaFile.exists()) {
      try {
        arenaFile.createNewFile();
      } catch (IOException exception) {
        Bukkit.getLogger().warning("[BedWars] Failed to create file for arena named " + arena.getArenaName() + "!");
        return;
      }
    }

    // MAIN SETTINGS
    final YamlConfiguration arenaYaml = YamlConfiguration.loadConfiguration(arenaFile);
    arenaYaml.set("arenaMode", arena.getArenaMode().toString());
    arenaYaml.set("enabled", arena.isEnabled());

    // ARENA REGION
    final ArenaRegion arenaRegion = arena.getArenaRegion();
    arenaYaml.set("region.mainLobbyLocation", DataUtils.locationToString(arenaRegion.getMainLobbyLocation()));
    arenaYaml.set("region.shopLobbyLocation", DataUtils.locationToString(arenaRegion.getShopLobbyLocation()));
    arenaYaml.set("region.spectatorLobbyLocation", DataUtils.locationToString(arenaRegion.getSpectatorLobbyLocation()));
    arenaYaml.set("region.arenaCenter", DataUtils.locationToString(arenaRegion.getArenaCenter()));
    arenaYaml.set("region.minBound", DataUtils.locationToString(arenaRegion.getArenaBounds().getLeft()));
    arenaYaml.set("region.maxBound", DataUtils.locationToString(arenaRegion.getArenaBounds().getRight()));

    // ARENA SHOPS
    final List<String> shops = new ArrayList<>();
    for (final Entry<Entity, ShopType> shopEntry : arenaRegion.getShops().entrySet()) {
      shops.add(DataUtils.locationToString(shopEntry.getKey().getLocation()) + "|" + shopEntry.getValue().toString());
    }

    arenaYaml.set("region.shops", shops);

    // ARENA GENERATORS
    final List<String> generators = new ArrayList<>();
    for (final ItemGenerator generator : arenaRegion.getGenerators()) {
      generators.add(DataUtils.locationToString(generator.getLocation()) + "|" + generator.getType().toString());
    }

    arenaYaml.set("region.generators", generators);

    // ARENA TEAMS
    for (final Team team : arena.getTeams().values()) {
      final String base = "teams." + team.getTeamColor().toString() + ".";

      final ItemGenerator generator = team.getTeamGenerator();
      if (generator != null) {
        arenaYaml.set(base + "teamGenerator", DataUtils.locationToString(generator.getLocation()));
      } else {
        arenaYaml.set(base + "teamGenerator", null);
      }

      arenaYaml.set(base + "teamSpawn", DataUtils.locationToString(team.getTeamSpawn()));
      arenaYaml.set(base + "teamChest", DataUtils.locationToString(team.getTeamChest()));
      arenaYaml.set(base + "deathmatchSpawn", DataUtils.locationToString(team.getDeathmatchSpawn()));

      final Pair<Location, Location> teamBed = team.getTeamBed();
      if (teamBed != null) {
        arenaYaml.set(base + "teamBed", DataUtils.locationToString(teamBed.getLeft()) + "|" + team.getTeamBedData());
      } else {
        arenaYaml.set(base + "teamBed", null);
      }
    }

    try {
      arenaYaml.save(arenaFile);
    } catch (IOException exception) {
      Bukkit.getLogger().warning("[BedWars] Failed to save file for arena named " + arena.getArenaName() + "!");
      return;
    }
  }

  private static void saveUsers() {
    final Collection<User> users = UserData.getUsers();
    if (users.isEmpty()) {
      return;
    }

    for (final User user : users) {
      final File userFile = new File(USERS_FOLDER, user.getUUID().toString() + ".user");
      if (!userFile.exists()) {
        try {
          userFile.createNewFile();
        } catch (IOException exception) {
          Bukkit.getLogger().warning("[BedWars] Failed to create file for user with uuid " + user.getUUID().toString() + "!");
          continue;
        }
      }

      final YamlConfiguration userYaml = YamlConfiguration.loadConfiguration(userFile);

      userYaml.set("money", user.getMoney());
      userYaml.set("kills", user.getKills());
      userYaml.set("finalKills", user.getFinalKills());
      userYaml.set("deaths", user.getDeaths());
      userYaml.set("destroyedBeds", user.getDestroyedBeds());
      userYaml.set("level", user.getLevel());
      userYaml.set("xp", user.getXP());

      for (final Entry<Perk, Integer> perkEntry : user.getPerkLevels().entrySet()) {
        userYaml.set("perkLevels." + perkEntry.getKey().toString(), perkEntry.getValue());
      }

      for (Map.Entry<Integer, QuickShopItem> entry : user.getQuickShopData().getQuickShopItems().entrySet()) {
        userYaml.set("quickShopItem." + entry.getKey().toString(), entry.getValue().serialize());
      }

      try {
        userYaml.save(userFile);
      } catch (IOException exception) {
        Bukkit.getLogger().warning("[BedWars] Failed to save file for user with uuid " + user.getUUID().toString() + "!");
      }
    }
  }

  private static void saveNameCache() {
    final Map<String, UUID> nameCache = UserData.getNameCache();
    if (nameCache.isEmpty()) {
      return;
    }

    if (!NAME_CACHE_FILE.exists()) {
      try {
        NAME_CACHE_FILE.createNewFile();
      } catch (IOException exception) {
        Bukkit.getLogger().warning("[BedWars] Failed to create a name cache file!");
        return;
      }
    }

    try {
      final FileWriter cacheWriter = new FileWriter(NAME_CACHE_FILE);
      for (final Entry<String, UUID> cacheEntry : nameCache.entrySet()) {
        cacheWriter.write(cacheEntry.getKey() + " " + cacheEntry.getValue().toString() + "\n");
      }

      cacheWriter.close();
    } catch (IOException exception) {
      Bukkit.getLogger().warning("[BedWars] Failed to save name cache data to its file!");
    }
  }

}
