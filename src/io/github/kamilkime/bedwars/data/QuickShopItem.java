package io.github.kamilkime.bedwars.data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import io.github.kamilkime.bedwars.constant.gui.GuiType;

/**
 * @author Plajer
 * <p>
 * Created at 23.02.2019
 */
public class QuickShopItem {

  private GuiType mainType;
  private int accessor;

  public QuickShopItem(GuiType mainType, int accessor) {
    this.mainType = mainType;
    this.accessor = accessor;
  }

  @Nullable
  public static QuickShopItem deserialize(String string) {
    Pattern p = Pattern.compile("\\[(.*?)]");
    Matcher m = p.matcher(string);
    String preItem = null;
    while (m.find()) {
      preItem = m.group(1);
    }
    if (preItem == null) {
      return null;
    }
    String[] data = preItem.split(";");
    return new QuickShopItem(GuiType.valueOf(data[0]), Integer.parseInt(data[1]));
  }

  public String serialize() {
    return "QuickShopItem[" + mainType.name() + ";" + accessor + "]";
  }

  public GuiType getMainType() {
    return mainType;
  }

  public int getAccessor() {
    return accessor;
  }
}
