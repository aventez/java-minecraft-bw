package io.github.kamilkime.bedwars.data;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

public final class DataUtils {

  private static final TreeMap<Long, String> ROMAN_NUMERALS = new TreeMap<>();
  private static final TreeMap<Long, String> ROMAN_BASE = new TreeMap<>();

  static {
    ROMAN_BASE.put(1000L, "M");
    ROMAN_BASE.put(900L, "CM");
    ROMAN_BASE.put(500L, "D");
    ROMAN_BASE.put(400L, "CD");
    ROMAN_BASE.put(100L, "C");
    ROMAN_BASE.put(90L, "XC");
    ROMAN_BASE.put(50L, "L");
    ROMAN_BASE.put(40L, "XL");
    ROMAN_BASE.put(10L, "X");
    ROMAN_BASE.put(9L, "IX");
    ROMAN_BASE.put(5L, "V");
    ROMAN_BASE.put(4L, "IV");
    ROMAN_BASE.put(1L, "I");
  }

  private DataUtils() {
  }

  public static String toRoman(final long arabic) {
    String roman = ROMAN_NUMERALS.get(arabic);

    if (roman == null) {
      final long floor = ROMAN_BASE.floorKey(arabic);
      roman = ROMAN_BASE.get(floor) + (floor == arabic ? "" : toRoman(arabic - floor));
      ROMAN_NUMERALS.put(arabic, roman);
    }

    return roman;
  }

  public static String locationToString(final Location toString) {
    if (toString == null) {
      return null;
    }

    return toString.getWorld().getName() + " " + toString.getX() + " " + toString.getY() + " " + toString.getZ() + " "
        + toString.getYaw() + " " + toString.getPitch();
  }

  public static Location stringToLocation(final String toLocation) {
    if (toLocation == null || toLocation.isEmpty()) {
      return null;
    }

    final String[] split = toLocation.split(" ");
    return new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]),
        Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));
  }

  public static String color(final String message) {
    return ChatColor.translateAlternateColorCodes('&', message);
  }

  public static List<String> color(final List<String> messages) {
    final List<String> colored = new ArrayList<>();
    for (final String message : messages) {
      colored.add(color(message));
    }

    return colored;
  }

  public static boolean isInRange(final long number, final long min, final long max) {
    return number >= min && number <= max;
  }

}
