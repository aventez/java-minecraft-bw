package io.github.kamilkime.bedwars.data;

import java.text.SimpleDateFormat;

public final class TimeUtils {

  private static final SimpleDateFormat ABBREVIATED_TIME = new SimpleDateFormat("s's'");
  private static final SimpleDateFormat COLON_TIME = new SimpleDateFormat("m':'ss");
  private static final SimpleDateFormat DATE = new SimpleDateFormat("dd/MM/yyyy");

  private TimeUtils() {
  }

  public static String getAbbreviatedTime(final long time) {
    return ABBREVIATED_TIME.format(time * 1000L);
  }

  public static String getColonTime(final long time) {
    return COLON_TIME.format(time * 1000L);
  }

  public static String getDate() {
    return DATE.format(System.currentTimeMillis());
  }

}
