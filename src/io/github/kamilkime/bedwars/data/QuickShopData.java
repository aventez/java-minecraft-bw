package io.github.kamilkime.bedwars.data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Plajer
 * <p>
 * Created at 23.02.2019
 */
public class QuickShopData {

  private Map<Integer, QuickShopItem> quickShopItems = new HashMap<>();
  private QuickShopItem eventQuickShopItem;

  public QuickShopItem getEventQuickShopItem() {
    return eventQuickShopItem;
  }

  public void setEventQuickShopItem(QuickShopItem eventQuickShopItem) {
    this.eventQuickShopItem = eventQuickShopItem;
  }

  public Map<Integer, QuickShopItem> getQuickShopItems() {
    return quickShopItems;
  }

}
