package io.github.kamilkime.bedwars.data;

import java.util.Comparator;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.user.User;
import io.github.kamilkime.bedwars.user.UserData;

public enum DataSort {

  ARENA((Comparator<Arena>) (a1, a2) -> {
    int compare = Byte.compare(a1.getArenaMode().getID(), a2.getArenaMode().getID());
    if (compare == 0) {
      compare = a1.getArenaName().compareTo(a2.getArenaName());
    }

    return compare;
  }),

  USER_BED((Comparator<User>) (u1, u2) -> {
    int compare = Long.compare(u2.getDestroyedBeds(), u1.getDestroyedBeds());
    if (compare == 0) {
      compare = UserData.getName(u1.getUUID()).compareTo(UserData.getName(u2.getUUID()));
    }

    return compare;
  }),

  USER_DEATHS((Comparator<User>) (u1, u2) -> {
    int compare = Long.compare(u2.getDeaths(), u1.getDeaths());
    if (compare == 0) {
      compare = UserData.getName(u1.getUUID()).compareTo(UserData.getName(u2.getUUID()));
    }

    return compare;
  }),

  USER_FINALKILLS((Comparator<User>) (u1, u2) -> {
    int compare = Long.compare(u2.getFinalKills(), u1.getFinalKills());
    if (compare == 0) {
      compare = UserData.getName(u1.getUUID()).compareTo(UserData.getName(u2.getUUID()));
    }

    return compare;
  }),

  USER_KILLS((Comparator<User>) (u1, u2) -> {
    int compare = Long.compare(u2.getKills(), u1.getKills());
    if (compare == 0) {
      compare = UserData.getName(u1.getUUID()).compareTo(UserData.getName(u2.getUUID()));
    }

    return compare;
  }),

  USER_LEVEL((Comparator<User>) (u1, u2) -> {
    int compare = Long.compare(u2.getLevel(), u1.getLevel());
    if (compare == 0) {
      compare = UserData.getName(u1.getUUID()).compareTo(UserData.getName(u2.getUUID()));
    }

    return compare;
  }),

  USER_MONEY((Comparator<User>) (u1, u2) -> {
    int compare = Long.compare(u2.getMoney(), u1.getMoney());
    if (compare == 0) {
      compare = UserData.getName(u1.getUUID()).compareTo(UserData.getName(u2.getUUID()));
    }

    return compare;
  }),

  USER_XP((Comparator<User>) (u1, u2) -> {
    int compare = Long.compare(u2.getXP(), u1.getXP());
    if (compare == 0) {
      compare = UserData.getName(u1.getUUID()).compareTo(UserData.getName(u2.getUUID()));
    }

    return compare;
  });

  private final Comparator<?> comparator;

  DataSort(final Comparator<?> comparator) {
    this.comparator = comparator;
  }

  public Comparator<?> getComparator() {
    return this.comparator;
  }

}
