package io.github.kamilkime.bedwars.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import io.github.kamilkime.bedwars.data.DataSort;

public class UserData {

  private static final Map<UUID, User> USERS = new HashMap<>();

  private static final Map<String, UUID> NAME_CACHE_FROM_NAME = new HashMap<>();
  private static final Map<UUID, String> NAME_CACHE_FROM_UUID = new HashMap<>();

  public static User getUser(final UUID uuid) {
    return USERS.get(uuid);
  }

  public static List<User> getSortedUsers(final DataSort sortType) {
    final List<User> users = new ArrayList<>(USERS.values());
    users.sort((Comparator<User>) sortType.getComparator());
    return users;
  }

  public static Collection<User> getUsers() {
    return USERS.values();
  }

  public static void addUser(final User user) {
    USERS.put(user.getUUID(), user);
  }

  public static void removeUser(final User user) {
    USERS.remove(user.getUUID());
  }

  public static Map<String, UUID> getNameCache() {
    return new HashMap<>(NAME_CACHE_FROM_NAME);
  }

  public static UUID getUUID(final String name, final boolean ignoreCase) {
    if (!ignoreCase) {
      return NAME_CACHE_FROM_NAME.get(name);
    }

    for (final Entry<String, UUID> entry : NAME_CACHE_FROM_NAME.entrySet()) {
      if (entry.getKey().equalsIgnoreCase(name)) {
        return entry.getValue();
      }
    }

    return null;
  }

  public static String getName(final UUID uuid) {
    return NAME_CACHE_FROM_UUID.get(uuid);
  }

  public static void createMatch(final String name, final UUID uuid) {
    NAME_CACHE_FROM_NAME.put(name, uuid);
    NAME_CACHE_FROM_UUID.put(uuid, name);
  }

}
