package io.github.kamilkime.bedwars.user;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import io.github.kamilkime.bedwars.arena.Arena;
import io.github.kamilkime.bedwars.buildmode.BuildMode;
import io.github.kamilkime.bedwars.constant.Messages;
import io.github.kamilkime.bedwars.constant.XpConstants;
import io.github.kamilkime.bedwars.constant.gui.PersonalLobbyUpgrade;
import io.github.kamilkime.bedwars.constant.perk.Perk;
import io.github.kamilkime.bedwars.constant.team.upgrade.TeamUpgrade;
import io.github.kamilkime.bedwars.constant.user.ToolTier;
import io.github.kamilkime.bedwars.data.QuickShopData;
import io.github.kamilkime.bedwars.data.Replacer;

public class User {

  private final UUID uuid;
  private final Map<Perk, Integer> perkLevels = new HashMap<>();
  private QuickShopData quickShopData = new QuickShopData();
  private long money;
  private long kills;
  private long finalKills;
  private long deaths;
  private long destroyedBeds;
  private long level;
  private long xp;
  public Set<PersonalLobbyUpgrade> personalUpgrades = new LinkedHashSet<>();
  private Arena currentArena;
  private ToolTier pickaxeTier = ToolTier.NONE;
  private ToolTier axeTier = ToolTier.NONE;

  public User(final UUID uuid) {
    this(uuid, 0, 0, 0, 0, 0, 0, 0);
  }

  public User(final UUID uuid, final long money, final long kills, final long finalKills, final long deaths, final long destroyedBeds, final long level, final long xp) {
    this.uuid = uuid;
    this.money = money;
    this.kills = kills;
    this.finalKills = finalKills;
    this.deaths = deaths;
    this.destroyedBeds = destroyedBeds;
    this.level = level;
    this.xp = xp;

    UserData.addUser(this);
  }

  public UUID getUUID() {
    return this.uuid;
  }

  public Player getPlayer() {
    return Bukkit.getPlayer(this.uuid);
  }

  public long getMoney() {
    return this.money;
  }

  public long addMoney(final long money) {
    return this.money += money;
  }

  public long removeMoney(final long money) {
    return this.money -= money;
  }

  public long setMoney(final long money) {
    this.money = money;
    return this.money;
  }

  public long getKills() {
    return this.kills;
  }

  public long addKill() {
    return ++this.kills;
  }

  public long setKills(final long kills) {
    this.kills = kills;
    return this.kills;
  }

  public long getFinalKills() {
    return this.finalKills;
  }

  public long addFinalKill() {
    return ++this.finalKills;
  }

  public long setFinalKills(final long finalKills) {
    this.finalKills = finalKills;
    return this.finalKills;
  }

  public long getDeaths() {
    return this.deaths;
  }

  public long addDeath() {
    return ++this.deaths;
  }

  public long setDeaths(final long deaths) {
    this.deaths = deaths;
    return this.deaths;
  }

  public long getDestroyedBeds() {
    return this.destroyedBeds;
  }

  public long addDestroyedBed() {
    return ++this.destroyedBeds;
  }

  public long setDestroyedBeds(final long destroyedBeds) {
    this.destroyedBeds = destroyedBeds;
    return this.destroyedBeds;
  }

  public long getLevel() {
    return this.level;
  }

  public long setLevel(final long level) {
    this.level = level;
    return this.level;
  }

  public long addXP(final long xp) {
    this.xp += xp;
    this.tryLvlUp(true);

    return this.xp;
  }

  public long getXP() {
    return this.xp;
  }

  public long setXP(final long xp) {
    this.xp = xp;
    this.level = 0;

    this.tryLvlUp(false);

    return this.xp;
  }

  public void tryLvlUp(final boolean sendMessage) {
    while (this.xp >= XpConstants.getTotalXpForLvl(this.level + 1)) {
      this.level++;

      if (sendMessage) {
        final Player player = this.getPlayer();
        if (player != null) {
          player.sendMessage(Replacer.of(Messages.USER_LVL_UP).with("{LEVEL}", this.level).toString());
        }
      }
    }
  }

  public Arena getCurrentArena() {
    return this.currentArena;
  }

  public QuickShopData getQuickShopData() {
    return quickShopData;
  }

  public void setQuickShopData(QuickShopData quickShopData) {
    this.quickShopData = quickShopData;
  }

  public void setCurrentArena(final Arena arena) {
    this.currentArena = arena;
  }

  public ToolTier getPickaxeTier() {
    return this.pickaxeTier;
  }

  public void setPickaxeTier(final ToolTier tier) {
    this.pickaxeTier = tier;
  }

  public ToolTier getAxeTier() {
    return this.axeTier;
  }

  public void setAxeTier(final ToolTier tier) {
    this.axeTier = tier;
  }

  public Map<Perk, Integer> getPerkLevels() {
    return new HashMap<>(this.perkLevels);
  }

  public void setPerkLevel(final Perk perk, final int perkLevel) {
    if (perkLevel <= 0) {
      this.perkLevels.remove(perk);
    } else {
      this.perkLevels.put(perk, perkLevel);
    }
  }

  public void resetPerks() {
    this.perkLevels.clear();
  }

  public int getPerkLevel(final Perk perk) {
    return this.perkLevels.getOrDefault(perk, 0);
  }

  public double getPerkBonus(final Perk perk) {
    return perk.getPerkBonus(this.getPerkLevel(perk));
  }

  public boolean isInGame() {
    return this.currentArena != null && !BuildMode.isBuilding(this);
  }

  public boolean hasPersonalLobbyUpgrade(PersonalLobbyUpgrade upgrade) {
	return personalUpgrades.contains(upgrade);
  }
	
  public void addPersonalLobbyUpgrade(PersonalLobbyUpgrade upgrade) {
	  personalUpgrades.add(upgrade);
  }
  
  public void removePersonalLobbyUpgrade(PersonalLobbyUpgrade upgrade) {
	  personalUpgrades.remove(upgrade);
  }

}
